<?php

class ImagenController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(array('CrugeAccessControlFilter'));
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$model->path = Yii::app()->params['setecelUrl']."images/".$model->path;
		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Imagen;
		if (isset($_GET["id"])) {
			$model->id_pieza = $_GET["id"];
		}
		else{
			$this->redirect(array('/componente'));
		}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Imagen']))
		{
			$model->attributes=$_POST['Imagen'];
			$image = CUploadedFile::getInstance($model,'path');
			$model->path = $image->name;

			/*echo "<pre>";
			print_r($model->path->name);
			echo "</pre>";
			die();*/
			if($model->save())
				$image->saveAs('images/'.$image->name);
				$this->redirect(array('componente/view','id'=>$model->id_pieza));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Imagen']))
		{
			$model->attributes=$_POST['Imagen'];
			$image = CUploadedFile::getInstance($model,'path');
			$model->path = $image->name;
			if($model->save())
				$image->saveAs('images/'.$image->name);
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		$componenteid = $_GET['componente'];

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		//$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'))
		if(!isset($_GET['ajax']))
			$this->redirect(array('componente/view','id'=>$componenteid));

	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->redirect(array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Imagen('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Imagen']))
			$model->attributes=$_GET['Imagen'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Imagen the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Imagen::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Imagen $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='imagen-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
