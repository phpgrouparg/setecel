<?php

class ComponenteController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(array('CrugeAccessControlFilter'));
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{

		$model = $this->loadModel($id);

		$pics = $model->componente_img;

		if ($model->materiaPrimaFk->Inactivo === 0) {
			$model->materiaPrimaFk->Inactivo = "No";
		}else{
			$model->materiaPrimaFk->Inactivo = "Si";
		}

		$this->render('view',array(
			'model'=>$model,
			'pics'=>$pics,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Componente;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$model->materia_prima_fk = $_GET["id"];
		if(isset($_POST['Componente']))
		{
			$model->attributes=$_POST['Componente'];
			if($model->save())
				$this->redirect(array('stock/create','id'=>$model->materia_prima_fk));
			    //$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=new Componente;
		$model=$model->findByAttributes(array('materia_prima_fk' => $id));

		//=$this->loadModel($id);
		/*$model=new Componente('search');
		$model->unsetAttributes();  // clear any default values
		$model->materia_prima_fk = $id;*/

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Componente']))
		{
			$model->attributes=$_POST['Componente'];
			if($model->save())
				$this->redirect(array('stock/update','id'=>$model->materia_prima_fk));
				
				//$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->redirect(array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Componente('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Componente']))
			$model->attributes=$_GET['Componente'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Componente the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Componente::model()->with('componente_img')->with('componente_cat')->with('materiaPrimaFk')->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Componente $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='componente-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
