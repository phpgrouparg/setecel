<?php

class ProductoController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(array('CrugeAccessControlFilter'));
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{

		$cables = new Cable('search');
		$cables->unsetAttributes();
		if(isset($_GET['Listadocables']))
			$cables->attributes=$_GET['Listadocables'];

		$componentes=new Listadocomponentes('search');
		$componentes->unsetAttributes();  // clear any default values
		if(isset($_GET['Listadocomponentes']))
			$componentes->attributes=$_GET['Listadocomponentes'];

		//$detalleParte = DetalleParte::model()->findByAttributes(array("producto_id",$id));
		$detalleParte = new ViewDetalleparte('search');
		$detalleParte->unsetAttributes();
		$detalleParte->producto_id = $id;

		$materiaPrima = new MateriaPrima;
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'detalleParte'=>$detalleParte,
			'materiaPrima'=>$materiaPrima,
			'cables'=>$cables,
			'componentes'=>$componentes,
		));
	}

	/*
	 * Function actionCantidades()
	 *
	 */

	public function actionCantidades()
	{

		//$model=$this->loadModel($id);

		if(isset($_POST['Producto']))
		{
			$model=new Producto;
			$model->attributes=$_POST['Producto'];
			$materiaPrima = new MateriaPrima;
			$productos = array();

			foreach ($_POST['MateriaPrima'] as $key => $value) {
				$item = $materiaPrima->findByPk($key);
				if ($item->id_tipo == "1") {
					$productos[$key] = Cable::model()->findByAttributes(array('materia_prima_fk' => $value))->cod_setecel;
				}

				if ($item->id_tipo == "2") {
					$productos[$key] = Componente::model()->findByAttributes(array('materia_prima_fk' => $value))->cod_setecel;
				}
			}

			$cables = new Cable('search');
			$cables->unsetAttributes();
			if(isset($_GET['Listadocables']))
				$cables->attributes=$_GET['Listadocables'];

			$componentes=new Listadocomponentes('search');
			$componentes->unsetAttributes();
			if(isset($_GET['Listadocomponentes']))
				$componentes->attributes=$_GET['Listadocomponentes'];

			$this->render('cantidades',array(
				'model'=>$model,
				'materiaPrima'=>$materiaPrima,
				'cables'=>$cables,
				'componentes'=>$componentes,
				'productos'=>$productos,
			));
		}
	}

	public function actionRevision($id)
	{

		$model=$this->loadModel($id);

		if(isset($model))
		{
			echo "<pre>";
			print_r($model);
			echo "</pre>";
			die();
			$model->attributes=$_POST['Producto'];
			$materiaPrima = new MateriaPrima;
			$productos = array();

			foreach ($_POST['MateriaPrima'] as $key => $value) {
				$item = $materiaPrima->findByPk($key);
				if ($item->id_tipo == "1") {
					$productos[$key] = Cable::model()->findByAttributes(array('materia_prima_fk' => $value))->cod_setecel;
				}

				if ($item->id_tipo == "2") {
					$productos[$key] = Componente::model()->findByAttributes(array('materia_prima_fk' => $value))->cod_setecel;
				}
			}

			$cables = new Cable('search');
			$cables->unsetAttributes();
			if(isset($_GET['Listadocables']))
				$cables->attributes=$_GET['Listadocables'];

			$componentes=new Listadocomponentes('search');
			$componentes->unsetAttributes();
			if(isset($_GET['Listadocomponentes']))
				$componentes->attributes=$_GET['Listadocomponentes'];

			$this->render('cantidades',array(
				'model'=>$model,
				'materiaPrima'=>$materiaPrima,
				'cables'=>$cables,
				'componentes'=>$componentes,
				'productos'=>$productos,
			));
		}
	}

	public function actionCrear()
	{
		/*echo "<pre>";
		print_r($_POST);
		echo "</pre>";
		die();*/

		if(isset($_POST['Producto']))
		{
			$model=new Producto;
			$model->FechaAlta= date('Y-m-d');
			$model->attributes=$_POST['Producto'];

			if($model->save())
			{
				$id = $model->id;

				$detalle = $_POST['MateriaPrima'];
				$materiaPrima = new MateriaPrima;

				foreach ($detalle as $key => $value) {
					$detalleParte = new DetalleParte();
					$item = $materiaPrima->findByPk($key);

					$detalleParte->materia_prima_id = $key;
					$detalleParte->producto_id = $id;
					$detalleParte->numero_revision = 1;

					/*echo "<pre>".$value;
					print_r($item->id_tipo);
					echo "</pre>";
					die();*/


					if ($item->id_tipo == "1")
					{
						$detalleParte->cantidad_cable = $value;
					}elseif($item->id_tipo == "2")
					{
						$detalleParte->cantidad_comp = $value;
					}
					$detalleParte->save();

				}

				$this->redirect(array('view','id'=>$model->id));
			}

		}
	}


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$cables = new Cable('search');
		$cables->unsetAttributes();
		if(isset($_GET['Listadocables']))
			$cables->attributes=$_GET['Listadocables'];

		$componentes=new Listadocomponentes('search');
		$componentes->unsetAttributes();  // clear any default values
		if(isset($_GET['Listadocomponentes']))
			$componentes->attributes=$_GET['Listadocomponentes'];

		$materiaPrima = new MateriaPrima;

		$model=new Producto;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$model->FechaAlta= date('Y-m-d');

		if(isset($_POST['Producto']))
		{
			$model->attributes=$_POST['Producto'];
			$session = new CHttpSession;
			$session->open();
			$session['post'] = $_POST;

			if($model->save())
				$this->redirect(array('cantidades','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
			'materiaPrima'=>$materiaPrima,
			'cables'=>$cables,
			'componentes'=>$componentes,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Producto']))
		{
			$model->attributes=$_POST['Producto'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->redirect(array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{

		$model=new Producto('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Producto']))
			$model->attributes=$_GET['Producto'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Producto the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Producto::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Producto $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='producto-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
