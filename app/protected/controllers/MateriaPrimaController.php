	<?php

class MateriaPrimaController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(array('CrugeAccessControlFilter'));
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);

		if ($model->id_tipo == 1) {
			$cable = Cable::model()->find("materia_prima_fk = $id")->id;
			$this->redirect(array('cable/view','id'=>$cable));
		}elseif($model->id_tipo == 2){
			$componente = Componente::model()->find("materia_prima_fk = $id")->id;
			$this->redirect(array('componente/view','id'=>$componente));
		}

		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MateriaPrima;
		$cable = new Cable;
		$componente = new Componente;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MateriaPrima']))
		{
			$tipo = ($_POST['MateriaPrima']['id_tipo'] == 1) ? 'cable' : 'componente';
			$model->attributes=$_POST['MateriaPrima'];
			if($model->save())
				$this->redirect(array($tipo.'/create','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
			'cable'=>$cable,
			'componente'=>$componente,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MateriaPrima']))
		{
			$tipo = ($_POST['MateriaPrima']['id_tipo'] == 1) ? 'cable' : 'componente';
			$model->attributes=$_POST['MateriaPrima'];
			if($model->save())
				$this->redirect(array($tipo.'/update','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->redirect(array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MateriaPrima('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MateriaPrima']))
			$model->attributes=$_GET['MateriaPrima'];

		$cables=new Listadocables('search');
		$cables->unsetAttributes();  // clear any default values
		if(isset($_GET['Listadocables']))
			$cables->attributes=$_GET['Listadocables'];

		$componentes=new Listadocomponentes('search');
		$componentes->unsetAttributes();  // clear any default values
		if(isset($_GET['Listadocomponentes']))
			$componentes->attributes=$_GET['Listadocomponentes'];


		$this->render('admin',array(
			'model'=>$model,
			'cables' => $cables,
			'componentes' => $componentes,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MateriaPrima the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MateriaPrima::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MateriaPrima $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='materia-prima-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
