<?php

class StockController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(array('CrugeAccessControlFilter'));
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Stock;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$model->stock = 0;
		$model->stock_min = 0;
		$model->stock_max = 0;
		$model->materia_prima_id = $_GET["id"];
		if($model->save())
			$this->redirect(array('imagen/create','id'=>Componente::model()->findByAttributes(array('materia_prima_fk'=>$model->materia_prima_id))->id));

/*
		if(isset($_POST['Stock']))
		{
			$model->attributes=$_POST['Stock'];
			if($model->save())
			$this->redirect(array('imagen/create','id'=>Componente::model()->findByAttributes(array('materia_prima_fk'=>$model->materia_prima_id))->id));

		}

		$this->render('create',array(
			'model'=>$model,
		));*/
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCrearcable()
	{
		$model=new Stock;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$model->stock = 0;
		$model->stock_min = 0;
		$model->stock_max = 0;
		$model->materia_prima_id = $_GET["id"];
		if($model->save())
			$this->redirect(array('cable/view','id'=>Cable::model()->findByAttributes(array('materia_prima_fk'=>$model->materia_prima_id))->id));

		/*if(isset($_POST['Stock']))
		{
			$model->attributes=$_POST['Stock'];
			if($model->save())
			$this->redirect(array('cable/view','id'=>Cable::model()->findByAttributes(array('materia_prima_fk'=>$model->materia_prima_id))->id));

		}

		$this->render('create',array(
			'model'=>$model,
		));*/
	}


	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		//$model=$this->loadModel($id);

		$model = new Stock;
		$model = $model->findByAttributes(array('materia_prima_id'=> $id));

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Stock']))
		{
			$model->attributes=$_POST['Stock'];
			if($model->save()){
				$materiaPrima = MateriaPrima::model()->findByPk($id);
				if ($materiaPrima->id_tipo == 1) {
					$cable = Cable::model()->findByAttributes(array('materia_prima_fk'=>$materiaPrima->id))->id;
					$this->redirect(array('cable/view','id'=>$cable));
				}elseif ($materiaPrima->id_tipo == 2) {
					$componente = Componente::model()->findByAttributes(array('materia_prima_fk'=>$materiaPrima->id))->id;
					$this->redirect(array('componente/view','id'=>$componente));
				}
			}

				//$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Stock');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Stock('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Stock']))
			$model->attributes=$_GET['Stock'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Stock the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Stock::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Stock $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='stock-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
