<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sidebar.css">

		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/setecel.css">

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="#"><?php echo CHtml::encode(Yii::app()->name); ?></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse">
		<?php $this->widget('application.components.myMenu', array(
			'items'=>array(
				array('label'=>'Mazos', 'url'=>array('/Producto'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Materia Prima', 'url'=>array('/materiaPrima'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Presupuesto', 'url'=>array('/orden/presupuesto'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Pedido', 'url'=>array('/orden/pedido'), 'visible'=>!Yii::app()->user->isGuest),
				//array('label'=>'Cables', 'url'=>array('/Cable'), 'visible'=>!Yii::app()->user->isGuest),
				//array('label'=>'Componente', 'url'=>array('/Componente'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Proveedores', 'url'=>array('/Proveedor'), 'visible'=>!Yii::app()->user->isGuest),
				//array('label'=>'Imagenes', 'url'=>array('/imagen'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Clientes', 'url'=>array('/Cliente'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Administrar Usuarios'
				, 'url'=>Yii::app()->user->ui->userManagementAdminUrl
				, 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Login'
				, 'url'=>Yii::app()->user->ui->loginUrl
				, 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')'
				, 'url'=>Yii::app()->user->ui->logoutUrl
				, 'visible'=>!Yii::app()->user->isGuest),
			),
		)); ?>
      </div>
    </nav>

	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('application.components.myBreadcrumb', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

<div class="container-fluid">
	<div class="row no-gutters">
			<?php echo $content; ?>
	</div>
</div>

<footer class="footer">
	<div class="container">
		<span class="text-muted">
		<h6>Copyright &copy; <?php echo date('Y'); ?> by <a href='http://www.phpgroup.com.ar/' target="_BLANK">PHPGroup</a>. All Rights Reserved.</h6>
		</span>
	</div>
</footer>

 <?php // echo Yii::app()->user->ui->displayErrorConsole(); Más información aquí ?>
</body>
</html>
