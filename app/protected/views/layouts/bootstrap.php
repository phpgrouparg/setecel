<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="col-10">
	<?php echo $content; ?>
</div>
<div class="col-2">
	<div class="d-none d-xl-block col-xl-2 bd-toc">
		<?php
			$this->beginWidget('application.components.sideBarMenu', array(
				'items'=>$this->menu,
				'htmlOptions'=>array('class'=>'section-nav'),
			));
			$this->endWidget();
		?>
	</div>
</div>
<?php $this->endContent(); ?>