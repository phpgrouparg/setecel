<?php
/* @var $this OrdenDetalleController */
/* @var $model OrdenDetalle */

$this->breadcrumbs=array(
	'Orden Detalles'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List OrdenDetalle', 'url'=>array('index')),
	array('label'=>'Create OrdenDetalle', 'url'=>array('create')),
	array('label'=>'View OrdenDetalle', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage OrdenDetalle', 'url'=>array('admin')),
);
?>

<h1>Update OrdenDetalle <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>