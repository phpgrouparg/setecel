<?php
/* @var $this OrdenDetalleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Orden Detalles',
);

$this->menu=array(
	array('label'=>'Create OrdenDetalle', 'url'=>array('create')),
	array('label'=>'Manage OrdenDetalle', 'url'=>array('admin')),
);
?>

<h1>Orden Detalles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
