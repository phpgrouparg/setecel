<?php
/* @var $this OrdenDetalleController */
/* @var $model OrdenDetalle */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'orden-detalle-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos marcados con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'orden_id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'orden_id', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'orden_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'materia_prima_id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'materia_prima_id', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'materia_prima_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'cantidad', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cantidad', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'cantidad'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'total', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'total', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'total'); ?>
	</div>


	<button type="submit" class="btn btn-primary "><?php echo $model->isNewRecord ? 'Crear' : 'Guardar'; ?></button>
	<!-- <div class="row buttons">
		\n"; ?>
	</div> -->

<?php $this->endWidget(); ?>

</div><!-- form -->