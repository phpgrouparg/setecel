<?php
/* @var $this OrdenDetalleController */
/* @var $model OrdenDetalle */

$this->breadcrumbs=array(
	'Orden Detalles'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List OrdenDetalle', 'url'=>array('index')),
	array('label'=>'Create OrdenDetalle', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#orden-detalle-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Orden Detalles</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'orden-detalle-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'orden_id',
		'materia_prima_id',
		'cantidad',
		'total',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
