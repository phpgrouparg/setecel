<?php
/* @var $this OrdenDetalleController */
/* @var $model OrdenDetalle */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'id', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'orden_id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'orden_id', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'materia_prima_id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'materia_prima_id', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cantidad', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cantidad', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'total', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'total', array('class'=>'form-control')); ?>
	</div>

	<!-- <div class="row buttons">
		\n"; ?>
	</div> -->

	<button type="submit" class="btn btn-primary "><?php echo $model->isNewRecord ? 'Crear' : 'Guardar'</button>

<?php $this->endWidget(); ?>

</div><!-- search-form -->