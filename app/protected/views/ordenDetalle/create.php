<?php
/* @var $this OrdenDetalleController */
/* @var $model OrdenDetalle */

$this->breadcrumbs=array(
	'Orden Detalles'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List OrdenDetalle', 'url'=>array('index')),
	array('label'=>'Manage OrdenDetalle', 'url'=>array('admin')),
);
?>

<h1>Create OrdenDetalle</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>