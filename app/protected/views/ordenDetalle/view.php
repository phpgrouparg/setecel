<?php
/* @var $this OrdenDetalleController */
/* @var $model OrdenDetalle */

$this->breadcrumbs=array(
	'Orden Detalles'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List OrdenDetalle', 'url'=>array('index')),
	array('label'=>'Create OrdenDetalle', 'url'=>array('create')),
	array('label'=>'Update OrdenDetalle', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete OrdenDetalle', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage OrdenDetalle', 'url'=>array('admin')),
);
?>

<h1>View OrdenDetalle #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'orden_id',
		'materia_prima_id',
		'cantidad',
		'total',
	),
)); ?>
