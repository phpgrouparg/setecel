<?php
/* @var $this ProveedorController */
/* @var $model Proveedor */

$this->breadcrumbs=array(
	'Proveedors'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Listar Proveedor', 'url'=>array('index')),
	array('label'=>'Crear Proveedor', 'url'=>array('create')),
	array('label'=>'Actualizar Proveedor', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar Proveedor', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Actualizar Proveedor', 'url'=>array('admin')),
);
?>

<h1>Proveedor: <?php echo /*$model->id,*/$model->nombre; ?></h1>  <!--Preguntar a Omar-->

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nombre',
		'telefono',
		'email',
		'dni_cuil_cuit',
		'codigo_postal',
		'direccion',
		'localidad',
		'provincia',
		'barrio',
		'fecha_alta',
		'fecha_baja',
	),
)); ?>
