<?php
/* @var $this ProveedorController */
/* @var $model Proveedor */

$this->breadcrumbs=array(
	'Proveedors'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar Proveedor', 'url'=>array('index')),
	array('label'=>'Administrar Proveedor', 'url'=>array('admin')),
);
?>

<h1>Crear Proveedores</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>