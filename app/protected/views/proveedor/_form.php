<?php
/* @var $this ProveedorController */
/* @var $model Proveedor */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'proveedor-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos Con <span class="required">*</span> Son Requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'nombre', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>100,'maxlength'=>100,'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'nombre', array('class'=>'col-2 col-form-label')); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'telefono', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'telefono',array('size'=>50,'maxlength'=>50,'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'telefono', array('class'=>'col-2 col-form-label')); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'email', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50,'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'email', array('class'=>'col-2 col-form-label')); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'dni_cuil_cuit', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'dni_cuil_cuit', array('class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'dni_cuil_cuit', array('class'=>'col-2 col-form-label')); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'codigo_postal', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'codigo_postal',array('size'=>30,'maxlength'=>30,'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'codigo_postal', array('class'=>'col-2 col-form-label')); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'direccion', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'direccion',array('size'=>100,'maxlength'=>100,'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'direccion', array('class'=>'col-2 col-form-label')); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'localidad', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'localidad',array('size'=>50,'maxlength'=>50,'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'localidad', array('class'=>'col-2 col-form-label')); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'provincia', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'provincia',array('size'=>30,'maxlength'=>30,'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'provincia', array('class'=>'col-2 col-form-label')); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'barrio', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'barrio',array('size'=>30,'maxlength'=>30,'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'barrio', array('class'=>'col-2 col-form-label')); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'fecha_alta', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'fecha_alta', array('class'=>'form-control col-9','id'=>'FechaAlta','autocomplete'=>'off')); ?>
		<?php echo $form->error($model,'fecha_alta', array('class'=>'col-2 col-form-label')); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'fecha_baja', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'fecha_baja', array('class'=>'form-control col-9','id'=>'FechaBaja','autocomplete'=>'off')); ?>
		<?php echo $form->error($model,'fecha_baja', array('class'=>'col-2 col-form-label')); ?>
	</div>

	<div class="form-row">
		<div class="col-sm-10">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-primary')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
  $( function() {
    $( "#FechaAlta" ).datepicker({
    		dateFormat :"yy-mm-dd",
    		minDate: 0
    	});
    $( "#FechaBaja" ).datepicker({
    		dateFormat :"yy-mm-dd",
    		minDate: 0
    	});
  } );
  </script>