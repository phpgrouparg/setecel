<?php
/* @var $this DetalleParteController */
/* @var $model DetalleParte */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'detalle-parte-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos marcados con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'materia_prima_id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'materia_prima_id', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'materia_prima_id'); ?>
	</div>


		<?php echo $form->hiddenField($model,'producto_id', array('class'=>'form-control')); ?>
		


	<div class="form-group">
		<?php echo $form->labelEx($model,'numero_revision', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'numero_revision', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'numero_revision'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'cantidad_cable', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cantidad_cable', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'cantidad_cable'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'cantidad_comp', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cantidad_comp', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'cantidad_comp'); ?>
	</div>


	<button type="submit" class="btn btn-primary "><?php echo $model->isNewRecord ? 'Crear' : 'Guardar'; ?></button>


<?php $this->endWidget(); ?>

</div><!-- form -->