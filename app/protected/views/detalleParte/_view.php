<?php
/* @var $this DetalleParteController */
/* @var $data DetalleParte */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('materia_prima_id')); ?>:</b>
	<?php echo CHtml::encode($data->materia_prima_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('producto_id')); ?>:</b>
	<?php echo CHtml::encode($data->producto_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numero_revision')); ?>:</b>
	<?php echo CHtml::encode($data->numero_revision); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidad_cable')); ?>:</b>
	<?php echo CHtml::encode($data->cantidad_cable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidad_comp')); ?>:</b>
	<?php echo CHtml::encode($data->cantidad_comp); ?>
	<br />


</div>