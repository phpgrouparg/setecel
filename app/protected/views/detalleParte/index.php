<?php
/* @var $this DetalleParteController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Detalle Partes',
);

$this->menu=array(
	array('label'=>'Create DetalleParte', 'url'=>array('create')),
	array('label'=>'Manage DetalleParte', 'url'=>array('admin')),
);
?>

<h1>Detalle Partes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
