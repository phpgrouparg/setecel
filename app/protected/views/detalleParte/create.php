<?php
/* @var $this DetalleParteController */
/* @var $model DetalleParte */

$this->breadcrumbs=array(
	'Detalle Partes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DetalleParte', 'url'=>array('index')),
	array('label'=>'Manage DetalleParte', 'url'=>array('admin')),
);
?>

<h2>Crear Detalle Para Producto N°: <?php echo $model->producto_id;?> - <?php echo $producto->Diseno;?></h2>

<?php $this->renderPartial('_form', array('model'=>$model, 'mpmodel'=>$mpmodel)); ?>