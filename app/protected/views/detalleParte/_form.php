<?php
/* @var $this DetalleParteController */
/* @var $model DetalleParte */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'detalle-parte-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'numero_revision', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'numero_revision', array('class'=>'col-sm-4 form-control-sm','readonly'=>'readonly')); ?>
		<?php echo $form->error($model,'numero_revision'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Nueva Revision', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php //echo $form->checkBox($model, 'nueva_revision', array('class'=>'col-sm-4 form-control-sm','readonly'=>'readonly')); ?>
		<input type="checkBox" name="nueva_revision" id="nueva_revision" value="false">
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'materia_prima_id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->hiddenField($model,'materia_prima_id', array('class'=>'col-sm-4 form-control-sm','readonly'=>'readonly')); ?>
		<?php echo $form->textField($mpmodel,'Descripcion', array('class'=>'col-sm-4 form-control-sm','readonly'=>'readonly')); ?>
		<?php echo $form->error($model,'materia_prima_id'); ?>
	</div>

	<?php echo $form->hiddenField($model,'producto_id', array('class'=>'form-control')); ?>

	<?php
	if ($mpmodel->id_tipo == 1) {
	?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'cantidad_cable', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cantidad_cable', array('class'=>'col-sm-2 form-control-sm')); ?>
		<?php echo $form->error($model,'cantidad_cable'); ?>
	</div>

	<?php }elseif($mpmodel->id_tipo	== 2){ ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'cantidad_comp', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cantidad_comp', array('class'=>'col-sm-2 form-control-sm')); ?>
		<?php echo $form->error($model,'cantidad_comp'); ?>
	</div>

	<?php } ?>

	<button type="submit" class="btn btn-primary "><?php echo $model->isNewRecord ? 'Crear' : 'Guardar'; ?></button>


<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
	$( document ).ready(function() {
		if ($("#nueva_revision").val() == null) {
			$("#nueva_revision").val(1);
		}
		$("#nueva_revision").click(function(){
			if ($("#nueva_revision").is(':checked')){
				$("#DetalleParte_numero_revision").val(parseInt($("#DetalleParte_numero_revision").val())+1);
			}else{
				$("#DetalleParte_numero_revision").val(parseInt($("#DetalleParte_numero_revision").val())-1);
			}
		});
	});
</script>