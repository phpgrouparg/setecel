<?php
/* @var $this DetalleParteController */
/* @var $model DetalleParte */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'id', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'materia_prima_id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'materia_prima_id', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'producto_id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'producto_id', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numero_revision', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'numero_revision', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cantidad_cable', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cantidad_cable', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cantidad_comp', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cantidad_comp', array('class'=>'form-control')); ?>
	</div>

	<!-- <div class="row buttons">
		\n"; ?>
	</div> -->

	<button type="submit" class="btn btn-primary "><?php echo $model->isNewRecord ? 'Crear' : 'Guardar'</button>

<?php $this->endWidget(); ?>

</div><!-- search-form -->