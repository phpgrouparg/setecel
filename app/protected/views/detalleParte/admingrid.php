<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'detalle-parte-grid',
	'summaryText' => '',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		//'id',
		'numero_revision',
		'materiaPrima.Descripcion',
		//'materia_prima_id',
		//'producto_id',
		'cantidad_cable',
		'cantidad_comp',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
			'buttons'=>array
			(
				'view' => array
				(
					'label'=>'Agregar',
					'url'=>'Yii::app()->createUrl("materiaPrima/view", array("id"=>$data->materia_prima_id))',
				),
			),
		),
	),
)); ?>