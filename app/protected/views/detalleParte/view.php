<?php
/* @var $this DetalleParteController */
/* @var $model DetalleParte */

$this->breadcrumbs=array(
	'Detalle Partes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List DetalleParte', 'url'=>array('index')),
	array('label'=>'Create DetalleParte', 'url'=>array('create')),
	array('label'=>'Update DetalleParte', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DetalleParte', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DetalleParte', 'url'=>array('admin')),
);
?>

<h1>View DetalleParte #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'materia_prima_id',
		'producto_id',
		'numero_revision',
		'cantidad_cable',
		'cantidad_comp',
	),
)); ?>