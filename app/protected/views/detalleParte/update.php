<?php
/* @var $this DetalleParteController */
/* @var $model DetalleParte */

$this->breadcrumbs=array(
	'Detalle Partes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DetalleParte', 'url'=>array('index')),
	array('label'=>'Create DetalleParte', 'url'=>array('create')),
	array('label'=>'View DetalleParte', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DetalleParte', 'url'=>array('admin')),
);
?>

<h1>Update DetalleParte <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>