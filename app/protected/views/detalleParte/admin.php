<?php
/* @var $this DetalleParteController */
/* @var $model DetalleParte */

$this->breadcrumbs=array(
	'Detalle Partes'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List DetalleParte', 'url'=>array('index')),
	array('label'=>'Create DetalleParte', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#detalle-parte-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Detalle Partes</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'detalle-parte-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'materia_prima_id',
		'producto_id',
		'numero_revision',
		'cantidad_cable',
		'cantidad_comp',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
