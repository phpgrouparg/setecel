<?php
/* @var $this ComponenteController */
/* @var $model Componente */

$this->breadcrumbs=array(
	'Componentes'=>array('../materiaPrima'),
	'Crear',
);

$this->menu=array(
	//array('label'=>'Listar Componente', 'url'=>array('index')),
	array('label'=>'Administrar Componente', 'url'=>array('admin')),
);
?>

<h1>Crear Componente</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>