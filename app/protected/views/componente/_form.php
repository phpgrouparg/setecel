<?php
/* @var $this ComponenteController */
/* @var $model Componente */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'componente-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos Con <span class="required">*</span> Son Requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'cod_setecel', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cod_setecel',array('size'=>30,'maxlength'=>30, 'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'cod_setecel'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'id_categoria', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->dropDownList($model,'id_categoria', CHtml::listData(Categoria::model()->findAll(), 'id', 'nombre'), array('class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'id_categoria'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'terminal_asociado', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'terminal_asociado', array('class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'terminal_asociado'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'precio', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'precio', array('class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'precio'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'cod_proveedor', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cod_proveedor',array('size'=>30,'maxlength'=>30, 'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'cod_proveedor'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'packaging', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'packaging', array('class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'packaging'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'cod_origen', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cod_origen',array('size'=>30,'maxlength'=>30, 'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'cod_origen'); ?>
	</div>

	<div class="form-group row">
		<!--<?<?php echo $form->labelEx($model,'materia_prima_fk', array('class'=>'col-2 col-form-label')); ?>-->
		<?php echo $form->hiddenField($model,'materia_prima_fk', array('class'=>'form-control col-9','readonly'=>'readonly')); ?>
		<?php echo $form->error($model,'materia_prima_fk'); ?>
	</div>
<!--
	<div class="form-row">
		<div class="col-sm-10">
		<?php// echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
		</div>
	</div>
-->
	<div class="form-row">
		<div class="col-sm-10">
		<button type="submit" class="btn btn-primary "><?php echo $model->isNewRecord ? 'Crear' : 'Guardar' ?></button>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->