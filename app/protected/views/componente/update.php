<?php
/* @var $this ComponenteController */
/* @var $model Componente */

$this->breadcrumbs=array(
	'Componentes'=>array('../materiaPrima'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	//array('label'=>'Listar Componente', 'url'=>array('index')),
	array('label'=>'Crear Componente', 'url'=>array('create')),
	//array('label'=>'Ver Componente', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Materia Prima', 'url'=>array('materiaprima')),
);
?>

<h1>Actualizar Componente <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>