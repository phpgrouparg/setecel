<?php
/* @var $this ComponenteController */
/* @var $data Componente */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cod_setecel')); ?>:</b>
	<?php echo CHtml::encode($data->cod_setecel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_categoria')); ?>:</b>
	<?php echo CHtml::encode($data->id_categoria); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('terminal_asociado')); ?>:</b>
	<?php echo CHtml::encode($data->terminal_asociado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('precio')); ?>:</b>
	<?php echo CHtml::encode($data->precio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cod_proveedor')); ?>:</b>
	<?php echo CHtml::encode($data->cod_proveedor); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('packaging')); ?>:</b>
	<?php echo CHtml::encode($data->packaging); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cod_origen')); ?>:</b>
	<?php echo CHtml::encode($data->cod_origen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('materia_prima_fk')); ?>:</b>
	<?php echo CHtml::encode($data->materia_prima_fk); ?>
	<br />

	*/ ?>

</div>