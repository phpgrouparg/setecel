<?php
/* @var $this ComponenteController */
/* @var $model Componente */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cod_setecel'); ?>
		<?php echo $form->textField($model,'cod_setecel',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_categoria'); ?>
		<?php echo $form->textField($model,'id_categoria'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'terminal_asociado'); ?>
		<?php echo $form->textField($model,'terminal_asociado'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'precio'); ?>
		<?php echo $form->textField($model,'precio'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cod_proveedor'); ?>
		<?php echo $form->textField($model,'cod_proveedor',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'packaging'); ?>
		<?php echo $form->textField($model,'packaging'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cod_origen'); ?>
		<?php echo $form->textField($model,'cod_origen',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'materia_prima_fk'); ?>
		<?php echo $form->textField($model,'materia_prima_fk'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->