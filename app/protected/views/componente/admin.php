<?php
/* @var $this ComponenteController */
/* @var $model Componente */

$this->breadcrumbs=array(
	'Componentes'=>array('../materiaPrima'),
	'Manage',
);

$this->menu=array(
	//array('label'=>'Listar Componente', 'url'=>array('index')),
	array('label'=>'Crear Componente', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#componente-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Componentes</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'componente-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'cod_setecel',
		'id_categoria',
		'terminal_asociado',
		'precio',
		/*
		'cod_proveedor',
		'packaging',
		'cod_origen',
		'materia_prima_fk',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
