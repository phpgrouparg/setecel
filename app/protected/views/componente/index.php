<?php
/* @var $this ComponenteController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Componentes',
);

$this->menu=array(
	array('label'=>'Crear Componente', 'url'=>array('create')),
	array('label'=>'Administrar Componente', 'url'=>array('materiaprima')),
);
?>

<h1>Componentes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
