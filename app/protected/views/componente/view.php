<?php
/* @var $this ComponenteController */
/* @var $model Componente */

$this->breadcrumbs=array(
	'Materia Prima'=>array('../materiaPrima'),
	'Componentes #'.$model->id,
);

$this->menu=array(
	//array('label'=>'Listar Componente', 'url'=>array('index')),
	//array('label'=>'Crear Componente', 'url'=>array('create')),
	//array('label'=>'Actualizar Componente', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Cargar Imagen', 'url'=>array('imagen/create', 'id'=>$model->id)),
	//array('label'=>'Borrar Componente', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Volver a Materia Prima', 'url'=>array('materiaPrima/admin')),
);
?>

<h1>Componente Codigo: <?php echo $model->cod_setecel;?></h1>
<!--<h1>Componente <?php echo $model->id;?></h1>-->

<?php $this->widget('zii.widgets.CDetailView', array(
	'cssFile' => Yii::app()->request->baseUrl . '/css/MyCDetailView.css',
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'cod_setecel',
		//'id_categoria',
		'componente_cat.nombre',
		'materiaPrimaFk.Inactivo',
		'materiaPrimaFk.Ubicacion',
		'terminal_asociado',
		'precio',
		'cod_proveedor',
		'packaging',
		'cod_origen',
		//'materia_prima_fk',
	),
)); ?>

<table class='detail-view'>
<?php
$this->beginWidget('EGalleria');
?>
<ul>
	<?php
	foreach ($pics as $key => $value) {
		echo "<a href='/imagen/".$value->id."?componente=".$model->id."'><img src='".Yii::app()->params['setecelUrl']."images/".$value->path."' /></a>";
	}
	?>
</ul>
<?php
$this->endWidget();
?>
</table>