<?php
/* @var $this ProductoController */
/* @var $model Producto */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'producto-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'action'=>Yii::app()->createUrl('/producto/cantidades'),
	'enableAjaxValidation'=>false,
)); ?>

	<p >  </p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'Diseno', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'Diseno',array('size'=>60,'maxlength'=>100, 'class'=>'form-control form-control-sm col-9')); ?>
		<?php echo $form->error($model,'Diseno'); ?>
	</div>
	<?php echo $form->hiddenField($model,'FechaAlta', array('class'=>'form-control col-9')); ?>
	<?php echo $form->hiddenField($model,'FechaBaja', array('class'=>'form-control col-9')); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'Observaciones', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'Observaciones',array('size'=>100,'maxlength'=>100, 'class'=>'form-control form-control-sm col-9')); ?>
		<?php echo $form->error($model,'Observaciones'); ?>
	</div>

	<div class="form-group">
		<div class="row">
			<div class="col">
				<?php

				$cables = CHtml::listData(Cable::model()->findAll(), 'materia_prima_fk', 'cod_setecel');

				$componentes = CHtml::listData(Componente::model()->findAll(), 'materia_prima_fk', 'cod_setecel');

				$data['cables'] = $cables;
				$data['componentes'] = $componentes;

				echo $form->listBox($materiaPrima,'id',$data,array('size'=>'10', 'class'=>'form-control'));

				?>
			</div>
			<div class="col-1">
				<div class="row m-2">
					<button type="button" class="btn btn-primary btn-sm" name="returnone" id="returnone"><</button>
				</div>
				<div class="row m-2">
					<button type="button" class="btn btn-primary btn-sm" name="returnall" id="returnall"><<</button>
				</div>
				<div class="row m-2">
					<button type="button" class="btn btn-primary btn-sm" name="passall" id="passall">>></button>
				</div>
				<div class="row m-2">
					<button type="button" class="btn btn-primary btn-sm" name="passone" id="passone">></button>
				</div>
			</div>
			<div class="col">
				<select name="Producto[mazo]" id="mazo" size="10" class="form-control">
					<optgroup label="cables">
					</optgroup>
					<optgroup label="componentes">
					</optgroup>
				</select>
			</div>
		</div>
		<div class="d-none" id="hidden"></div>
	</div>

	<button type="submit" class="btn btn-primary "><?php echo $model->isNewRecord ? 'Crear' : 'Guardar'; ?></button>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
$( document ).ready(function(){

	$("#returnone").click(function(){
		if ($("#mazo :selected").val()) {
			addItem($("#mazo :selected").val(),
				$("#mazo :selected").text(),
				$("#mazo :selected").parent().attr('label'),
				"MateriaPrima_id");
			console.log($("#mazo :selected").text());
			$("#mazo :selected").remove();
		}else{
			console.log("vacio");
		}
	});

	$("#returnall").click(function(){
		$("#mazo optgroup[label=cables] option").each(function(index, element){
			addItem(element.value, element.text, 'cables', "MateriaPrima_id")
			$(this).remove();
		});
		$("#mazo optgroup[label=componentes] option").each(function(index, element){
			addItem(element.value, element.text, 'componentes', "MateriaPrima_id")
			$(this).remove();
		});
	});

	$("#passone").click(function(){
		if ($("#MateriaPrima_id :selected").val()) {
			addItem($("#MateriaPrima_id :selected").val(),
				$("#MateriaPrima_id :selected").text(),
				$("#MateriaPrima_id :selected").parent().attr('label'),
				"mazo");
			console.log($("#MateriaPrima_id :selected").text());
			$("#MateriaPrima_id :selected").remove();
		}else{
			console.log("vacio");
		}
	});

	$("#passall").click(function(){
		$("#MateriaPrima_id optgroup[label=cables] option").each(function(index, element){
			addItem(element.value, element.text, 'cables', 'mazo')
			$(this).remove();
		});
		$("#MateriaPrima_id optgroup[label=componentes] option").each(function(index, element){
			addItem(element.value, element.text, 'componentes', 'mazo')
			$(this).remove();
		});
	});
});

function addItem(id, text, parent, select){
	$("#"+select+" optgroup[label="+parent+"]").append("<option value='" + id + "'>" + text + "</option>");
	if (select == "mazo") {
		$("#hidden").append("<input type='hidden' name='MateriaPrima["+id+"]' id="+id+" value="+id+">");
	}else{
		$("#"+id).remove();
	}
}

</script>