<?php
/* @var $this ProductoController */
/* @var $model Producto */

$this->breadcrumbs=array(
	'Productos'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List Producto', 'url'=>array('index')),
	array('label'=>'Administrar Producto', 'url'=>array('admin')),
);
?>

<h1>Crear Producto</h1>

<?php
/* @var $this ProductoController */
/* @var $model Producto */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'producto-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'action'=>Yii::app()->createUrl('/producto/crear'),
	'enableAjaxValidation'=>false,
	"clientOptions"=>array(
		"validateOnSubmit"=>true,
	),
)); ?>

	<p >  </p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'Diseno', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'Diseno',array('size'=>60,'maxlength'=>100, 'class'=>'form-control form-control-sm col-9', 'required'=>'required')); ?>
		<?php echo $form->error($model,'Diseno'); ?>
	</div>
	<?php echo $form->hiddenField($model,'FechaAlta', array('class'=>'form-control col-9')); ?>
	<?php echo $form->hiddenField($model,'FechaBaja', array('class'=>'form-control col-9')); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'Observaciones', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'Observaciones',array('size'=>100,'maxlength'=>100, 'class'=>'form-control form-control-sm col-9', 'required'=>'required')); ?>
		<?php echo $form->error($model,'Observaciones'); ?>
	</div>

		<?php
			foreach ($productos as $key => $value) {
				echo '<div class="form-group row">';
				echo '<label class="col-sm-2 col-form-label required">'.$value.' <span class="required">*</span></label>';
				echo '<input type="number" name="MateriaPrima['.$key.']" placeholder="Ingrese la cantidad" class="form-control form-control-sm col-9" required>';
				echo '</div>';
			}
		?>

	<button type="submit" class="btn btn-primary "><?php echo $model->isNewRecord ? 'Crear' : 'Guardar'; ?></button>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">

$( document ).ready(function(){

});

</script>