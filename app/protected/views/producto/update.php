<?php
/* @var $this ProductoController */
/* @var $model Producto */

$this->breadcrumbs=array(
	'Productos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Producto', 'url'=>array('index')),
	array('label'=>'Crear Producto', 'url'=>array('create')),
	array('label'=>'Ver Producto', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Producto', 'url'=>array('admin')),
);
?>

<h1>Modificar Producto <?php echo $model->id; ?></h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'producto-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p >  </p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'Diseno', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'Diseno',array('size'=>60,'maxlength'=>100, 'class'=>'form-control form-control-sm col-9')); ?>
		<?php echo $form->error($model,'Diseno'); ?>
	</div>
	<?php echo $form->hiddenField($model,'FechaAlta', array('class'=>'form-control col-9')); ?>
	<?php echo $form->hiddenField($model,'FechaBaja', array('class'=>'form-control col-9')); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'Observaciones', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'Observaciones',array('size'=>100,'maxlength'=>100, 'class'=>'form-control form-control-sm col-9')); ?>
		<?php echo $form->error($model,'Observaciones'); ?>
	</div>
	<button type="submit" class="btn btn-primary "><?php echo $model->isNewRecord ? 'Crear' : 'Guardar'; ?></button>

<?php $this->endWidget(); ?>

</div>