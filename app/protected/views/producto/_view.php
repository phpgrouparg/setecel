<?php
/* @var $this ProductoController */
/* @var $data Producto */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Diseno')); ?>:</b>
	<?php echo CHtml::encode($data->Diseno); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaAlta')); ?>:</b>
	<?php echo CHtml::encode($data->FechaAlta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaBaja')); ?>:</b>
	<?php echo CHtml::encode($data->FechaBaja); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Observaciones')); ?>:</b>
	<?php echo CHtml::encode($data->Observaciones); ?>
	<br />


</div>