<?php
/* @var $this ProductoController */
/* @var $model Producto */

$this->breadcrumbs=array(
	'Productos'=>array('index'),
	$model->id,
);

$this->menu=array(
	//array('label'=>'List Producto', 'url'=>array('index')),
	//array('label'=>'Create Producto', 'url'=>array('create')),
	//array('label'=>'Update Producto', 'url'=>array('update', 'id'=>$model->id)),
	//array('label'=>'Delete Producto', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Producto', 'url'=>array('admin')),
	array('label'=>'Nueva Revision', 'url'=>array('revision', 'id'=>$model->id)),
	//array('label'=>'Crear Detalle', 'url'=>array('detalleParte/create','id'=>$model->id)),
);
?>

<h3>Producto #<?php echo $model->id; ?> - <?php echo $model->Diseno; ?></h3>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		//'Diseno',
		//'FechaAlta',
		//'FechaBaja',
		'Observaciones',
	),
)); ?>
<br>
<?php

/*$this->widget('CTabView', array(
    'tabs'=>array(
        'tab1'=>array(
            'title'=>'Detalle',
            'view'=>'../detalleParte/admingrid',
            'data'=>array('model'=>$detalleParte),
        ),
        'tab2'=>array(
            'title'=>'Agregar Materia Prima',
            'view'=>'buscarmateriaprima',
            'data'=>array('cables'=>$cables, 'componentes'=>$componentes),
        ),
    ),
));*/

?>

<?php $this->renderPartial('../viewDetalleparte/admingrid', array('model'=>$detalleParte,'materiaPrima'=>$materiaPrima)); ?>
