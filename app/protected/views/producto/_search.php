<?php
/* @var $this ProductoController */
/* @var $model Producto */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'id', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Diseno', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'Diseno',array('size'=>60,'maxlength'=>100, 'class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'FechaAlta', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'FechaAlta', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'FechaBaja', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'FechaBaja', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Observaciones', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'Observaciones',array('size'=>11,'maxlength'=>11, 'class'=>'form-control')); ?>
	</div>

	<!-- <div class="row buttons">
		\n"; ?>
	</div> -->

	<button type="submit" class="btn btn-primary "><?php echo $model->isNewRecord ? 'Crear' : 'Guardar'</button>

<?php $this->endWidget(); ?>

</div><!-- search-form -->