<?php

$this->widget('CTabView', array(
    'tabs'=>array(
        'tabcable'=>array(
            'title'=>'Cables',
            'view'=>'../listadocables/cablestabsbuscar',
            'data'=>array('model'=>$cables),
        ),
        'tabcomponente'=>array(
            'title'=>'Componentes',
            'view'=>'../listadocomponentes/componentestabsbuscar',
            'data'=>array('model'=>$componentes),
        ),
    ),
));

?>