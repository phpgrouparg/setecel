	<?php
/* @var $this CableController */
/* @var $model Cable */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cable-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos Con <span class="required">*</span> Son Requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'cod_setecel', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cod_setecel',array('size'=>30,'maxlength'=>30, 'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'cod_setecel'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'color_base', array('class'=>'col-2 col-form-label')); ?>
		 <select class="form-control col-9" name="Cable[color_base]">
           <option value='Amarillo'>Amarillo</option>
           <option value='Azul'>Azul</option>
           <option value='Blanco'>Blanco</option>
           <option value='Celeste'>Celeste</option>
           <option value='Gris'>Gris</option>
           <option value='Marron'>Marron</option>
           <option value='Negro'>Negro</option>
           <option value='Negro-Azul'>Negro-Azul</option>
           <option value='Rojo'>Rojo</option>
           <option value='Rosado'>Rosado</option>
           <option value='Verde'>Verde</option>
           <option value='Verde-Claro'>Verde-Claro</option>
           <option value='Violeta'>Violeta</option>
           <option value='Ninguno'>Ninguno</option>
       </select>

		<?php echo $form->error($model,'color_base'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'color_linea', array('class'=>'col-2 col-form-label')); ?>
		<?php //echo $form->textField($model,'color_linea',array('size'=>30,'maxlength'=>30, 'class'=>'form-control col-9')); ?>
		<select class="form-control col-9" name="Cable[color_linea]">
           <option value='Amarillo'>Amarillo</option>
           <option value='Azul'>Azul</option>
           <option value='Blanco'>Blanco</option>
           <option value='Gris'>Gris</option>
           <option value='Marron'>Marron</option>
           <option value='Negro'>Negro</option>
           <option value='Rojo'>Rojo</option>
           <option value='Verde'>Verde</option>
           <option value='Violeta'>Violeta</option>
           <option value='Ninguno'>Ninguno</option>
       </select>
		<?php echo $form->error($model,'color_linea'); ?>
	</div>

	<div class="form-group row">
       <?php echo $form->labelEx($model,'seccion', array('class'=>'col-2 col-form-label')); ?>
       <?php //echo $form->textField($model,'seccion', array('class'=>'form-control col-9')); ?>
       <select class="form-control col-9" name="Cable[seccion]">
           <option value='0.35'>0.35</option>
           <option value='0.50'>0.50</option>
           <option value='0.75'>0.75</option>
           <option value='1.00'>1.00</option>
           <option value='1.50'>1.50</option>
           <option value='2.00'>2.00</option>
           <option value='2.50'>2.50</option>
           <option value='4.00'>4.00</option>
           <option value='6.00'>6.00</option>
           <option value='1'>2X0.5</option>
       </select>
   
       <?php echo $form->error($model,'seccion'); ?>
   </div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'packaging', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'packaging', array('class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'packaging'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'cod_proveedor', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cod_proveedor', array('size'=>30,'maxlength'=>30, 'class' => 'form-control col-9')); ?>
		<?php echo $form->error($model,'cod_proveedor'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'precio', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'precio', array('class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'precio'); ?>
	</div>

	<div class="form-group row">
		<!--<?php echo $form->labelEx($model,'materia_prima_fk', array('class'=>'col-2 col-form-label')); ?>-->
		<?php echo $form->hiddenField($model,'materia_prima_fk', array('class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'materia_prima_fk'); ?>
	</div>

	<div class="form-row">
		<div class="col-sm-10">
		<button type="submit" class="btn btn-primary "><?php echo $model->isNewRecord ? 'Crear' : 'Guardar' ?></button>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->