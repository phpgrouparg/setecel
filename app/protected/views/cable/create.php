<?php
/* @var $this CableController */
/* @var $model Cable */

$this->breadcrumbs=array(
	'Cables'=>array('../materiaPrima'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Administrar Cable', 'url'=>array('admin')),
);
?>

<h1>Cargar Cable</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>