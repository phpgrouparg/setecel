<?php
/* @var $this CableController */
/* @var $model Cable */

$this->breadcrumbs=array(
	'Cables'=>array('../materiaPrima'),
	$model->id=>array('view','id'=>$model->id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Crear Cable', 'url'=>array('create')),
	array('label'=>'Ver Cable', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Cable', 'url'=>array('admin')),
);
?>

<h1>Actualizar Cable <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>