<?php
/* @var $this CableController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cables',
);

$this->menu=array(
	array('label'=>'Crear Cable', 'url'=>array('create')),
	array('label'=>'Administrar Cable', 'url'=>array('admin')),
);
?>

<h1>Cables</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
