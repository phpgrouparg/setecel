<?php
/* @var $this CableController */
/* @var $model Cable */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cod_setecel'); ?>
		<?php echo $form->textField($model,'cod_setecel',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'color_base'); ?>
		<?php echo $form->textField($model,'color_base',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'color_linea'); ?>
		<?php echo $form->textField($model,'color_linea',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'seccion'); ?>
		<?php echo $form->textField($model,'seccion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'packaging'); ?>
		<?php echo $form->textField($model,'packaging'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cod_proveedor'); ?>
		<?php echo $form->textField($model,'cod_proveedor',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'precio'); ?>
		<?php echo $form->textField($model,'precio'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'materia_prima_fk'); ?>
		<?php echo $form->textField($model,'materia_prima_fk'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->