<?php
/* @var $this CableController */
/* @var $data Cable */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cod_setecel')); ?>:</b>
	<?php echo CHtml::encode($data->cod_setecel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('color_base')); ?>:</b>
	<?php echo CHtml::encode($data->color_base); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('color_linea')); ?>:</b>
	<?php echo CHtml::encode($data->color_linea); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seccion')); ?>:</b>
	<?php echo CHtml::encode($data->seccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('packaging')); ?>:</b>
	<?php echo CHtml::encode($data->packaging); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cod_proveedor')); ?>:</b>
	<?php echo CHtml::encode($data->cod_proveedor); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('precio')); ?>:</b>
	<?php echo CHtml::encode($data->precio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('materia_prima_fk')); ?>:</b>
	<?php echo CHtml::encode($data->materia_prima_fk); ?>
	<br />

	*/ ?>

</div>