<?php
/* @var $this CableController */
/* @var $model Cable */

$this->breadcrumbs=array(
	'Materia Prima'=>array('../materiaPrima'),
	'Cable #'.$model->id,
);

$this->menu=array(
	/*array('label'=>'Listar Cable', 'url'=>array('index')),
	array('label'=>'Crear Cable', 'url'=>array('create')),
	array('label'=>'Actualizar Cable', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Cable', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	*/
	array('label'=>'Volver a Materia Prima', 'url'=>array('materiaPrima/admin')),
);
?>

<!--<h1>Cable #<?//php echo $model->id; ?></h1>-->
<h1>Cable Codigo:<?php echo $model->cod_setecel;?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'cod_setecel',
		'color_base',
		'color_linea',
		'seccion',
		'packaging',
		'cod_proveedor',
		'precio',
		//'materia_prima_fk',
		'materia_fk.Inactivo',
		'materia_fk.Ubicacion',
	),
)); ?>
