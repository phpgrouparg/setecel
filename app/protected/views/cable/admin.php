<?php
/* @var $this CableController */
/* @var $model Cable */

$this->breadcrumbs=array(
	'Cables'=>array('../materiaPrima'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Crear Cable', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#cable-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Cables</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cable-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'cod_setecel',
		'color_base',
		'color_linea',
		'seccion',
		'packaging',
		'cod_proveedor',
		'precio',
		/*
		'materia_prima_fk',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
