<?php
/* @var $this ImagenController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Imagens',
);

$this->menu=array(
	array('label'=>'Crear Imagen', 'url'=>array('create')),
	array('label'=>'Admin Imagen', 'url'=>array('admin')),
);
?>

<h1>Imagenes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
