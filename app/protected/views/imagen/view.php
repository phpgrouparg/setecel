<?php
/* @var $this ImagenController */
/* @var $model Imagen */

$this->breadcrumbs=array(
	'Imagenes'=>array('index'),
	$model->id,
);

$this->menu=array(
	//array('label'=>'List Imagen', 'url'=>array('index')),
	//array('label'=>'Create Imagen', 'url'=>array('create')),
	array('label'=>'Volver', 'url'=>array('componente/view', 'id'=>$_GET['componente'])),
	array('label'=>'Borrar Imagen', 'url'=>array('delete', 'id'=>$model->id, 'componente'=>$_GET['componente']), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>'Manage Imagen', 'url'=>array('admin')),
);
?>

<h1>Imagen N°: <?php echo $model->id; ?></h1>

<img src="<?php echo $model->path ?>">
