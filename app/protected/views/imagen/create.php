<?php
/* @var $this ImagenController */
/* @var $model Imagen */

$this->breadcrumbs=array(
	'Imagens'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar Imagen', 'url'=>array('index')),
	array('label'=>'Administrar Imagen', 'url'=>array('admin')),
);
?>

<h1>Subir Imagen</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>