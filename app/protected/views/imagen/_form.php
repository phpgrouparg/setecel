<?php
/* @var $this ImagenController */
/* @var $model Imagen */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'imagen-form',
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p>  </p>

	<?php echo $form->errorSummary($model); ?>
	<?php echo $form->hiddenField($model,'id_pieza', array('class'=>'form-control')); ?>
	<div class="form-group row">
		<?php echo $form->labelEx($model,'<strong> ruta imagen </strong>', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->fileField($model,'path',array('size'=>30,'maxlength'=>30, 'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'path'); ?>
	</div>

	<div class="form-row">
		<div class="col-sm-10">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar', array('class'=>'btn btn-primary')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->