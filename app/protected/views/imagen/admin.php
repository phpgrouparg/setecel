<?php
/* @var $this ImagenController */
/* @var $model Imagen */

$this->breadcrumbs=array(
	'Imagens'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Listar Imagen', 'url'=>array('index')),
	array('label'=>'Crear Imagen', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#imagen-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Imagens</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'imagen-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_pieza',
		'path',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
