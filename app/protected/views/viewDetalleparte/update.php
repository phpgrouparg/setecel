<?php
/* @var $this ViewDetalleparteController */
/* @var $model ViewDetalleparte */

$this->breadcrumbs=array(
	'View Detallepartes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ViewDetalleparte', 'url'=>array('index')),
	array('label'=>'Create ViewDetalleparte', 'url'=>array('create')),
	array('label'=>'View ViewDetalleparte', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ViewDetalleparte', 'url'=>array('admin')),
);
?>

<h1>Update ViewDetalleparte <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>