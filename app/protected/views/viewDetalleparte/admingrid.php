
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'view-detalleparte-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		//'id',
		//'materia_prima_id',
		//'id_tipo',
		//'elemento_id',
		//'producto_id',
		'numero_revision',
		'descripcion',
		'cod_setecel',
		'cantidad_cable',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
			'buttons'=>array
			(
				'view' => array
				(
					'label'=>'ver',
					'url'=>'Yii::app()->createUrl("materiaPrima/view", array("id"=>$data->materia_prima_id))',
					'options' => array('target' => '_new'),
				),
			),
		),
	),
)); ?>
