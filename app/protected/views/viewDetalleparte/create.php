<?php
/* @var $this ViewDetalleparteController */
/* @var $model ViewDetalleparte */

$this->breadcrumbs=array(
	'View Detallepartes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ViewDetalleparte', 'url'=>array('index')),
	array('label'=>'Manage ViewDetalleparte', 'url'=>array('admin')),
);
?>

<h1>Create ViewDetalleparte</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>