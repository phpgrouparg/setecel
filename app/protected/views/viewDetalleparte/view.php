<?php
/* @var $this ViewDetalleparteController */
/* @var $model ViewDetalleparte */

$this->breadcrumbs=array(
	'View Detallepartes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ViewDetalleparte', 'url'=>array('index')),
	array('label'=>'Create ViewDetalleparte', 'url'=>array('create')),
	array('label'=>'Update ViewDetalleparte', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ViewDetalleparte', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ViewDetalleparte', 'url'=>array('admin')),
);
?>

<h1>View ViewDetalleparte #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'materia_prima_id',
		'id_tipo',
		'elemento_id',
		'producto_id',
		'numero_revision',
		'descripcion',
		'cod_setecel',
		'cantidad_cable',
	),
)); ?>
