<?php
/* @var $this ViewDetalleparteController */
/* @var $model ViewDetalleparte */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'id', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'materia_prima_id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'materia_prima_id', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_tipo', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'id_tipo', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'elemento_id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'elemento_id', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'producto_id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'producto_id', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numero_revision', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'numero_revision', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'descripcion', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'descripcion',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cod_setecel', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cod_setecel',array('size'=>30,'maxlength'=>30, 'class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cantidad_cable', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cantidad_cable', array('class'=>'form-control')); ?>
	</div>

	<!-- <div class="row buttons">
		\n"; ?>
	</div> -->

	<button type="submit" class="btn btn-primary "><?php echo $model->isNewRecord ? 'Crear' : 'Guardar'</button>

<?php $this->endWidget(); ?>

</div><!-- search-form -->