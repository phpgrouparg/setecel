<?php
/* @var $this ViewDetalleparteController */
/* @var $model ViewDetalleparte */

$this->breadcrumbs=array(
	'View Detallepartes'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ViewDetalleparte', 'url'=>array('index')),
	array('label'=>'Create ViewDetalleparte', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#view-detalleparte-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar View Detallepartes</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'view-detalleparte-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		//'materia_prima_id',
		//'id_tipo',
		//'elemento_id',
		'producto_id',
		'numero_revision',
		'descripcion',
		'cod_setecel',
		'cantidad_cable',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
