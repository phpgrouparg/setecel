<?php
/* @var $this ViewDetalleparteController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'View Detallepartes',
);

$this->menu=array(
	array('label'=>'Create ViewDetalleparte', 'url'=>array('create')),
	array('label'=>'Manage ViewDetalleparte', 'url'=>array('admin')),
);
?>

<h1>View Detallepartes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
