<?php
/* @var $this ViewDetalleparteController */
/* @var $data ViewDetalleparte */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('materia_prima_id')); ?>:</b>
	<?php echo CHtml::encode($data->materia_prima_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tipo')); ?>:</b>
	<?php echo CHtml::encode($data->id_tipo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('elemento_id')); ?>:</b>
	<?php echo CHtml::encode($data->elemento_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('producto_id')); ?>:</b>
	<?php echo CHtml::encode($data->producto_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numero_revision')); ?>:</b>
	<?php echo CHtml::encode($data->numero_revision); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('cod_setecel')); ?>:</b>
	<?php echo CHtml::encode($data->cod_setecel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidad_cable')); ?>:</b>
	<?php echo CHtml::encode($data->cantidad_cable); ?>
	<br />

	*/ ?>

</div>