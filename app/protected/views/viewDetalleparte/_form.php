<?php
/* @var $this ViewDetalleparteController */
/* @var $model ViewDetalleparte */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'view-detalleparte-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos marcados con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'id', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'materia_prima_id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'materia_prima_id', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'materia_prima_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'id_tipo', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'id_tipo', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'id_tipo'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'elemento_id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'elemento_id', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'elemento_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'producto_id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'producto_id', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'producto_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'numero_revision', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'numero_revision', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'numero_revision'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'descripcion', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'descripcion',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'cod_setecel', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cod_setecel',array('size'=>30,'maxlength'=>30, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'cod_setecel'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'cantidad_cable', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cantidad_cable', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'cantidad_cable'); ?>
	</div>


	<button type="submit" class="btn btn-primary "><?php echo $model->isNewRecord ? 'Crear' : 'Guardar'; ?></button>
	<!-- <div class="row buttons">
		\n"; ?>
	</div> -->

<?php $this->endWidget(); ?>

</div><!-- form -->