<?php
/* @var $this DetalleController */
/* @var $model Detalle */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'detalle-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'id_producto', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'id_producto', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'id_producto'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'id_materia_prima', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'id_materia_prima', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'id_materia_prima'); ?>
	</div>

	<div class="form-row">
		<div class="col-sm-10">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->