<?php
/* @var $this ClienteController */
/* @var $model Cliente */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cliente-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos marcados con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'nombre', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>50,'maxlength'=>50, 'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'telefono', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'telefono', array('class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'telefono'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'direccion', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'direccion',array('size'=>60,'maxlength'=>100, 'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'direccion'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'dni_cuil_cuit', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'dni_cuil_cuit', array('class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'dni_cuil_cuit'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'codigo_postal', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'codigo_postal',array('size'=>50,'maxlength'=>50, 'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'codigo_postal'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'localidad', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'localidad',array('size'=>50,'maxlength'=>50, 'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'localidad'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'provincia', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'provincia',array('size'=>50,'maxlength'=>50, 'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'provincia'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'barrio', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'barrio',array('size'=>50,'maxlength'=>50, 'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'barrio'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'fecha_alta', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'fecha_alta', array('class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'fecha_alta'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'fecha_baja', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'fecha_baja', array('class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'fecha_baja'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'email', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50, 'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'rubro', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'rubro',array('size'=>50,'maxlength'=>50, 'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'rubro'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'situacion_fiscal', array('class'=>'col-2 col-form-label')); ?>
		<!--<?php// echo $form->textField($model,'situacion_fiscal',array('size'=>50,'maxlength'=>50, 'class'=>'form-control col-9')); ?>-->
		<select class="form-control col-9" name="Cliente[situacion_fiscal]">
		 <option value='IVA Responsable Inscripto'>IVA Responsable Inscripto</option>
		 <option value='IVA Responsable no Inscripto'>IVA Responsable no Inscripto</option>
		 <option value='IVA no Responsable'>IVA no Responsable</option>
		 <option value='IVA Sujeto Exento'>IVA Sujeto Exento</option>
		 <option value='Consumidor Final'>Consumidor Fina</option>
		 <option value='Responsable Monotributo'>Responsable Monotributo</option>
		 <option value='Sujeto no Categorizado'>Sujeto no Categorizado</option>
		 <option value='Proveedor del Exterior'>Proveedor del Exterior</option>
		 <option value='Cliente del Exterior'>Cliente del Exterior</option>
		 <option value='IVA Liberado – Ley Nº 19.640'>IVA Liberado – Ley Nº 19.640</option>
		 <option value='IVA Responsable Inscripto – Agente de Percepción'>IVA Responsable Inscripto – Agente de Percepción</option>
		 <option value='Pequeño Contribuyente Eventual'>Pequeño Contribuyente Eventual</option>
		 <option value='Monotributista Social'>Monotributista Social</option>
		 <option value='Pequeño Contribuyente Eventual Social'>Pequeño Contribuyente Eventual Social</option>
       </select>
		<?php echo $form->error($model,'situacion_fiscal'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'web', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'web',array('size'=>50,'maxlength'=>50, 'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'web'); ?>
	</div>


	<button type="submit" class="btn btn-primary "><?php echo $model->isNewRecord ? 'Crear' : 'Guardar';?></button>


<?php $this->endWidget(); ?>

</div><!-- form -->