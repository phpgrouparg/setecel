<?php
/* @var $this ClienteController */
/* @var $model Cliente */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'id', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombre', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'telefono', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'telefono', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'direccion', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'direccion',array('size'=>60,'maxlength'=>100, 'class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dni_cuil_cuit', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'dni_cuil_cuit', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'codigo_postal', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'codigo_postal',array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'localidad', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'localidad',array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'provincia', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'provincia',array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'barrio', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'barrio',array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_alta', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'fecha_alta', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_baja', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'fecha_baja', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rubro', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'rubro',array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'situacion_fiscal', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'situacion_fiscal',array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'web', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'web',array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>
	</div>

	<!-- <div class="row buttons">
		\n"; ?>
	</div> -->

	<button type="submit" class="btn btn-primary "><?php echo $model->isNewRecord ? 'Crear' : 'Guardar'</button>

<?php $this->endWidget(); ?>

</div><!-- search-form -->