<?php
/* @var $this ListadocomponentesController */
/* @var $model Listadocomponentes */

$this->breadcrumbs=array(
	'Listadocomponentes'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Listadocomponentes', 'url'=>array('index')),
	array('label'=>'Create Listadocomponentes', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#listadocomponentes-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Listadocomponentes</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'listadocomponentes-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'tipo',
		'componente_id',
		'proveedor_id',
		'cod_setecel',
		'materia_prima_fk',

		/*
		'Descripcion',
		'Unidad_Medida',
		'stock',
		'nombre',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
