<?php
/* @var $this ListadocomponentesController */
/* @var $model Listadocomponentes */

$this->breadcrumbs=array(
	'Listadocomponentes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Listadocomponentes', 'url'=>array('index')),
	array('label'=>'Create Listadocomponentes', 'url'=>array('create')),
	array('label'=>'Update Listadocomponentes', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Listadocomponentes', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Listadocomponentes', 'url'=>array('admin')),
);
?>

<h1>View Listadocomponentes #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'tipo',
		'componente_id',
		'proveedor_id',
		'cod_setecel',
		'materia_prima_fk',
		'Descripcion',
		'Unidad_Medida',
		'stock',
		'nombre',
	),
)); ?>
