<?php
/* @var $this ListadocomponentesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Listadocomponentes',
);

$this->menu=array(
	array('label'=>'Create Listadocomponentes', 'url'=>array('create')),
	array('label'=>'Manage Listadocomponentes', 'url'=>array('admin')),
);
?>

<h1>Listadocomponentes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
