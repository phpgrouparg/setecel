<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'listadocomponentes-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'tipo',
		//'componente_id',
		'categoria',
		/*array(
			'name'=>'proveedor_id',
			'header'=>'Proveedor',
			'value'=>'$data->getRelated(\'proveedor_fk\')->nombre', // ESTO TRAE EL DATO DE OTRA TABLA RELACIONADA
		),*/
		'nombre',
		'cod_setecel',
		'Unidad_Medida',
		'stock',
		//'id_categoria'
		//'materia_prima_fk',
		/*
		'Descripcion',
		'Unidad_Medida',
		'stock',
		
		'nombre',
		*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}',
			'buttons'=>array
			(
				'view' => array
				(
					'label'=>'Ver mas',
					'url'=>'Yii::app()->createUrl("componente/view", array("id"=>$data->componente_id))',
				),
				'update' => array
				(
					'label'=>'Actualizar',
					'url'=>'Yii::app()->createUrl("materiaPrima/update", array("id"=>$data->id))',
				),
			),
		),
	),
)); ?>
