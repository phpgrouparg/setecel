<?php
/* @var $this ListadocablesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Listadocables',
);

$this->menu=array(
	array('label'=>'Create Listadocables', 'url'=>array('create')),
	array('label'=>'Manage Listadocables', 'url'=>array('admin')),
);
?>

<h1>Listadocables</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
