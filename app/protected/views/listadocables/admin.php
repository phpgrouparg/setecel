<?php
/* @var $this ListadocablesController */
/* @var $model Listadocables */

$this->breadcrumbs=array(
	'Listadocables'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Listadocables', 'url'=>array('index')),
	array('label'=>'Create Listadocables', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#listadocables-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Listadocables</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'htmlOptions' => array('class' => 'tables'),
	'itemsCssClass' => 'table-class',
	'id'=>'listadocables-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'tipo',
		'componente_id',
		'proveedor_id',
		'cod_setecel',
		'materia_prima_fk',
		/*
		'Descripcion',
		'Unidad_Medida',
		'stock',
		'nombre',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
