<?php
/* @var $this ListadocablesController */
/* @var $model Listadocables */

$this->breadcrumbs=array(
	'Listadocables'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Listadocables', 'url'=>array('index')),
	array('label'=>'Create Listadocables', 'url'=>array('create')),
	array('label'=>'Update Listadocables', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Listadocables', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Listadocables', 'url'=>array('admin')),
);
?>

<h1>View Listadocables #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'tipo',
		'componente_id',
		'proveedor_id',
		'cod_setecel',
		'materia_prima_fk',
		'Descripcion',
		'Unidad_Medida',
		'stock',
		'nombre',
	),
)); ?>
