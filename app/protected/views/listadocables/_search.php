<?php
/* @var $this ListadocablesController */
/* @var $model Listadocables */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipo', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'tipo',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'componente_id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'componente_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'proveedor_id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'proveedor_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cod_setecel', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cod_setecel',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'materia_prima_fk', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'materia_prima_fk'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Descripcion', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'Descripcion',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Unidad_Medida'); ?>
		<?php echo $form->textField($model,'Unidad_Medida',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stock'); ?>
		<?php echo $form->textField($model,'stock'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->