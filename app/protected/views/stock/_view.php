<?php
/* @var $this StockController */
/* @var $data Stock */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stock')); ?>:</b>
	<?php echo CHtml::encode($data->stock); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stock_min')); ?>:</b>
	<?php echo CHtml::encode($data->stock_min); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stock_max')); ?>:</b>
	<?php echo CHtml::encode($data->stock_max); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('materia_prima_id')); ?>:</b>
	<?php echo CHtml::encode($data->materia_prima_id); ?>
	<br />


</div>