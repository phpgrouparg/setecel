<?php
/* @var $this StockController */
/* @var $model Stock */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'stock-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos Con <span class="required">*</span> Son Requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'stock', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'stock', array('class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'stock', array('class'=>'col-2 col-form-label')); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'stock_min', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'stock_min', array('class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'stock_min', array('class'=>'col-2 col-form-label')); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'stock_max', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'stock_max', array('class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'stock_max', array('class'=>'col-2 col-form-label')); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'materia_prima_id', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'materia_prima_id', array('class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'materia_prima_id', array('class'=>'col-2 col-form-label')); ?>
	</div>

	<div class="form-row">
		<div class="col-sm-10">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-primary')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->