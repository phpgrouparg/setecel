<?php
/* @var $this StockController */
/* @var $model Stock */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stock'); ?>
		<?php echo $form->textField($model,'stock'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stock_min'); ?>
		<?php echo $form->textField($model,'stock_min'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stock_max'); ?>
		<?php echo $form->textField($model,'stock_max'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'materia_prima_id'); ?>
		<?php echo $form->textField($model,'materia_prima_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->