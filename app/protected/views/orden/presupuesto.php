<?php
/* @var $this OrdenController */
/* @var $model Orden */

$this->breadcrumbs=array(
	'Ordens'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Crear Presupuesto', 'url'=>array('create', 'tipo'=>'presupuesto')),
);
?>

<h1>Administrar Presupuestos</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'orden-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'cliente_id',
		'fecha',
		'tipo',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
