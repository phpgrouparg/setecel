<?php
/* @var $this OrdenController */
/* @var $model Orden */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'orden-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos marcados con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'cliente_id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cliente_id', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'cliente_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'fecha', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->dateField($model,'fecha', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'fecha'); ?>
	</div>

	<?php echo $form->hiddenField($model,'tipo',array('size'=>11,'maxlength'=>11, 'class'=>'form-control')); ?>


	<button type="submit" class="btn btn-primary "><?php echo $model->isNewRecord ? 'Crear' : 'Guardar'; ?></button>
	<!-- <div class="row buttons">
		\n"; ?>
	</div> -->

<?php $this->endWidget(); ?>

</div><!-- form -->

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">
  $( function() {
    function log( message ) {
      $( "<div>" ).text( message ).prependTo( "#log" );
      $( "#log" ).scrollTop( 0 );
    }
 
    $( "#Orden_cliente_id" ).autocomplete({
      source: "/cliente/buscar",
      minLength: 2,
      select: function( event, ui ) {
        log( "Selected: " + ui.item.value + " aka " + ui.item.id );
      }
    });
  } );
</script>