<?php
/* @var $this OrdenController */
/* @var $model Orden */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'id', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cliente_id', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'cliente_id', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'fecha', array('class'=>'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipo', array('class'=>'col-sm-2 col-form-label')); ?>
		<?php echo $form->textField($model,'tipo',array('size'=>11,'maxlength'=>11, 'class'=>'form-control')); ?>
	</div>

	<!-- <div class="row buttons">
		\n"; ?>
	</div> -->

	<button type="submit" class="btn btn-primary "><?php echo $model->isNewRecord ? 'Crear' : 'Guardar'</button>

<?php $this->endWidget(); ?>

</div><!-- search-form -->