<?php
/* @var $this MateriaPrimaController */
/* @var $data MateriaPrima */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->Descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Inactivo')); ?>:</b>
	<?php echo CHtml::encode($data->Inactivo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Caja_Sujecion')); ?>:</b>
	<?php echo CHtml::encode($data->Caja_Sujecion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Unidad_Medida')); ?>:</b>
	<?php echo CHtml::encode($data->Unidad_Medida); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Ubicacion')); ?>:</b>
	<?php echo CHtml::encode($data->Ubicacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_proveedor')); ?>:</b>
	<?php echo CHtml::encode($data->id_proveedor); ?>
	<br />


</div>