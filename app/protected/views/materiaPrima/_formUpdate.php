<?php
/* @var $this MateriaPrimaController */
/* @var $model MateriaPrima */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'materia-prima-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'Descripcion', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'Descripcion',array('size'=>100,'maxlength'=>100, 'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'Descripcion'); ?>
	</div>



	<div class="form-group row">
		<?php echo $form->labelEx($model,'Inactivo', array('class'=>'col-2 col-form-label')); ?>
		 <select class="form-control col-9" name="MateriaPrima[Inactivo]">
           <option value='0'>No</option>
           <option value='1'>Si</option>
       </select>
		<?php echo $form->error($model,'Inactivo'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'Caja_Sujecion', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'Caja_Sujecion',array('size'=>30,'maxlength'=>30, 'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'Caja_Sujecion'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'Unidad_Medida', array('class'=>'col-2 col-form-label')); ?>
		 <select class="form-control col-9" name="MateriaPrima[Unidad_Medida]">
           <option value='mm2'>mm2</option>
           <option value='Unidad'>Unidad</option>
       </select>
		<?php echo $form->error($model,'Unidad_Medida'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'Ubicacion', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->textField($model,'Ubicacion',array('size'=>30,'maxlength'=>30, 'class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'Ubicacion'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'id_proveedor', array('class'=>'col-2 col-form-label')); ?>
		<?php echo $form->dropDownList($model,'id_proveedor', CHtml::listData(Proveedor::model()->findAll(), 'id', 'nombre'), array('class'=>'form-control col-9')); ?>
		<?php echo $form->error($model,'id_proveedor'); ?>
	</div>
	<?php echo $form->hiddenField($model,'id_tipo',array('size'=>30,'maxlength'=>30, 'class'=>'form-control')); ?>	
	<div class="form-row">
		<div class="col-sm-10">
	<button type="submit" class="btn btn-primary "><?php echo $model->isNewRecord ? 'Crear' : 'Guardar' ?></button>
		</div>
	</div>

	<?php ///echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-primary')); ?>

<?php $this->endWidget(); ?>

</div><!-- form -->