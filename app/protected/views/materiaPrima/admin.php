<?php
/* @var $this MateriaPrimaController */
/* @var $model MateriaPrima */

$this->breadcrumbs=array(
	'Materia Prima'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Crear MateriaPrima', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#materia-prima-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administracion de Materia Prima</h1>

<?php

$this->widget('CTabView', array(
    'tabs'=>array(
        'tab1'=>array(
            'title'=>'Cables',
            'view'=>'../listadocables/cablestabs',
            'data'=>array('model'=>$cables),
        ),
        'tab2'=>array(
            'title'=>'Componentes',
            'view'=>'../listadocomponentes/componentestabs',
            'data'=>array('model'=>$componentes),
        ),
    ),
));

?>