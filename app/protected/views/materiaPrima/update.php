<?php
/* @var $this MateriaPrimaController */
/* @var $model MateriaPrima */

$this->breadcrumbs=array(
	'Materia Primas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	//array('label'=>'Listar MateriaPrima', 'url'=>array('index')),
	array('label'=>'Crear MateriaPrima', 'url'=>array('create')),
	array('label'=>'Ver MateriaPrima', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administar MateriaPrima', 'url'=>array('admin')),
);

?>

<h1>Actualizando Materia Prima N°:<?php echo $model->id; ?> <?php echo ($model->id_tipo == 1?"Cable":"Componente");?></h1>

<?php $this->renderPartial('_formUpdate', array('model'=>$model)); ?>