<?php
/* @var $this MateriaPrimaController */
/* @var $model MateriaPrima */

$this->breadcrumbs=array(
	'Materia Primas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Creaar MateriaPrima', 'url'=>array('create')),
	array('label'=>'Actualizar MateriaPrima', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Administrar MateriaPrima', 'url'=>array('admin')),
);
?>

<h1>MateriaPrima <?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'Descripcion',
		'Inactivo',
		'Caja_Sujecion',
		'Unidad_Medida',
		'Ubicacion',
		'id_proveedor',
	),
)); ?>
