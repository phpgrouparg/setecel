<?php
/* @var $this MateriaPrimaController */
/* @var $model MateriaPrima */

$this->breadcrumbs=array(
	'Materia Primas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Administrar Materia Prima', 'url'=>array('admin')),
);
?>

<h1>Crear Materia Prima</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>

<?php
/*$this->widget('CTabView', array(
    'tabs'=>array(
        'tab1'=>array(
            'title'=>'Ficha',
            'view'=>'../componente/_form',
            'data'=>array('model'=>$componente),
        ),
        'tab2'=>array(
            'title'=>'Cable',
            'view'=>'../cable/_form',
            'data'=>array('model'=>$cable),
        ),
    ),
));
*/
?>