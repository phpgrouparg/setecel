<?php /*<h1><?php echo CrugeTranslator::t('logon',"Login"); ?></h1>
<?php if(Yii::app()->user->hasFlash('loginflash')): ?>
<div class="flash-error">
	<?php echo Yii::app()->user->getFlash('loginflash'); ?>
</div>
<?php else: ?>
<div class="form"> */ ?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'logon-form',
	'enableClientValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
    'htmlOptions'=>array(
        'class'=>'form-signin',
    ),
)); ?>

	<h1 class="h3 mb-3 font-weight-normal">Inicie Sesion</h1>

		<?php echo $form->labelEx($model,'username', array('class'=>'sr-only')); ?>
		<?php echo $form->textField($model,'username', array('class'=>'form-control', 'required autofocus' => '')); ?>
		<?php echo $form->error($model,'username'); ?>

		<?php echo $form->labelEx($model,'password', array('class'=>'sr-only')); ?>
		<?php echo $form->passwordField($model,'password', array('class'=>'form-control', 'required autofocus' => '')); ?>
		<?php echo $form->error($model,'password'); ?>

		<?php echo $form->checkBox($model,'rememberMe', array('class' => 'form-check-input')); ?>
		<?php echo $form->label($model,'Recordarme', array('class'=>'form-check-label')); ?>
		<?php echo $form->error($model,'rememberMe'); ?>

		<?php echo CHtml::submitButton('Iniciar Sesion', array('class' => 'btn btn-lg btn-primary btn-block')); ?>

		<?php //Yii::app()->user->ui->tbutton(CrugeTranslator::t('logon', "Login")); ?>
		<?php //echo Yii::app()->user->ui->passwordRecoveryLink; ?>
		<?php
			if(Yii::app()->user->um->getDefaultSystem()->getn('registrationonlogin')===1)
				echo Yii::app()->user->ui->registrationLink;
		?>


	<?php
		//	si el componente CrugeConnector existe lo usa:
		//
		if(Yii::app()->getComponent('crugeconnector') != null){
		if(Yii::app()->crugeconnector->hasEnabledClients){ 
	/*
	<div class='crugeconnector'>
		<span><?php echo CrugeTranslator::t('logon', 'You also can login with');?>:</span>
		<ul>
		<?php 
			$cc = Yii::app()->crugeconnector;
			foreach($cc->enabledClients as $key=>$config){
				$image = CHtml::image($cc->getClientDefaultImage($key));
				echo "<li>".CHtml::link($image,
					$cc->getClientLoginUrl($key))."</li>";
			}
		?>
		</ul>
	</div> */ ?>
	<?php }} ?>
	

<?php $this->endWidget(); ?>
<?php /*</div>
<?php endif; */ ?>
