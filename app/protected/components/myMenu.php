<?php
        Yii :: import('zii.widgets.CMenu');
        class MyMenu extends CMenu {
        // must set this to allow  parameter changes in CMenu widget call
            public $activateItemsOuter = true;
 
            public function run() {
                $this->renderMenu($this->items);
            }

            public function dd($var){
                echo "<pre>";
                print_r($var);
                echo "</pre>";
                die(1);
            }
 
            protected function renderMenuRecursive($items) {
                //$this->dd($items);
                echo CHtml :: openTag('ul',array('class'=>'navbar-nav mr-auto')) . "\n";
                foreach ($items as $key => $value) {
                    echo CHtml :: openTag('li', array('class'=>'nav-item'));
                    echo CHtml :: link($value['label'], $value['url'], array('class'=>'nav-link'));
                    echo CHtml :: closeTag('li') . "\n";
                }
                echo CHtml :: closeTag('ul') . "\n";
            }
        }
    ?>