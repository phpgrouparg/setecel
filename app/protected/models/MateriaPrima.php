<?php

/**
 * This is the model class for table "materia_prima".
 *
 * The followings are the available columns in table 'materia_prima':
 * @property integer $id
 * @property string $Descripcion
 * @property integer $Inactivo
 * @property string $Caja_Sujecion
 * @property string $Unidad_Medida
 * @property string $Ubicacion
 * @property integer $id_proveedor
 * @property integer $id_tipo
 */
class MateriaPrima extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'materia_prima';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Descripcion, Inactivo, Unidad_Medida, id_proveedor', 'required'),
			array('Inactivo, id_proveedor, id_tipo', 'numerical', 'integerOnly'=>true),
			array('Descripcion','length' , 'max'=>255),
			array('Caja_Sujecion, Unidad_Medida, Ubicacion', 'length', 'max'=>50),
			//array('Descripcion', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Descripcion, Inactivo, Caja_Sujecion, Unidad_Medida, Ubicacion, id_proveedor, id_tipo', 'safe', 'on'=>'search'),
		);
	}	

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'proveedor_fk' => array(self::BELONGS_TO, 'Proveedor', 'id'),
			'cables' => array(self::HAS_MANY, 'Cable', 'materia_prima_fk'),
			'componentes' => array(self::HAS_MANY, 'Componente', 'materia_prima_fk'),
			'detalles' => array(self::HAS_MANY, 'Detalle', 'id_materia_prima'),
			'detallePartes' => array(self::HAS_MANY, 'DetalleParte', 'materia_prima_id'),
			'idTipo' => array(self::BELONGS_TO, 'Materiaprimatipo', 'id_tipo'),
			'idProveedor' => array(self::BELONGS_TO, 'Proveedor', 'id_proveedor'),
			'stocks' => array(self::HAS_MANY, 'Stock', 'materia_prima_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Descripcion' => 'Descripcion',
			'Inactivo' => 'Inactivo',
			'Caja_Sujecion' => 'Caja Sujecion',
			'Unidad_Medida' => 'Unidad Medida',
			'Ubicacion' => 'Ubicacion',
			'id_proveedor' => 'Proveedor',
			'id_tipo' => 'Tipo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('Inactivo',$this->Inactivo);
		$criteria->compare('Caja_Sujecion',$this->Caja_Sujecion,true);
		$criteria->compare('Unidad_Medida',$this->Unidad_Medida,true);
		$criteria->compare('Ubicacion',$this->Ubicacion,true);
		$criteria->compare('id_proveedor',$this->id_proveedor);
		$criteria->compare('id_tipo',$this->id_tipo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MateriaPrima the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
