<?php

/**
 * This is the model class for table "proveedor".
 *
 * The followings are the available columns in table 'proveedor':
 * @property integer $id
 * @property string $nombre
 * @property string $telefono
 * @property string $email
 * @property integer $dni_cuil_cuit
 * @property string $codigo_postal
 * @property string $direccion
 * @property string $localidad
 * @property string $provincia
 * @property string $barrio
 * @property string $fecha_alta
 * @property string $fecha_baja
 */
class Proveedor extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'proveedor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, telefono, direccion', 'required'),
			array('dni_cuil_cuit', 'numerical', 'integerOnly'=>true),
			array('nombre, direccion', 'length', 'max'=>100),
			array('telefono, localidad', 'length', 'max'=>50),
			array('email, provincia', 'length', 'max'=>40),
			array('codigo_postal', 'length', 'max'=>10),
			array('barrio', 'length', 'max'=>30),
			array('fecha_alta, fecha_baja', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombre, telefono, email, dni_cuil_cuit, codigo_postal, direccion, localidad, provincia, barrio, fecha_alta, fecha_baja', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'mp_fk'=>array(self::HAS_MANY, 'MateriaPrima', 'id_proveedor'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
			'telefono' => 'Telefono',
			'email' => 'Email',
			'dni_cuil_cuit' => 'Dni Cuil Cuit',
			'codigo_postal' => 'Codigo Postal',
			'direccion' => 'Direccion',
			'localidad' => 'Localidad',
			'provincia' => 'Provincia',
			'barrio' => 'Barrio',
			'fecha_alta' => 'Fecha Alta',
			'fecha_baja' => 'Fecha Baja',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('dni_cuil_cuit',$this->dni_cuil_cuit);
		$criteria->compare('codigo_postal',$this->codigo_postal,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('localidad',$this->localidad,true);
		$criteria->compare('provincia',$this->provincia,true);
		$criteria->compare('barrio',$this->barrio,true);
		$criteria->compare('fecha_alta',$this->fecha_alta,true);
		$criteria->compare('fecha_baja',$this->fecha_baja,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Proveedor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
