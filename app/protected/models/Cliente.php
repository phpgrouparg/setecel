<?php

/**
 * This is the model class for table "cliente".
 *
 * The followings are the available columns in table 'cliente':
 * @property integer $id
 * @property string $nombre
 * @property integer $telefono
 * @property string $direccion
 * @property integer $dni_cuil_cuit
 * @property string $codigo_postal
 * @property string $localidad
 * @property string $provincia
 * @property string $barrio
 * @property string $fecha_alta
 * @property string $fecha_baja
 * @property string $email
 * @property string $rubro
 * @property string $situacion_fiscal
 * @property string $web
 */
class Cliente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cliente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, telefono, direccion, dni_cuil_cuit, localidad, provincia, barrio, fecha_alta', 'required'),
			array('telefono, dni_cuil_cuit', 'numerical', 'integerOnly'=>true),
			array('nombre, codigo_postal, localidad, provincia, barrio, email, rubro, situacion_fiscal, web', 'length', 'max'=>50),
			array('direccion', 'length', 'max'=>100),
			array('fecha_baja', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombre, telefono, direccion, dni_cuil_cuit, codigo_postal, localidad, provincia, barrio, fecha_alta, fecha_baja, email, rubro, situacion_fiscal, web', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
			'telefono' => 'Telefono',
			'direccion' => 'Direccion',
			'dni_cuil_cuit' => 'Dni Cuil Cuit',
			'codigo_postal' => 'Codigo Postal',
			'localidad' => 'Localidad',
			'provincia' => 'Provincia',
			'barrio' => 'Barrio',
			'fecha_alta' => 'Fecha Alta',
			'fecha_baja' => 'Fecha Baja',
			'email' => 'Email',
			'rubro' => 'Rubro',
			'situacion_fiscal' => 'Situacion Fiscal',
			'web' => 'Web',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('telefono',$this->telefono);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('dni_cuil_cuit',$this->dni_cuil_cuit);
		$criteria->compare('codigo_postal',$this->codigo_postal,true);
		$criteria->compare('localidad',$this->localidad,true);
		$criteria->compare('provincia',$this->provincia,true);
		$criteria->compare('barrio',$this->barrio,true);
		$criteria->compare('fecha_alta',$this->fecha_alta,true);
		$criteria->compare('fecha_baja',$this->fecha_baja,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('rubro',$this->rubro,true);
		$criteria->compare('situacion_fiscal',$this->situacion_fiscal,true);
		$criteria->compare('web',$this->web,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cliente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
