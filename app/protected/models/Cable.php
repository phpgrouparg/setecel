<?php

/**
 * This is the model class for table "cable".
 *
 * The followings are the available columns in table 'cable':
 * @property integer $id
 * @property string $cod_setecel
 * @property string $color_base
 * @property string $color_linea
 * @property double $seccion
 * @property integer $packaging
 * @property string $cod_proveedor
 * @property double $precio
 * @property integer $materia_prima_fk
 */
class Cable extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cable';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cod_setecel, color_base, color_linea, seccion, packaging, cod_proveedor, precio', 'required'),
			array('packaging, materia_prima_fk', 'numerical', 'integerOnly'=>true),
			array('seccion, precio', 'numerical'),
			array('cod_setecel, color_base, color_linea, cod_proveedor', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, cod_setecel, color_base, color_linea, seccion, packaging, cod_proveedor, precio, materia_prima_fk', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.

		return array(
			'materia_fk' => array(self::BELONGS_TO, 'MateriaPrima', 'materia_prima_fk'),
			'materiaPrimaFk' => array(self::BELONGS_TO, 'MateriaPrima', 'materia_prima_fk'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cod_setecel' => 'Codigo Setecel',
			'color_base' => 'Color Base',
			'color_linea' => 'Color Linea',
			'seccion' => 'Seccion',
			'packaging' => 'Packaging',
			'cod_proveedor' => 'Cod Proveedor',
			'precio' => 'Precio',
			'materia_prima_fk' => 'Materia Prima Fk',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('materiaPrimaFk');

		$criteria->compare('id',$this->id);
		$criteria->compare('cod_setecel',$this->cod_setecel,true);
		$criteria->compare('color_base',$this->color_base,true);
		$criteria->compare('color_linea',$this->color_linea,true);
		$criteria->compare('seccion',$this->seccion);
		$criteria->compare('packaging',$this->packaging);
		$criteria->compare('cod_proveedor',$this->cod_proveedor,true);
		$criteria->compare('precio',$this->precio);
		$criteria->compare('materia_prima_fk',$this->materia_prima_fk);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cable the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
