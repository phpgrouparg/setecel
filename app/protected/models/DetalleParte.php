<?php

/**
 * This is the model class for table "detalle_parte".
 *
 * The followings are the available columns in table 'detalle_parte':
 * @property integer $id
 * @property integer $materia_prima_id
 * @property integer $producto_id
 * @property integer $numero_revision
 * @property integer $cantidad_cable
 * @property integer $cantidad_comp
 */
class DetalleParte extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'detalle_parte';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('materia_prima_id, producto_id', 'required'),
			array('materia_prima_id, producto_id, numero_revision, cantidad_cable, cantidad_comp', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, materia_prima_id, producto_id, numero_revision, cantidad_cable, cantidad_comp', 'safe', 'on'=>'search'),
		);
	}

	public function getMaxVersion($id)
	{
		return DetalleParte::model()->findByPk($id) == null ? 1 : Yii::app()->db->createCommand()
			->select('max(numero_revision) as maximo')
			->from('detalle_parte dp')
			->where('producto_id=:id', array(':id'=>$id))
			->queryRow()['maximo'];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'materiaPrima' => array(self::BELONGS_TO, 'MateriaPrima', 'materia_prima_id'),
			'producto' => array(self::BELONGS_TO, 'Producto', 'producto_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'materia_prima_id' => 'Materia Prima',
			'producto_id' => 'Producto',
			'numero_revision' => 'Revision',
			'cantidad_cable' => 'Cant. Cable',
			'cantidad_comp' => 'Cant. Componentes',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with = array('materiaPrima');
		$criteria->with = array('producto');

		$criteria->compare('id',$this->id);
		$criteria->compare('materia_prima_id',$this->materia_prima_id);
		$criteria->compare('producto_id',$this->producto_id);
		$criteria->compare('numero_revision',$this->numero_revision);
		$criteria->compare('cantidad_cable',$this->cantidad_cable);
		$criteria->compare('cantidad_comp',$this->cantidad_comp);

		/*return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));*/

        return new CActiveDataProvider($this, array(
            'sort' => array('defaultOrder' => 'numero_revision desc'),
            'pagination'=>array('pageSize'=>50),
            'criteria'=>$criteria,
        ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DetalleParte the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
