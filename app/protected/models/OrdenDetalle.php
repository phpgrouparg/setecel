<?php

/**
 * This is the model class for table "orden_detalle".
 *
 * The followings are the available columns in table 'orden_detalle':
 * @property integer $id
 * @property integer $orden_id
 * @property integer $materia_prima_id
 * @property integer $cantidad
 * @property double $total
 *
 * The followings are the available model relations:
 * @property MateriaPrima $materiaPrima
 * @property Orden $orden
 */
class OrdenDetalle extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'orden_detalle';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('orden_id, materia_prima_id, cantidad', 'numerical', 'integerOnly'=>true),
            array('total', 'numerical'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, orden_id, materia_prima_id, cantidad, total', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'materiaPrima' => array(self::BELONGS_TO, 'MateriaPrima', 'materia_prima_id'),
            'orden' => array(self::BELONGS_TO, 'Orden', 'orden_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'orden_id' => 'Orden',
            'materia_prima_id' => 'Materia Prima',
            'cantidad' => 'Cantidad',
            'total' => 'Total',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('orden_id',$this->orden_id);
        $criteria->compare('materia_prima_id',$this->materia_prima_id);
        $criteria->compare('cantidad',$this->cantidad);
        $criteria->compare('total',$this->total);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return OrdenDetalle the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}