<?php

/**
 * This is the model class for table "componente".
 *
 * The followings are the available columns in table 'componente':
 * @property integer $id
 * @property string $cod_setecel
 * @property integer $id_categoria
 * @property integer $terminal_asociado
 * @property double $precio
 * @property string $cod_proveedor
 * @property integer $packaging
 * @property string $cod_origen
 * @property integer $materia_prima_fk
 */
class Componente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'componente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cod_setecel, id_categoria, terminal_asociado, precio, cod_proveedor, packaging, cod_origen, materia_prima_fk', 'required'),
			array('id_categoria, packaging, materia_prima_fk', 'numerical', 'integerOnly'=>true),
			array('precio', 'numerical'),
			array('cod_setecel,  terminal_asociado, cod_proveedor, cod_origen', 'length', 'max'=>100),
			// The following rule is used by search().s
			// @todo Please remove those attributes that should not be searched.
			array('id, cod_setecel, id_categoria, terminal_asociado, precio, cod_proveedor, packaging, cod_origen, materia_prima_fk', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'componente_img'=>array(self::HAS_MANY, 'Imagen', 'id_pieza'),
			'componente_cat'=>array(self::BELONGS_TO, 'Categoria', 'id_categoria'),
			'materiaPrimaFk' => array(self::BELONGS_TO, 'MateriaPrima', 'materia_prima_fk'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cod_setecel' => 'Codigo Setecel',
			'id_categoria' => 'Categoria',
			'terminal_asociado' => 'Terminal Asociado',
			'precio' => 'Precio',
			'cod_proveedor' => 'Cod Proveedor',
			'packaging' => 'Packaging',
			'cod_origen' => 'Cod Origen',
			'materia_prima_fk' => 'Materia Prima Fk',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cod_setecel',$this->cod_setecel,true);
		$criteria->compare('id_categoria',$this->id_categoria);
		$criteria->compare('terminal_asociado',$this->terminal_asociado);
		$criteria->compare('precio',$this->precio);
		$criteria->compare('cod_proveedor',$this->cod_proveedor,true);
		$criteria->compare('packaging',$this->packaging);
		$criteria->compare('cod_origen',$this->cod_origen,true);
		$criteria->compare('materia_prima_fk',$this->materia_prima_fk);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Componente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
