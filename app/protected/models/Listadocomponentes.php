<?php

/**
 * This is the model class for table "listadocomponentes".
 *
 * The followings are the available columns in table 'listadocomponentes':
 * @property integer $id
 * @property string $tipo
 * @property integer $componente_id
 * @property integer $proveedor_id
 * @property string $cod_setecel
 * @property integer $materia_prima_fk
 * @property string $Descripcion
 * @property string $Unidad_Medida
 * @property integer $stock
 * @property string $nombre
 */
class Listadocomponentes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'listadocomponentes';
	}

	public function primaryKey()
	{
		return 'id';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cod_setecel, materia_prima_fk, Descripcion, Unidad_Medida, stock, nombre,categoria', 'required'),
			array('id, componente_id, proveedor_id, materia_prima_fk, stock', 'numerical', 'integerOnly'=>true),
			array('tipo, cod_setecel', 'length', 'max'=>10),
			array('Descripcion', 'length', 'max'=>15),
			array('Unidad_Medida', 'length', 'max'=>11),
			array('nombre', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tipo, componente_id, proveedor_id, cod_setecel, materia_prima_fk, Descripcion, Unidad_Medida, stock, nombre', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'proveedor_fk' => array(self::BELONGS_TO, 'Proveedor', 'proveedor_id'),
			'componente_ibfk_1' => array(self::BELONGS_TO, 'Categoria','id'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tipo' => 'Tipo',
			'componente_id' => 'Componente',
			'proveedor_id' => 'Proveedor',
			'cod_setecel' => 'Cod Set',
			'materia_prima_fk' => 'Materia Prima Fk',
			'Descripcion' => 'Descripcion',
			'Unidad_Medida' => 'UM',
			'stock' => 'Stock',
			'nombre' => 'Proveedor',
			'categoria' => 'Categoria',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('componente_id',$this->componente_id);
		$criteria->compare('proveedor_id',$this->proveedor_id);
		$criteria->compare('cod_setecel',$this->cod_setecel,true);
		$criteria->compare('materia_prima_fk',$this->materia_prima_fk);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('Unidad_Medida',$this->Unidad_Medida,true);
		$criteria->compare('stock',$this->stock);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('categoria',$this->categoria,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Listadocomponentes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
