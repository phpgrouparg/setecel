<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php echo "<?php \$form=\$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl(\$this->route),
	'method'=>'get',
)); ?>\n"; ?>

<?php foreach($this->tableSchema->columns as $column): ?>
<?php
	$field=$this->generateInputField($this->modelClass,$column);
	if(strpos($field,'password')!==false)
		continue;
?>
	<div class="row">
		<?php echo "<?php echo \$form->label(\$model,'{$column->name}', array('class'=>'col-sm-2 col-form-label')); ?>\n"; ?>
		<?php echo "<?php echo ".$this->generateActiveField($this->modelClass,$column)."; ?>\n"; ?>
	</div>

<?php endforeach; ?>
	<!-- <div class="row buttons">
		<?php //echo "<?php echo CHtml::submitButton('Search'); ?>\n"; ?>
	</div> -->

	<button type="submit" class="btn btn-primary "><?php echo "<?php echo \$model->isNewRecord ? 'Crear' : 'Guardar'"; ?></button>

<?php echo "<?php \$this->endWidget(); ?>\n"; ?>

</div><!-- search-form -->