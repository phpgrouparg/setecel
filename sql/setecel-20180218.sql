/*
SQLyog Ultimate v9.63 
MySQL - 5.5.5-10.1.29-MariaDB : Database - setecel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`setecel` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `setecel`;

/*Table structure for table `cable` */

DROP TABLE IF EXISTS `cable`;

CREATE TABLE `cable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_setecel` varchar(30) NOT NULL,
  `color_base` varchar(30) NOT NULL,
  `color_linea` varchar(30) NOT NULL,
  `seccion` float NOT NULL,
  `packaging` int(11) NOT NULL,
  `cod_proveedor` varchar(30) NOT NULL,
  `precio` float NOT NULL,
  `materia_prima_fk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `materia_prima` (`materia_prima_fk`),
  CONSTRAINT `materia_fk` FOREIGN KEY (`materia_prima_fk`) REFERENCES `materia_prima` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `cable` */

insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (4,'CA203002','Amarillo','Amarillo',0.35,100008,'Z85606302VL',78990,13),(5,'CA203002','Rosado','Negro',2.5,1000,'Z85606302VL',1324.48,16);

/*Table structure for table `categoria` */

DROP TABLE IF EXISTS `categoria`;

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `categoria` */

insert  into `categoria`(`id`,`nombre`) values (12,'Capuchones'),(13,'Cintas'),(14,'Conectores'),(15,'Easy Gun'),(16,'Moldeados'),(17,'Sellos '),(18,'Terminales'),(19,'Termocontraibles'),(20,'Tubos'),(21,'Varios');

/*Table structure for table `componente` */

DROP TABLE IF EXISTS `componente`;

CREATE TABLE `componente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_setecel` varchar(10) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `descripcion` varchar(10) NOT NULL,
  `terminal_asociado` int(11) NOT NULL,
  `precio` float NOT NULL,
  `cod_proveedor` varchar(10) NOT NULL,
  `packaging` int(11) NOT NULL,
  `cod_origen` varchar(10) NOT NULL,
  `materia_prima_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_categoria` (`id_categoria`) USING BTREE,
  KEY `componente_fk_mp` (`materia_prima_fk`),
  CONSTRAINT `componente_fk_mp` FOREIGN KEY (`materia_prima_fk`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `componente_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `componente` */

insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`descripcion`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (5,'123123',14,' ñlj',234,1234,'780',100,'jdi8',14),(6,'1231233',12,'ajñfsdlka',234,1234,'780',100,'jdi8',15);

/*Table structure for table `cruge_authassignment` */

DROP TABLE IF EXISTS `cruge_authassignment`;

CREATE TABLE `cruge_authassignment` (
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text,
  `itemname` varchar(64) NOT NULL,
  PRIMARY KEY (`userid`,`itemname`),
  KEY `fk_cruge_authassignment_cruge_authitem1` (`itemname`),
  KEY `fk_cruge_authassignment_user` (`userid`),
  CONSTRAINT `fk_cruge_authassignment_cruge_authitem1` FOREIGN KEY (`itemname`) REFERENCES `cruge_authitem` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cruge_authassignment_user` FOREIGN KEY (`userid`) REFERENCES `cruge_user` (`iduser`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_authassignment` */

/*Table structure for table `cruge_authitem` */

DROP TABLE IF EXISTS `cruge_authitem`;

CREATE TABLE `cruge_authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_authitem` */

insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_cable_admin',0,'',NULL,'N;'),('action_cable_create',0,'',NULL,'N;'),('action_cable_index',0,'',NULL,'N;'),('action_cable_update',0,'',NULL,'N;'),('action_cable_view',0,'',NULL,'N;'),('action_componente_admin',0,'',NULL,'N;'),('action_componente_create',0,'',NULL,'N;'),('action_componente_index',0,'',NULL,'N;'),('action_componente_update',0,'',NULL,'N;'),('action_componente_view',0,'',NULL,'N;'),('action_imagen_admin',0,'',NULL,'N;'),('action_imagen_create',0,'',NULL,'N;'),('action_imagen_index',0,'',NULL,'N;'),('action_materiaPrima_admin',0,'',NULL,'N;'),('action_materiaPrima_create',0,'',NULL,'N;'),('action_materiaPrima_index',0,'',NULL,'N;'),('action_materiaPrima_update',0,'',NULL,'N;'),('action_materiaPrima_view',0,'',NULL,'N;'),('action_producto_admin',0,'',NULL,'N;'),('action_producto_create',0,'',NULL,'N;'),('action_producto_delete',0,'',NULL,'N;'),('action_producto_index',0,'',NULL,'N;'),('action_producto_update',0,'',NULL,'N;'),('action_producto_view',0,'',NULL,'N;'),('action_proveedor_admin',0,'',NULL,'N;'),('action_proveedor_create',0,'',NULL,'N;'),('action_proveedor_index',0,'',NULL,'N;'),('action_proveedor_view',0,'',NULL,'N;'),('action_stock_create',0,'',NULL,'N;'),('action_ui_usermanagementadmin',0,'',NULL,'N;'),('admin',0,'',NULL,'N;');

/*Table structure for table `cruge_authitemchild` */

DROP TABLE IF EXISTS `cruge_authitemchild`;

CREATE TABLE `cruge_authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `crugeauthitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `cruge_authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `crugeauthitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `cruge_authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_authitemchild` */

/*Table structure for table `cruge_field` */

DROP TABLE IF EXISTS `cruge_field`;

CREATE TABLE `cruge_field` (
  `idfield` int(11) NOT NULL AUTO_INCREMENT,
  `fieldname` varchar(20) NOT NULL,
  `longname` varchar(50) DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  `required` int(11) DEFAULT '0',
  `fieldtype` int(11) DEFAULT '0',
  `fieldsize` int(11) DEFAULT '20',
  `maxlength` int(11) DEFAULT '45',
  `showinreports` int(11) DEFAULT '0',
  `useregexp` varchar(512) DEFAULT NULL,
  `useregexpmsg` varchar(512) DEFAULT NULL,
  `predetvalue` mediumblob,
  PRIMARY KEY (`idfield`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_field` */

/*Table structure for table `cruge_fieldvalue` */

DROP TABLE IF EXISTS `cruge_fieldvalue`;

CREATE TABLE `cruge_fieldvalue` (
  `idfieldvalue` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `idfield` int(11) NOT NULL,
  `value` blob,
  PRIMARY KEY (`idfieldvalue`),
  KEY `fk_cruge_fieldvalue_cruge_user1` (`iduser`),
  KEY `fk_cruge_fieldvalue_cruge_field1` (`idfield`),
  CONSTRAINT `fk_cruge_fieldvalue_cruge_field1` FOREIGN KEY (`idfield`) REFERENCES `cruge_field` (`idfield`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_cruge_fieldvalue_cruge_user1` FOREIGN KEY (`iduser`) REFERENCES `cruge_user` (`iduser`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_fieldvalue` */

/*Table structure for table `cruge_session` */

DROP TABLE IF EXISTS `cruge_session`;

CREATE TABLE `cruge_session` (
  `idsession` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `created` bigint(30) DEFAULT NULL,
  `expire` bigint(30) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `ipaddress` varchar(45) DEFAULT NULL,
  `usagecount` int(11) DEFAULT '0',
  `lastusage` bigint(30) DEFAULT NULL,
  `logoutdate` bigint(30) DEFAULT NULL,
  `ipaddressout` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idsession`),
  KEY `crugesession_iduser` (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `cruge_session` */

insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (1,1,1517261222,1517263022,0,'::1',1,1517261222,NULL,NULL),(2,1,1517311377,1517313177,1,'::1',2,1517311403,NULL,NULL),(3,1,1517602714,1517604514,0,'::1',1,1517602714,NULL,NULL),(4,1,1517605429,1517607229,0,'::1',2,1517605568,NULL,NULL),(5,1,1517841324,1517843124,0,'::1',1,1517841324,NULL,NULL),(6,1,1518033328,1518035128,1,'::1',1,1518033328,NULL,NULL),(7,1,1518751353,1518753153,1,'::1',1,1518751353,NULL,NULL),(8,1,1518753254,1518755054,1,'::1',3,1518753808,NULL,NULL),(9,1,1518819666,1518821466,0,'::1',1,1518819666,NULL,NULL),(10,1,1518821572,1518823372,1,'::1',1,1518821572,NULL,NULL);

/*Table structure for table `cruge_system` */

DROP TABLE IF EXISTS `cruge_system`;

CREATE TABLE `cruge_system` (
  `idsystem` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `largename` varchar(45) DEFAULT NULL,
  `sessionmaxdurationmins` int(11) DEFAULT '30',
  `sessionmaxsameipconnections` int(11) DEFAULT '10',
  `sessionreusesessions` int(11) DEFAULT '1' COMMENT '1yes 0no',
  `sessionmaxsessionsperday` int(11) DEFAULT '-1',
  `sessionmaxsessionsperuser` int(11) DEFAULT '-1',
  `systemnonewsessions` int(11) DEFAULT '0' COMMENT '1yes 0no',
  `systemdown` int(11) DEFAULT '0',
  `registerusingcaptcha` int(11) DEFAULT '0',
  `registerusingterms` int(11) DEFAULT '0',
  `terms` blob,
  `registerusingactivation` int(11) DEFAULT '1',
  `defaultroleforregistration` varchar(64) DEFAULT NULL,
  `registerusingtermslabel` varchar(100) DEFAULT NULL,
  `registrationonlogin` int(11) DEFAULT '1',
  PRIMARY KEY (`idsystem`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `cruge_system` */

insert  into `cruge_system`(`idsystem`,`name`,`largename`,`sessionmaxdurationmins`,`sessionmaxsameipconnections`,`sessionreusesessions`,`sessionmaxsessionsperday`,`sessionmaxsessionsperuser`,`systemnonewsessions`,`systemdown`,`registerusingcaptcha`,`registerusingterms`,`terms`,`registerusingactivation`,`defaultroleforregistration`,`registerusingtermslabel`,`registrationonlogin`) values (1,'default',NULL,30,10,1,-1,-1,0,0,0,0,'',0,'','',1);

/*Table structure for table `cruge_user` */

DROP TABLE IF EXISTS `cruge_user`;

CREATE TABLE `cruge_user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `regdate` bigint(30) DEFAULT NULL,
  `actdate` bigint(30) DEFAULT NULL,
  `logondate` bigint(30) DEFAULT NULL,
  `username` varchar(64) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL COMMENT 'Hashed password',
  `authkey` varchar(100) DEFAULT NULL COMMENT 'llave de autentificacion',
  `state` int(11) DEFAULT '0',
  `totalsessioncounter` int(11) DEFAULT '0',
  `currentsessioncounter` int(11) DEFAULT '0',
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `cruge_user` */

insert  into `cruge_user`(`iduser`,`regdate`,`actdate`,`logondate`,`username`,`email`,`password`,`authkey`,`state`,`totalsessioncounter`,`currentsessioncounter`) values (1,NULL,NULL,1518821572,'admin','admin@tucorreo.com','admin',NULL,1,0,0),(2,NULL,NULL,NULL,'invitado','invitado','nopassword',NULL,1,0,0);

/*Table structure for table `detalle` */

DROP TABLE IF EXISTS `detalle`;

CREATE TABLE `detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `id_materia_prima` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_producto` (`id_producto`),
  KEY `id_materia_prima` (`id_materia_prima`),
  CONSTRAINT `detalle_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detalle_ibfk_2` FOREIGN KEY (`id_materia_prima`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `detalle` */

/*Table structure for table `imagen` */

DROP TABLE IF EXISTS `imagen`;

CREATE TABLE `imagen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pieza` int(11) NOT NULL,
  `path` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_materia_prima` (`id_pieza`),
  CONSTRAINT `imagen_ibfk_1` FOREIGN KEY (`id_pieza`) REFERENCES `componente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `imagen` */

/*Table structure for table `materia_prima` */

DROP TABLE IF EXISTS `materia_prima`;

CREATE TABLE `materia_prima` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(15) NOT NULL,
  `Inactivo` tinyint(1) NOT NULL,
  `Caja_Sujecion` varchar(15) NOT NULL,
  `Unidad_Medida` varchar(11) NOT NULL,
  `Ubicacion` varchar(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prove` (`id_proveedor`) USING BTREE,
  CONSTRAINT `proveedor_fk` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `materia_prima` */

insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (13,'prueba1',0,'no','mm','nose',3,NULL),(14,'asdasda',0,'asdasd','asda','DOSjj',3,NULL),(15,'testv',1,'2','asdas','DOS',3,NULL),(16,'test',0,'asdasd','1','asd',3,NULL);

/*Table structure for table `producto` */

DROP TABLE IF EXISTS `producto`;

CREATE TABLE `producto` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `Diseno` varchar(100) NOT NULL,
  `FechaAlta` date NOT NULL,
  `FechaBaja` date NOT NULL,
  `Revision` varchar(30) NOT NULL,
  `Cliente` varchar(30) NOT NULL,
  `Observaciones` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `producto` */

/*Table structure for table `proveedor` */

DROP TABLE IF EXISTS `proveedor`;

CREATE TABLE `proveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `email` varchar(40) DEFAULT NULL,
  `dni_cuil_cuit` int(14) DEFAULT NULL,
  `codigo_postal` varchar(10) DEFAULT NULL,
  `direccion` varchar(100) NOT NULL,
  `localidad` varchar(50) DEFAULT NULL,
  `provincia` varchar(40) DEFAULT NULL,
  `barrio` varchar(30) DEFAULT NULL,
  `fecha_alta` date DEFAULT NULL,
  `fecha_baja` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `proveedor` */

insert  into `proveedor`(`id`,`nombre`,`telefono`,`email`,`dni_cuil_cuit`,`codigo_postal`,`direccion`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`) values (3,'Omar','34q3193791|','nesto',789983794,'5425','af jñsdlkfañdslkf','klajdslkfa','lkjsdf ñlaks','lajds ñflads','2017-09-23','0000-00-00');

/*Table structure for table `stock` */

DROP TABLE IF EXISTS `stock`;

CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock` int(11) NOT NULL,
  `stock_min` int(11) NOT NULL,
  `stock_max` int(11) NOT NULL,
  `materia_prima_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `materia_prima_id_2` (`materia_prima_id`),
  KEY `materia_prima_id` (`materia_prima_id`),
  CONSTRAINT `stock_ibfk_1` FOREIGN KEY (`materia_prima_id`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `stock` */

insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (12,1000033,20,150,14),(14,1000033,20,150,15);

/*Table structure for table `listadocables` */

DROP TABLE IF EXISTS `listadocables`;

/*!50001 DROP VIEW IF EXISTS `listadocables` */;
/*!50001 DROP TABLE IF EXISTS `listadocables` */;

/*!50001 CREATE TABLE  `listadocables`(
 `id` int(11) ,
 `tipo` varchar(5) ,
 `componente_id` int(11) ,
 `proveedor_id` int(11) ,
 `cod_setecel` varchar(30) ,
 `materia_prima_fk` int(11) ,
 `Descripcion` varchar(15) ,
 `Unidad_Medida` varchar(11) ,
 `stock` int(11) ,
 `nombre` varchar(100) 
)*/;

/*Table structure for table `listadocomponentes` */

DROP TABLE IF EXISTS `listadocomponentes`;

/*!50001 DROP VIEW IF EXISTS `listadocomponentes` */;
/*!50001 DROP TABLE IF EXISTS `listadocomponentes` */;

/*!50001 CREATE TABLE  `listadocomponentes`(
 `id` int(11) ,
 `tipo` varchar(10) ,
 `categoria` varchar(100) ,
 `componente_id` int(11) ,
 `proveedor_id` int(11) ,
 `cod_setecel` varchar(10) ,
 `materia_prima_fk` int(11) ,
 `Descripcion` varchar(15) ,
 `Unidad_Medida` varchar(11) ,
 `stock` int(11) ,
 `nombre` varchar(100) 
)*/;

/*View structure for view listadocables */

/*!50001 DROP TABLE IF EXISTS `listadocables` */;
/*!50001 DROP VIEW IF EXISTS `listadocables` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `listadocables` AS (select `mp`.`id` AS `id`,'cable' AS `tipo`,`c`.`id` AS `componente_id`,`p`.`id` AS `proveedor_id`,`c`.`cod_setecel` AS `cod_setecel`,`c`.`materia_prima_fk` AS `materia_prima_fk`,`mp`.`Descripcion` AS `Descripcion`,`mp`.`Unidad_Medida` AS `Unidad_Medida`,`s`.`stock` AS `stock`,`p`.`nombre` AS `nombre` from (((`cable` `c` join `materia_prima` `mp` on((`mp`.`id` = `c`.`materia_prima_fk`))) join `stock` `s` on((`s`.`materia_prima_id` = `mp`.`id`))) join `proveedor` `p` on((`p`.`id` = `mp`.`id_proveedor`)))) */;

/*View structure for view listadocomponentes */

/*!50001 DROP TABLE IF EXISTS `listadocomponentes` */;
/*!50001 DROP VIEW IF EXISTS `listadocomponentes` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `listadocomponentes` AS (select `mp`.`id` AS `id`,'componente' AS `tipo`,`ca`.`nombre` AS `categoria`,`c`.`id` AS `componente_id`,`p`.`id` AS `proveedor_id`,`c`.`cod_setecel` AS `cod_setecel`,`c`.`materia_prima_fk` AS `materia_prima_fk`,`mp`.`Descripcion` AS `Descripcion`,`mp`.`Unidad_Medida` AS `Unidad_Medida`,`s`.`stock` AS `stock`,`p`.`nombre` AS `nombre` from ((((`componente` `c` join `materia_prima` `mp` on((`mp`.`id` = `c`.`materia_prima_fk`))) join `stock` `s` on((`s`.`materia_prima_id` = `mp`.`id`))) join `proveedor` `p` on((`p`.`id` = `mp`.`id_proveedor`))) join `categoria` `ca` on((`ca`.`id` = `c`.`id_categoria`)))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
