-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-01-2018 a las 23:10:17
-- Versión del servidor: 10.1.29-MariaDB
-- Versión de PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `setecel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cable`
--

CREATE TABLE `cable` (
  `id` int(11) NOT NULL,
  `cod_setecel` varchar(11) NOT NULL,
  `color_base` varchar(11) NOT NULL,
  `color_linea` varchar(11) NOT NULL,
  `seccion` float NOT NULL,
  `packaging` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `cod_proveedor` varchar(11) NOT NULL,
  `precio` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cable`
--

INSERT INTO `cable` (`id`, `cod_setecel`, `color_base`, `color_linea`, `seccion`, `packaging`, `id_proveedor`, `cod_proveedor`, `precio`) VALUES
(2, 'CA203020', 'rojo', 'negro', 0, 100, 0, 'A0,35', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`) VALUES
(1, 'Cintas'),
(2, 'Conectores'),
(3, 'EasyGun'),
(5, 'Moldeados'),
(6, 'Sellos'),
(7, 'Varios'),
(8, 'Capuchones'),
(9, 'Terminales'),
(10, 'TermoContraibles'),
(11, 'Tubos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `componente`
--

CREATE TABLE `componente` (
  `id` int(11) NOT NULL,
  `cod_setecel` varchar(10) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `descripcion` varchar(10) NOT NULL,
  `terminal_asociado` int(11) NOT NULL,
  `precio` float NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `cod_proveedor` varchar(10) NOT NULL,
  `packaging` int(11) NOT NULL,
  `cod_origen` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `componente`
--

INSERT INTO `componente` (`id`, `cod_setecel`, `id_categoria`, `descripcion`, `terminal_asociado`, `precio`, `id_proveedor`, `cod_proveedor`, `packaging`, `cod_origen`) VALUES
(1, 'MOL10003', 5, 'asded', 123, 12, 1, '1234', 12345, 'KJ89');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle`
--

CREATE TABLE `detalle` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `id_materia_prima` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle`
--

INSERT INTO `detalle` (`id`, `id_producto`, `id_materia_prima`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagen`
--

CREATE TABLE `imagen` (
  `id` int(11) NOT NULL,
  `id_pieza` int(11) NOT NULL,
  `path` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia_prima`
--

CREATE TABLE `materia_prima` (
  `id` int(11) NOT NULL,
  `Descripcion` varchar(15) NOT NULL,
  `Inactivo` tinyint(1) NOT NULL,
  `Caja_Sujecion` varchar(15) NOT NULL,
  `Stock_Minimo` float NOT NULL,
  `Stock_Maximo` float NOT NULL,
  `Stock` float NOT NULL,
  `Unidad_Medida` varchar(11) NOT NULL,
  `Ubicacion` varchar(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `materia_prima`
--

INSERT INTO `materia_prima` (`id`, `Descripcion`, `Inactivo`, `Caja_Sujecion`, `Stock_Minimo`, `Stock_Maximo`, `Stock`, `Unidad_Medida`, `Ubicacion`, `id_proveedor`) VALUES
(1, 'nada', 45, '2', 89, 87, 56, '1', 'DOS', 0),
(2, 'test', 45, '2', 89, 87, 56, '1', 'DOS', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(12) NOT NULL,
  `Diseno` varchar(100) NOT NULL,
  `FechaAlta` date NOT NULL,
  `FechaBaja` date NOT NULL,
  `Revision` varchar(30) NOT NULL,
  `Cliente` varchar(30) NOT NULL,
  `Observaciones` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `Diseno`, `FechaAlta`, `FechaBaja`, `Revision`, `Cliente`, `Observaciones`) VALUES
(1, 'Diseño Maver', '0000-00-00', '0000-00-00', 'CUALQUIER', 'AÑSDKJF', 'ÑALKFDSJ'),
(2, 'Diseño Maver', '0000-00-00', '0000-00-00', 'CUALQUIER', 'AÑSDKJF', 'ÑALKFDSJ'),
(3, 'lkjhlk', '0000-00-00', '0000-00-00', 'primera revision', 'Maverik', '123'),
(4, 'Este es un texto Largo', '0000-00-00', '0000-00-00', 'primera revision', 'Maverik', '123'),
(5, 'jk', '0000-00-00', '0000-00-00', 'CUALQUIER', 'moto', 'Ninguna'),
(6, 'jk', '0000-00-00', '0000-00-00', 'bn', 'moto', 'Ninguna'),
(7, 'jk', '0000-00-00', '0000-00-00', 'jkliuo', 'motoweds', 'Ninguna'),
(8, 'jk', '0000-00-00', '0000-00-00', 'primera revision', 'Maverik', '123'),
(9, 'jk', '0000-00-00', '0000-00-00', 'CUALQUIER', 'Maverik', 'Ver'),
(10, 'sdasdas', '0000-00-00', '0000-00-00', 'primera revision', 'Maverik', 'Ver');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cable`
--
ALTER TABLE `cable`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `componente`
--
ALTER TABLE `componente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_categoria` (`id_categoria`) USING BTREE;

--
-- Indices de la tabla `detalle`
--
ALTER TABLE `detalle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_producto` (`id_producto`),
  ADD KEY `id_materia_prima` (`id_materia_prima`);

--
-- Indices de la tabla `imagen`
--
ALTER TABLE `imagen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_materia_prima` (`id_pieza`);

--
-- Indices de la tabla `materia_prima`
--
ALTER TABLE `materia_prima`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prove` (`id_proveedor`) USING BTREE;

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `componente`
--
ALTER TABLE `componente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `detalle`
--
ALTER TABLE `detalle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `imagen`
--
ALTER TABLE `imagen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `materia_prima`
--
ALTER TABLE `materia_prima`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cable`
--
ALTER TABLE `cable`
  ADD CONSTRAINT `cable_ibfk_1` FOREIGN KEY (`id`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `componente`
--
ALTER TABLE `componente`
  ADD CONSTRAINT `componente_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `componente_ibfk_2` FOREIGN KEY (`id`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalle`
--
ALTER TABLE `detalle`
  ADD CONSTRAINT `detalle_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `detalle_ibfk_2` FOREIGN KEY (`id_materia_prima`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `imagen`
--
ALTER TABLE `imagen`
  ADD CONSTRAINT `imagen_ibfk_1` FOREIGN KEY (`id_pieza`) REFERENCES `componente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD CONSTRAINT `proveedor_ibfk_1` FOREIGN KEY (`id`) REFERENCES `materia_prima` (`id_proveedor`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
