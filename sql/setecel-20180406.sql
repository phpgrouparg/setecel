/*
SQLyog Ultimate v9.63 
MySQL - 5.7.20 : Database - setecel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`setecel` /*!40100 DEFAULT CHARACTER SET latin1 */;

/*Table structure for table `cable` */

DROP TABLE IF EXISTS `cable`;

CREATE TABLE `cable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_setecel` varchar(30) NOT NULL,
  `color_base` varchar(30) NOT NULL,
  `color_linea` varchar(30) NOT NULL,
  `seccion` float NOT NULL,
  `packaging` int(11) NOT NULL,
  `cod_proveedor` varchar(30) NOT NULL,
  `precio` float NOT NULL,
  `materia_prima_fk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `materia_prima` (`materia_prima_fk`),
  CONSTRAINT `materia_fk` FOREIGN KEY (`materia_prima_fk`) REFERENCES `materia_prima` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `cable` */

insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (1,'CA203002','Amarillo','Azul',0.35,100087,'Z85606302VL',1324.63,45);

/*Table structure for table `categoria` */

DROP TABLE IF EXISTS `categoria`;

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `categoria` */

insert  into `categoria`(`id`,`nombre`) values (12,'Capuchones'),(13,'Cintas'),(14,'Conectores'),(15,'Easy Gun'),(16,'Moldeados'),(17,'Sellos '),(18,'Terminales'),(19,'Termocontraibles'),(20,'Tubos'),(21,'Varios');

/*Table structure for table `cliente` */

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `telefono` int(50) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `dni_cuil_cuit` int(14) NOT NULL,
  `codigo_postal` varchar(50) DEFAULT NULL,
  `localidad` varchar(50) NOT NULL,
  `provincia` varchar(50) NOT NULL,
  `barrio` varchar(50) NOT NULL,
  `fecha_alta` date NOT NULL,
  `fecha_baja` date DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `rubro` varchar(50) DEFAULT NULL,
  `situacion_fiscal` varchar(50) DEFAULT NULL,
  `web` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `cliente` */

insert  into `cliente`(`id`,`nombre`,`telefono`,`direccion`,`dni_cuil_cuit`,`codigo_postal`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`,`email`,`rubro`,`situacion_fiscal`,`web`) values (1,'Primer Cliente',347373939,'av siempre viva',2147483647,'5425','rawson','san juan','rawson','2018-03-19',NULL,'nestorestrada@gmail.com','ventas','responsable inscripto','www.ella.com.ar'),(2,'Omarsiño',453535353,'av añkdjñfal',2147483647,'43234','asdfadsf adfa ','adsfasd','adsfasd','2011-11-11','2011-11-11','asdfasdf','adfasdfasdf','adsfadsfa','asdfasdf');

/*Table structure for table `componente` */

DROP TABLE IF EXISTS `componente`;

CREATE TABLE `componente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_setecel` varchar(10) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `terminal_asociado` varchar(10) NOT NULL,
  `precio` float NOT NULL,
  `cod_proveedor` varchar(10) NOT NULL,
  `packaging` int(11) NOT NULL,
  `cod_origen` varchar(10) NOT NULL,
  `materia_prima_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_categoria` (`id_categoria`) USING BTREE,
  KEY `componente_fk_mp` (`materia_prima_fk`),
  CONSTRAINT `componente_cat` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`),
  CONSTRAINT `componente_fk_mp` FOREIGN KEY (`materia_prima_fk`) REFERENCES `materia_prima` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `componente` */

insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (16,'1231233',12,'1231233HH',1234,'780',1235,'WEWQ123',46),(17,'123123',14,'no tiene',1234,'780',18900,'WEWQ123',50),(18,'rj4500001',14,'utp',0.0458,'rj45885522',1000,'rj45889966',51);

/*Table structure for table `cruge_authassignment` */

DROP TABLE IF EXISTS `cruge_authassignment`;

CREATE TABLE `cruge_authassignment` (
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text,
  `itemname` varchar(64) NOT NULL,
  PRIMARY KEY (`userid`,`itemname`),
  KEY `fk_cruge_authassignment_cruge_authitem1` (`itemname`),
  KEY `fk_cruge_authassignment_user` (`userid`),
  CONSTRAINT `fk_cruge_authassignment_cruge_authitem1` FOREIGN KEY (`itemname`) REFERENCES `cruge_authitem` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cruge_authassignment_user` FOREIGN KEY (`userid`) REFERENCES `cruge_user` (`iduser`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_authassignment` */

/*Table structure for table `cruge_authitem` */

DROP TABLE IF EXISTS `cruge_authitem`;

CREATE TABLE `cruge_authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_authitem` */

insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_cable_admin',0,'',NULL,'N;'),('action_cable_create',0,'',NULL,'N;'),('action_cable_index',0,'',NULL,'N;'),('action_cable_update',0,'',NULL,'N;'),('action_cable_view',0,'',NULL,'N;'),('action_cliente_admin',0,'',NULL,'N;'),('action_cliente_create',0,'',NULL,'N;'),('action_cliente_index',0,'',NULL,'N;'),('action_cliente_update',0,'',NULL,'N;'),('action_cliente_view',0,'',NULL,'N;'),('action_componente_admin',0,'',NULL,'N;'),('action_componente_create',0,'',NULL,'N;'),('action_componente_delete',0,'',NULL,'N;'),('action_componente_index',0,'',NULL,'N;'),('action_componente_update',0,'',NULL,'N;'),('action_componente_view',0,'',NULL,'N;'),('action_detalleParte_admin',0,'',NULL,'N;'),('action_detalleParte_create',0,'',NULL,'N;'),('action_detalleParte_index',0,'',NULL,'N;'),('action_imagen_admin',0,'',NULL,'N;'),('action_imagen_create',0,'',NULL,'N;'),('action_imagen_delete',0,'',NULL,'N;'),('action_imagen_index',0,'',NULL,'N;'),('action_imagen_update',0,'',NULL,'N;'),('action_imagen_view',0,'',NULL,'N;'),('action_listadocables_admin',0,'',NULL,'N;'),('action_listadocables_index',0,'',NULL,'N;'),('action_listadocomponentes_admin',0,'',NULL,'N;'),('action_listadocomponentes_index',0,'',NULL,'N;'),('action_listadocomponentes_view',0,'',NULL,'N;'),('action_materiaPrima_admin',0,'',NULL,'N;'),('action_materiaPrima_create',0,'',NULL,'N;'),('action_materiaPrima_index',0,'',NULL,'N;'),('action_materiaPrima_update',0,'',NULL,'N;'),('action_materiaPrima_view',0,'',NULL,'N;'),('action_producto_admin',0,'',NULL,'N;'),('action_producto_create',0,'',NULL,'N;'),('action_producto_delete',0,'',NULL,'N;'),('action_producto_index',0,'',NULL,'N;'),('action_producto_update',0,'',NULL,'N;'),('action_producto_view',0,'',NULL,'N;'),('action_proveedor_admin',0,'',NULL,'N;'),('action_proveedor_create',0,'',NULL,'N;'),('action_proveedor_delete',0,'',NULL,'N;'),('action_proveedor_index',0,'',NULL,'N;'),('action_proveedor_update',0,'',NULL,'N;'),('action_proveedor_view',0,'',NULL,'N;'),('action_stock_admin',0,'',NULL,'N;'),('action_stock_crearcable',0,'',NULL,'N;'),('action_stock_create',0,'',NULL,'N;'),('action_stock_index',0,'',NULL,'N;'),('action_stock_update',0,'',NULL,'N;'),('action_ui_usermanagementadmin',0,'',NULL,'N;'),('admin',0,'',NULL,'N;');

/*Table structure for table `cruge_authitemchild` */

DROP TABLE IF EXISTS `cruge_authitemchild`;

CREATE TABLE `cruge_authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `crugeauthitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `cruge_authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `crugeauthitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `cruge_authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_authitemchild` */

/*Table structure for table `cruge_field` */

DROP TABLE IF EXISTS `cruge_field`;

CREATE TABLE `cruge_field` (
  `idfield` int(11) NOT NULL AUTO_INCREMENT,
  `fieldname` varchar(20) NOT NULL,
  `longname` varchar(50) DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  `required` int(11) DEFAULT '0',
  `fieldtype` int(11) DEFAULT '0',
  `fieldsize` int(11) DEFAULT '20',
  `maxlength` int(11) DEFAULT '45',
  `showinreports` int(11) DEFAULT '0',
  `useregexp` varchar(512) DEFAULT NULL,
  `useregexpmsg` varchar(512) DEFAULT NULL,
  `predetvalue` mediumblob,
  PRIMARY KEY (`idfield`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_field` */

/*Table structure for table `cruge_fieldvalue` */

DROP TABLE IF EXISTS `cruge_fieldvalue`;

CREATE TABLE `cruge_fieldvalue` (
  `idfieldvalue` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `idfield` int(11) NOT NULL,
  `value` blob,
  PRIMARY KEY (`idfieldvalue`),
  KEY `fk_cruge_fieldvalue_cruge_user1` (`iduser`),
  KEY `fk_cruge_fieldvalue_cruge_field1` (`idfield`),
  CONSTRAINT `fk_cruge_fieldvalue_cruge_field1` FOREIGN KEY (`idfield`) REFERENCES `cruge_field` (`idfield`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_cruge_fieldvalue_cruge_user1` FOREIGN KEY (`iduser`) REFERENCES `cruge_user` (`iduser`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_fieldvalue` */

/*Table structure for table `cruge_session` */

DROP TABLE IF EXISTS `cruge_session`;

CREATE TABLE `cruge_session` (
  `idsession` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `created` bigint(30) DEFAULT NULL,
  `expire` bigint(30) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `ipaddress` varchar(45) DEFAULT NULL,
  `usagecount` int(11) DEFAULT '0',
  `lastusage` bigint(30) DEFAULT NULL,
  `logoutdate` bigint(30) DEFAULT NULL,
  `ipaddressout` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idsession`),
  KEY `crugesession_iduser` (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

/*Data for the table `cruge_session` */

insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (1,1,1517261222,1517263022,0,'::1',1,1517261222,NULL,NULL),(2,1,1517311377,1517313177,1,'::1',2,1517311403,NULL,NULL),(3,1,1517602714,1517604514,0,'::1',1,1517602714,NULL,NULL),(4,1,1517605429,1517607229,0,'::1',2,1517605568,NULL,NULL),(5,1,1517841324,1517843124,0,'::1',1,1517841324,NULL,NULL),(6,1,1518033328,1518035128,1,'::1',1,1518033328,NULL,NULL),(7,1,1518751353,1518753153,1,'::1',1,1518751353,NULL,NULL),(8,1,1518753254,1518755054,1,'::1',3,1518753808,NULL,NULL),(9,1,1518819666,1518821466,0,'::1',1,1518819666,NULL,NULL),(10,1,1518821572,1518823372,1,'::1',1,1518821572,NULL,NULL),(11,1,1519776770,1519778570,0,'::1',2,1519777473,NULL,NULL),(12,1,1519779095,1519780895,0,'::1',2,1519779391,NULL,NULL),(13,1,1519781153,1519782953,1,'::1',1,1519781153,NULL,NULL),(14,1,1519790850,1519792650,0,'::1',2,1519792042,NULL,NULL),(15,1,1519794690,1519796490,1,'::1',1,1519794690,NULL,NULL),(16,1,1519942871,1519944671,0,'::1',1,1519942871,NULL,NULL),(17,1,1519949594,1519951394,1,'::1',1,1519949594,NULL,NULL),(18,1,1519957381,1519959181,1,'::1',1,1519957381,NULL,NULL),(19,1,1519991372,1519993172,0,'::1',1,1519991372,NULL,NULL),(20,1,1519993746,1519995546,1,'::1',1,1519993746,NULL,NULL),(21,1,1520024119,1520025919,0,'::1',1,1520024119,NULL,NULL),(22,1,1520026653,1520028453,0,'::1',1,1520026653,NULL,NULL),(23,1,1520028754,1520030554,0,'::1',3,1520029664,NULL,NULL),(24,1,1520035698,1520037498,0,'::1',1,1520035698,NULL,NULL),(25,1,1520093776,1520095576,0,'::1',2,1520093957,NULL,NULL),(26,1,1520104182,1520105982,1,'::1',1,1520104182,NULL,NULL),(27,1,1520198793,1520200593,0,'::1',1,1520198793,NULL,NULL),(28,1,1520208473,1520210273,1,'::1',2,1520208504,NULL,NULL),(29,1,1520257432,1520259232,0,'::1',1,1520257432,NULL,NULL),(30,1,1520260358,1520262158,0,'::1',1,1520260358,NULL,NULL),(31,1,1520279027,1520280827,0,'::1',2,1520280212,NULL,NULL),(32,1,1520288472,1520290272,0,'::1',1,1520288472,NULL,NULL),(33,1,1520291562,1520293362,0,'::1',1,1520291562,NULL,NULL),(34,1,1520302553,1520304353,0,'::1',3,1520302640,NULL,NULL),(35,1,1520387604,1520389404,0,'::1',1,1520387604,NULL,NULL),(36,1,1520392761,1520394561,0,'::1',3,1520393154,NULL,NULL),(37,1,1520396578,1520398378,0,'::1',1,1520396578,NULL,NULL),(38,1,1520399109,1520400909,0,'::1',1,1520399109,NULL,NULL),(39,1,1520425344,1520427144,0,'::1',2,1520426373,NULL,NULL),(40,1,1520447472,1520449272,0,'::1',1,1520447472,NULL,NULL),(41,1,1520451295,1520453095,0,'::1',3,1520452353,NULL,NULL),(42,1,1520453979,1520455779,0,'::1',3,1520454427,NULL,NULL),(43,1,1520475880,1520477680,0,'::1',1,1520475880,NULL,NULL),(44,1,1520479777,1520481577,0,'::1',1,1520479777,NULL,NULL),(45,1,1520530487,1520532287,0,'::1',1,1520530487,NULL,NULL),(46,1,1520535093,1520536893,0,'::1',2,1520535637,1520535653,'::1'),(47,1,1520535780,1520537580,0,'::1',1,1520535780,NULL,NULL),(48,1,1520538512,1520540312,0,'::1',1,1520538512,NULL,NULL),(49,1,1520562384,1520564184,0,'::1',3,1520563778,NULL,NULL),(50,1,1520564294,1520566094,1,'::1',4,1520566034,NULL,NULL),(51,1,1520638805,1520640605,1,'::1',1,1520638805,NULL,NULL),(52,1,1521166586,1521168386,0,'::1',2,1521167159,NULL,NULL),(53,1,1521173443,1521175243,1,'::1',1,1521173443,NULL,NULL),(54,1,1521235752,1521237552,1,'127.0.0.1',1,1521235752,NULL,NULL),(55,1,1521255894,1521257694,0,'127.0.0.1',1,1521255894,NULL,NULL),(56,1,1521258947,1521260747,0,'127.0.0.1',1,1521258947,NULL,NULL),(57,1,1521260936,1521262736,1,'127.0.0.1',1,1521260936,NULL,NULL),(58,1,1521311493,1521313293,0,'127.0.0.1',1,1521311493,NULL,NULL),(59,1,1521316608,1521318408,1,'127.0.0.1',1,1521316608,NULL,NULL),(60,1,1521720901,1521722701,1,'127.0.0.1',1,1521720901,NULL,NULL),(61,1,1521760470,1521762270,1,'127.0.0.1',1,1521760470,NULL,NULL),(62,1,1521775491,1521777291,1,'127.0.0.1',1,1521775491,NULL,NULL),(63,1,1522964912,1522966712,1,'127.0.0.1',1,1522964912,NULL,NULL),(64,1,1522985700,1522987500,0,'127.0.0.1',1,1522985700,NULL,NULL),(65,1,1522991407,1522993207,0,'127.0.0.1',1,1522991407,NULL,NULL);

/*Table structure for table `cruge_system` */

DROP TABLE IF EXISTS `cruge_system`;

CREATE TABLE `cruge_system` (
  `idsystem` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `largename` varchar(45) DEFAULT NULL,
  `sessionmaxdurationmins` int(11) DEFAULT '30',
  `sessionmaxsameipconnections` int(11) DEFAULT '10',
  `sessionreusesessions` int(11) DEFAULT '1' COMMENT '1yes 0no',
  `sessionmaxsessionsperday` int(11) DEFAULT '-1',
  `sessionmaxsessionsperuser` int(11) DEFAULT '-1',
  `systemnonewsessions` int(11) DEFAULT '0' COMMENT '1yes 0no',
  `systemdown` int(11) DEFAULT '0',
  `registerusingcaptcha` int(11) DEFAULT '0',
  `registerusingterms` int(11) DEFAULT '0',
  `terms` blob,
  `registerusingactivation` int(11) DEFAULT '1',
  `defaultroleforregistration` varchar(64) DEFAULT NULL,
  `registerusingtermslabel` varchar(100) DEFAULT NULL,
  `registrationonlogin` int(11) DEFAULT '1',
  PRIMARY KEY (`idsystem`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `cruge_system` */

insert  into `cruge_system`(`idsystem`,`name`,`largename`,`sessionmaxdurationmins`,`sessionmaxsameipconnections`,`sessionreusesessions`,`sessionmaxsessionsperday`,`sessionmaxsessionsperuser`,`systemnonewsessions`,`systemdown`,`registerusingcaptcha`,`registerusingterms`,`terms`,`registerusingactivation`,`defaultroleforregistration`,`registerusingtermslabel`,`registrationonlogin`) values (1,'default',NULL,30,10,1,-1,-1,0,0,0,0,'',0,'','',1);

/*Table structure for table `cruge_user` */

DROP TABLE IF EXISTS `cruge_user`;

CREATE TABLE `cruge_user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `regdate` bigint(30) DEFAULT NULL,
  `actdate` bigint(30) DEFAULT NULL,
  `logondate` bigint(30) DEFAULT NULL,
  `username` varchar(64) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL COMMENT 'Hashed password',
  `authkey` varchar(100) DEFAULT NULL COMMENT 'llave de autentificacion',
  `state` int(11) DEFAULT '0',
  `totalsessioncounter` int(11) DEFAULT '0',
  `currentsessioncounter` int(11) DEFAULT '0',
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `cruge_user` */

insert  into `cruge_user`(`iduser`,`regdate`,`actdate`,`logondate`,`username`,`email`,`password`,`authkey`,`state`,`totalsessioncounter`,`currentsessioncounter`) values (1,NULL,NULL,1522991407,'admin','admin@tucorreo.com','admin',NULL,1,0,0),(2,NULL,NULL,NULL,'invitado','invitado','nopassword',NULL,1,0,0);

/*Table structure for table `detalle` */

DROP TABLE IF EXISTS `detalle`;

CREATE TABLE `detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `id_materia_prima` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_producto` (`id_producto`),
  KEY `id_materia_prima` (`id_materia_prima`),
  CONSTRAINT `detalle_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detalle_ibfk_2` FOREIGN KEY (`id_materia_prima`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detalle` */

/*Table structure for table `detalle_parte` */

DROP TABLE IF EXISTS `detalle_parte`;

CREATE TABLE `detalle_parte` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `materia_prima_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  `numero_revision` int(11) DEFAULT NULL,
  `cantidad_cable` int(11) DEFAULT NULL,
  `cantidad_comp` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `materia_prima_fk` (`materia_prima_id`),
  KEY `producto_fk` (`producto_id`),
  CONSTRAINT `materia_prima_fk` FOREIGN KEY (`materia_prima_id`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `producto_fk` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `detalle_parte` */

/*Table structure for table `imagen` */

DROP TABLE IF EXISTS `imagen`;

CREATE TABLE `imagen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pieza` int(11) NOT NULL,
  `path` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_materia_prima` (`id_pieza`),
  CONSTRAINT `imagen_ibfk_1` FOREIGN KEY (`id_pieza`) REFERENCES `componente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `imagen` */

insert  into `imagen`(`id`,`id_pieza`,`path`) values (1,16,'conector.jpg'),(2,16,'coonector2.jpg'),(3,17,'coonector2.jpg'),(6,18,'51LBNKtT7PL._SL1067_.jpg');

/*Table structure for table `materia_prima` */

DROP TABLE IF EXISTS `materia_prima`;

CREATE TABLE `materia_prima` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(255) NOT NULL,
  `Inactivo` tinyint(1) NOT NULL,
  `Caja_Sujecion` varchar(50) DEFAULT NULL,
  `Unidad_Medida` varchar(50) NOT NULL,
  `Ubicacion` varchar(50) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prove` (`id_proveedor`) USING BTREE,
  KEY `materiaprimatipo` (`id_tipo`),
  CONSTRAINT `materiaprimatipo` FOREIGN KEY (`id_tipo`) REFERENCES `materiaprimatipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `proveedor_fk` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

/*Data for the table `materia_prima` */

insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (15,'testv',1,'2','asdas','DOS',3,NULL),(16,'test',0,'asdasd','1','asd',3,NULL),(17,'Cable nuevo  biiennh sjj567',0,'no','mm2','DOS asdfads',3,1),(18,'nada',1,'dasd','asda','DOS',3,NULL),(19,'nadakk',0,'dasd','1','sdasdas',3,NULL),(20,'LÑ',1,'dasd','asdas','asd',3,NULL),(21,'LÑ',1,'dasd','asdas','asd',3,NULL),(22,'asdasda 3333',0,'dasd','mm2','DOS',3,NULL),(23,'Nuevo tipo de conector',1,'Caja uno','unidad','Cajas ',3,NULL),(24,'test nuevo',0,'no tiene','ti','no',3,NULL),(25,' nuevo componentejjj 567',0,'no tiene adfasdf','mm2','Ubicacionlñ',5,NULL),(26,'descripcion valida',0,'no','seccion','frente',3,NULL),(27,'test12',0,'no','seccion','DOS',5,NULL),(28,'nueva descripcion',0,'89','seccion','no tiene',5,NULL),(29,'8877721',0,'no posee','mm2','DOS',3,2),(30,'test para probar inactivo o no ',1,'no tiene','Seccion:mm2','Ubicado en caja de madera',5,NULL),(31,'Otro test',0,'no tiene','Seccion:mm2','en la caja azul',4,NULL),(32,'test1211',0,'dasd','mm2','DOS',5,NULL),(33,'test12',0,'no tiene','Unidad','DOS',4,NULL),(34,'Nuevo Producto',0,'Si','mm2','nosewww',4,NULL),(35,'nada',0,'no tiene','mm2','DOS',3,1),(36,'nada',0,'dasd','mm2','DOS',3,NULL),(37,'test',0,'dasd','mm2','DOS',4,NULL),(38,'Probando editar un cable',0,'no tiene','mm2','DOS',4,NULL),(39,'test121',0,'aa','mm2','sdasdas',3,NULL),(40,'test12',0,'dasd','mm2','sdasdas',3,NULL),(41,'nada',0,'2','mm2','sdasdas',3,NULL),(42,'Nueva prueba de cable 2',0,'por ahora no','mm2','ewq',3,NULL),(43,'Probando para editar',0,'dasd','Unidad','DOS',4,2),(44,'Esto es un nuevo test Para editar Cabless editando',0,'dasd','mm2','DOS',3,1),(45,'Primer Cable Azul y Amarillo Como Boquita',0,'No tiene por ahora','mm2','No tiene',3,1),(46,'Primer componente de prueba',0,'No tiene','mm2','No tiene',4,2),(47,'Decode Software ',0,'No tiene','mm2','por ahi',4,1),(48,'jñlkjñlk ñlkj ñlkjñ kjñ lk jñlkj ñlkj ñlkj ñlkj ñlkjkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk',0,'2','mm2','DOS',3,2),(49,'textos hacen parecerlo un español que se puede leer. Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de \"Lorem Ipsum\" va a dar por resultado muchos sitios web que usan este te',0,'dasd','mm2','DOS',3,2),(50,'textos hacen parecerlo un español que se puede leer. Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de \"Lorem Ipsum\" va a dar por resultado muchos sitios web que usan este te',0,'','mm2','sdasdas',4,2),(51,'RJ45',1,'','Unidad','deposito',4,2);

/*Table structure for table `materiaprimatipo` */

DROP TABLE IF EXISTS `materiaprimatipo`;

CREATE TABLE `materiaprimatipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `materiaprimatipo` */

insert  into `materiaprimatipo`(`id`,`nombre`) values (1,'Cable'),(2,'Componente');

/*Table structure for table `producto` */

DROP TABLE IF EXISTS `producto`;

CREATE TABLE `producto` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `Diseno` varchar(100) DEFAULT NULL,
  `FechaAlta` date DEFAULT NULL,
  `FechaBaja` date DEFAULT NULL,
  `Observaciones` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `producto` */

insert  into `producto`(`id`,`Diseno`,`FechaAlta`,`FechaBaja`,`Observaciones`) values (3,'Diseño Maverik','2018-03-02','2018-03-15','Ninguna'),(4,'Diseño Maverik','2018-03-02','2018-03-16','Ver. esto es una prueba para Nestor'),(5,'Diseño Maverik','2018-03-08','0000-00-00','Nuevo conector para guiñes '),(6,'Nestor Design','2018-04-06',NULL,'Esto es una prueba'),(7,'Omar Design','2018-04-06',NULL,'Esto es otra prueba de Nestor joder tio');

/*Table structure for table `proveedor` */

DROP TABLE IF EXISTS `proveedor`;

CREATE TABLE `proveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `dni_cuil_cuit` int(14) DEFAULT NULL,
  `codigo_postal` varchar(30) DEFAULT NULL,
  `direccion` varchar(100) NOT NULL,
  `localidad` varchar(50) DEFAULT NULL,
  `provincia` varchar(40) DEFAULT NULL,
  `barrio` varchar(30) DEFAULT NULL,
  `fecha_alta` date DEFAULT NULL,
  `fecha_baja` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `proveedor` */

insert  into `proveedor`(`id`,`nombre`,`telefono`,`email`,`dni_cuil_cuit`,`codigo_postal`,`direccion`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`) values (3,'Trielec','3456787','nestorestrada@gmail.com',33059373,'5425','av siempre viva 1298','sprinfield ','san juan','rawson','2017-09-23','0000-00-00'),(4,'Omar','687934','nestorestrada@gmail.com',23444567,'547','cordoba 89','rawson','san juan','obrero','2018-03-09','2018-03-22'),(5,'Cables Argentinos SA','687934','nestorestrada@gmail.com',23444567,'547','cordoba 89','rawson','san juan','obrero','2018-03-09','2018-03-22');

/*Table structure for table `stock` */

DROP TABLE IF EXISTS `stock`;

CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock` int(11) NOT NULL,
  `stock_min` int(11) NOT NULL,
  `stock_max` int(11) NOT NULL,
  `materia_prima_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `materia_prima_id_2` (`materia_prima_id`),
  KEY `materia_prima_id` (`materia_prima_id`),
  CONSTRAINT `stock_ibfk_1` FOREIGN KEY (`materia_prima_id`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

/*Data for the table `stock` */

insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (14,1000033,20,150,15),(15,1200999,123,5000,17),(16,1000033,20,150,22),(17,100003,123,150,25),(18,1000033,20,150,27),(19,15444,50,333,28),(20,1200,20,333,29),(21,1000033,11,5000,30),(22,1000033,123,5000,31),(23,1000033,123,333,32),(24,100003,20,150,33),(25,58000000,123,5000,34),(26,2147483647,50,150,35),(27,1000033,123,150,36),(28,1000033,123,150,37),(29,1000033,20,150,38),(30,1000,20,5000,39),(31,1000033,11,5000,40),(32,1000033,20,5000,42),(33,15444,123,333,43),(34,10000311,50,150,44),(35,23000,2505,50000,45),(36,70000,1000,10000,46),(37,1000033,123,5000,50),(38,0,0,0,51);

/*Table structure for table `listadocables` */

DROP TABLE IF EXISTS `listadocables`;

/*!50001 DROP VIEW IF EXISTS `listadocables` */;
/*!50001 DROP TABLE IF EXISTS `listadocables` */;

/*!50001 CREATE TABLE  `listadocables`(
 `id` int(11) ,
 `tipo` varchar(5) ,
 `componente_id` int(11) ,
 `proveedor_id` int(11) ,
 `cod_setecel` varchar(30) ,
 `materia_prima_fk` int(11) ,
 `Descripcion` varchar(255) ,
 `Unidad_Medida` varchar(50) ,
 `stock` int(11) ,
 `nombre` varchar(100) 
)*/;

/*Table structure for table `listadocomponentes` */

DROP TABLE IF EXISTS `listadocomponentes`;

/*!50001 DROP VIEW IF EXISTS `listadocomponentes` */;
/*!50001 DROP TABLE IF EXISTS `listadocomponentes` */;

/*!50001 CREATE TABLE  `listadocomponentes`(
 `id` int(11) ,
 `tipo` varchar(10) ,
 `categoria` varchar(100) ,
 `componente_id` int(11) ,
 `proveedor_id` int(11) ,
 `cod_setecel` varchar(10) ,
 `materia_prima_fk` int(11) ,
 `Descripcion` varchar(255) ,
 `Unidad_Medida` varchar(50) ,
 `stock` int(11) ,
 `nombre` varchar(100) 
)*/;

/*View structure for view listadocables */

/*!50001 DROP TABLE IF EXISTS `listadocables` */;
/*!50001 DROP VIEW IF EXISTS `listadocables` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `listadocables` AS (select `mp`.`id` AS `id`,'cable' AS `tipo`,`c`.`id` AS `componente_id`,`p`.`id` AS `proveedor_id`,`c`.`cod_setecel` AS `cod_setecel`,`c`.`materia_prima_fk` AS `materia_prima_fk`,`mp`.`Descripcion` AS `Descripcion`,`mp`.`Unidad_Medida` AS `Unidad_Medida`,`s`.`stock` AS `stock`,`p`.`nombre` AS `nombre` from (((`cable` `c` join `materia_prima` `mp` on((`mp`.`id` = `c`.`materia_prima_fk`))) join `stock` `s` on((`s`.`materia_prima_id` = `mp`.`id`))) join `proveedor` `p` on((`p`.`id` = `mp`.`id_proveedor`)))) */;

/*View structure for view listadocomponentes */

/*!50001 DROP TABLE IF EXISTS `listadocomponentes` */;
/*!50001 DROP VIEW IF EXISTS `listadocomponentes` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `listadocomponentes` AS (select `mp`.`id` AS `id`,'componente' AS `tipo`,`ca`.`nombre` AS `categoria`,`c`.`id` AS `componente_id`,`p`.`id` AS `proveedor_id`,`c`.`cod_setecel` AS `cod_setecel`,`c`.`materia_prima_fk` AS `materia_prima_fk`,`mp`.`Descripcion` AS `Descripcion`,`mp`.`Unidad_Medida` AS `Unidad_Medida`,`s`.`stock` AS `stock`,`p`.`nombre` AS `nombre` from ((((`componente` `c` join `materia_prima` `mp` on((`mp`.`id` = `c`.`materia_prima_fk`))) join `stock` `s` on((`s`.`materia_prima_id` = `mp`.`id`))) join `proveedor` `p` on((`p`.`id` = `mp`.`id_proveedor`))) join `categoria` `ca` on((`ca`.`id` = `c`.`id_categoria`)))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
