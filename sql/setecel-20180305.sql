/*
SQLyog Ultimate v9.63 
MySQL - 5.5.5-10.1.29-MariaDB : Database - setecel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`setecel` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `setecel`;

/*Table structure for table `cable` */

DROP TABLE IF EXISTS `cable`;

CREATE TABLE `cable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_setecel` varchar(30) NOT NULL,
  `color_base` varchar(30) NOT NULL,
  `color_linea` varchar(30) NOT NULL,
  `seccion` float NOT NULL,
  `packaging` int(11) NOT NULL,
  `cod_proveedor` varchar(30) NOT NULL,
  `precio` float NOT NULL,
  `materia_prima_fk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `materia_prima` (`materia_prima_fk`),
  CONSTRAINT `materia_fk` FOREIGN KEY (`materia_prima_fk`) REFERENCES `materia_prima` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `cable` */

insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (4,'CA203002','Amarillo','Amarillo',0.35,100008,'Z85606302VL',78990,13),(5,'CA203002','Rosado','Negro',2.5,1000,'Z85606302VL',1324.48,16),(6,'CA203002','Verde-Claro','Amarillo',0.35,1000,'Z85606302VL',5000,17),(7,'CA203000','Verde-Claro','Verde',4,1000,'Z85606302VL',5000,18),(8,'CA203000','Verde-Claro','Verde',4,1000,'Z85606302VL',5000,18),(9,'CA203002','Amarillo','Amarillo',0.35,123,'Z85606302VL',1324.48,22),(10,'CA203000','Amarillo','Amarillo',0.35,1000,'Z85606302VL',1324.48,25),(11,'CA203000','Violeta','Rojo',1,1000,'Z85606302VL',1324.48,30),(12,'1231233','Verde-Claro','Rojo',0.75,1000,'Z85606302VL',1324.48,31),(13,'2casdc','Amarillo','Amarillo',0.35,1000,'Z85606302VL',1324.48,32),(14,'1231233','Verde-Claro','Rojo',0.35,10000,'Z85606302VL',1324.48,35),(15,'CA203002','Amarillo','Amarillo',0.35,9000,'Z85606302VL',50002,37),(16,'CA203002','Amarillo','Amarillo',0.35,1000,'Z85606302VL',1324.48,38),(17,'CA203002','Amarillo','Amarillo',0.35,1000,'Z85606302VL',1324.48,39),(18,'CA203002','Amarillo','Amarillo',1,1000,'Z85606302VL',1324.48,39),(19,'CA203002','Amarillo','Amarillo',1,1000,'Z85606302VL',1324.48,40);

/*Table structure for table `categoria` */

DROP TABLE IF EXISTS `categoria`;

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `categoria` */

insert  into `categoria`(`id`,`nombre`) values (12,'Capuchones'),(13,'Cintas'),(14,'Conectores'),(15,'Easy Gun'),(16,'Moldeados'),(17,'Sellos '),(18,'Terminales'),(19,'Termocontraibles'),(20,'Tubos'),(21,'Varios');

/*Table structure for table `cliente` */

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `telefono` int(50) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `dni_cuil_cuit` int(14) NOT NULL,
  `codigo_postal` varchar(50) DEFAULT NULL,
  `localidad` varchar(50) NOT NULL,
  `provincia` varchar(50) NOT NULL,
  `barrio` varchar(50) NOT NULL,
  `fecha_alta` date NOT NULL,
  `fecha_baja` date DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `rubro` varchar(50) DEFAULT NULL,
  `situacion_fiscal` varchar(50) DEFAULT NULL,
  `web` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `cliente` */

insert  into `cliente`(`id`,`nombre`,`telefono`,`direccion`,`dni_cuil_cuit`,`codigo_postal`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`,`email`,`rubro`,`situacion_fiscal`,`web`) values (1,'Primer Cliente',347373939,'av siempre viva',2147483647,'5425','rawson','san juan','rawson','2018-03-19',NULL,'nestorestrada@gmail.com','ventas','responsable inscripto','www.ella.com.ar'),(2,'Omarsiño',453535353,'av añkdjñfal',2147483647,'43234','asdfadsf adfa ','adsfasd','adsfasd','2011-11-11','2011-11-11','asdfasdf','adfasdfasdf','adsfadsfa','asdfasdf');

/*Table structure for table `componente` */

DROP TABLE IF EXISTS `componente`;

CREATE TABLE `componente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_setecel` varchar(10) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `terminal_asociado` varchar(10) NOT NULL,
  `precio` float NOT NULL,
  `cod_proveedor` varchar(10) NOT NULL,
  `packaging` int(11) NOT NULL,
  `cod_origen` varchar(10) NOT NULL,
  `materia_prima_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_categoria` (`id_categoria`) USING BTREE,
  KEY `componente_fk_mp` (`materia_prima_fk`),
  CONSTRAINT `componente_cat` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`),
  CONSTRAINT `componente_fk_mp` FOREIGN KEY (`materia_prima_fk`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `componente` */

insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`descripcion`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (10,'123123AA',19,'Componentes termocontraibles','1234',1234,'780',18900,'jdi800',28),(11,'123123',12,'Capuchon para easy gun y otras cosas','234',78,'780AA',10099,'WEWQ123',29),(12,'23482',16,'Capuchon para easy gun y otras cosas','7899',1234,'67',100,'WEWQ123',33),(13,'23482',13,'Capuchon para easy gun y otras cosas mucho masssss','6789',1234,'780',18900,'jdi8',34),(14,'123123',14,'ajñfsdlka','no tiene',1234,'780',100,'WEWQ123',36);

/*Table structure for table `cruge_authassignment` */

DROP TABLE IF EXISTS `cruge_authassignment`;

CREATE TABLE `cruge_authassignment` (
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text,
  `itemname` varchar(64) NOT NULL,
  PRIMARY KEY (`userid`,`itemname`),
  KEY `fk_cruge_authassignment_cruge_authitem1` (`itemname`),
  KEY `fk_cruge_authassignment_user` (`userid`),
  CONSTRAINT `fk_cruge_authassignment_cruge_authitem1` FOREIGN KEY (`itemname`) REFERENCES `cruge_authitem` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cruge_authassignment_user` FOREIGN KEY (`userid`) REFERENCES `cruge_user` (`iduser`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_authassignment` */

/*Table structure for table `cruge_authitem` */

DROP TABLE IF EXISTS `cruge_authitem`;

CREATE TABLE `cruge_authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_authitem` */

insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_cable_admin',0,'',NULL,'N;'),('action_cable_create',0,'',NULL,'N;'),('action_cable_index',0,'',NULL,'N;'),('action_cable_update',0,'',NULL,'N;'),('action_cable_view',0,'',NULL,'N;'),('action_cliente_admin',0,'',NULL,'N;'),('action_cliente_create',0,'',NULL,'N;'),('action_cliente_index',0,'',NULL,'N;'),('action_cliente_update',0,'',NULL,'N;'),('action_cliente_view',0,'',NULL,'N;'),('action_componente_admin',0,'',NULL,'N;'),('action_componente_create',0,'',NULL,'N;'),('action_componente_delete',0,'',NULL,'N;'),('action_componente_index',0,'',NULL,'N;'),('action_componente_update',0,'',NULL,'N;'),('action_componente_view',0,'',NULL,'N;'),('action_imagen_admin',0,'',NULL,'N;'),('action_imagen_create',0,'',NULL,'N;'),('action_imagen_delete',0,'',NULL,'N;'),('action_imagen_index',0,'',NULL,'N;'),('action_imagen_update',0,'',NULL,'N;'),('action_imagen_view',0,'',NULL,'N;'),('action_listadocables_admin',0,'',NULL,'N;'),('action_listadocables_index',0,'',NULL,'N;'),('action_listadocomponentes_admin',0,'',NULL,'N;'),('action_listadocomponentes_index',0,'',NULL,'N;'),('action_listadocomponentes_view',0,'',NULL,'N;'),('action_materiaPrima_admin',0,'',NULL,'N;'),('action_materiaPrima_create',0,'',NULL,'N;'),('action_materiaPrima_index',0,'',NULL,'N;'),('action_materiaPrima_update',0,'',NULL,'N;'),('action_materiaPrima_view',0,'',NULL,'N;'),('action_producto_admin',0,'',NULL,'N;'),('action_producto_create',0,'',NULL,'N;'),('action_producto_delete',0,'',NULL,'N;'),('action_producto_index',0,'',NULL,'N;'),('action_producto_update',0,'',NULL,'N;'),('action_producto_view',0,'',NULL,'N;'),('action_proveedor_admin',0,'',NULL,'N;'),('action_proveedor_create',0,'',NULL,'N;'),('action_proveedor_delete',0,'',NULL,'N;'),('action_proveedor_index',0,'',NULL,'N;'),('action_proveedor_update',0,'',NULL,'N;'),('action_proveedor_view',0,'',NULL,'N;'),('action_stock_admin',0,'',NULL,'N;'),('action_stock_crearcable',0,'',NULL,'N;'),('action_stock_create',0,'',NULL,'N;'),('action_stock_index',0,'',NULL,'N;'),('action_ui_usermanagementadmin',0,'',NULL,'N;'),('admin',0,'',NULL,'N;');

/*Table structure for table `cruge_authitemchild` */

DROP TABLE IF EXISTS `cruge_authitemchild`;

CREATE TABLE `cruge_authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `crugeauthitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `cruge_authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `crugeauthitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `cruge_authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_authitemchild` */

/*Table structure for table `cruge_field` */

DROP TABLE IF EXISTS `cruge_field`;

CREATE TABLE `cruge_field` (
  `idfield` int(11) NOT NULL AUTO_INCREMENT,
  `fieldname` varchar(20) NOT NULL,
  `longname` varchar(50) DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  `required` int(11) DEFAULT '0',
  `fieldtype` int(11) DEFAULT '0',
  `fieldsize` int(11) DEFAULT '20',
  `maxlength` int(11) DEFAULT '45',
  `showinreports` int(11) DEFAULT '0',
  `useregexp` varchar(512) DEFAULT NULL,
  `useregexpmsg` varchar(512) DEFAULT NULL,
  `predetvalue` mediumblob,
  PRIMARY KEY (`idfield`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_field` */

/*Table structure for table `cruge_fieldvalue` */

DROP TABLE IF EXISTS `cruge_fieldvalue`;

CREATE TABLE `cruge_fieldvalue` (
  `idfieldvalue` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `idfield` int(11) NOT NULL,
  `value` blob,
  PRIMARY KEY (`idfieldvalue`),
  KEY `fk_cruge_fieldvalue_cruge_user1` (`iduser`),
  KEY `fk_cruge_fieldvalue_cruge_field1` (`idfield`),
  CONSTRAINT `fk_cruge_fieldvalue_cruge_field1` FOREIGN KEY (`idfield`) REFERENCES `cruge_field` (`idfield`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_cruge_fieldvalue_cruge_user1` FOREIGN KEY (`iduser`) REFERENCES `cruge_user` (`iduser`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_fieldvalue` */

/*Table structure for table `cruge_session` */

DROP TABLE IF EXISTS `cruge_session`;

CREATE TABLE `cruge_session` (
  `idsession` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `created` bigint(30) DEFAULT NULL,
  `expire` bigint(30) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `ipaddress` varchar(45) DEFAULT NULL,
  `usagecount` int(11) DEFAULT '0',
  `lastusage` bigint(30) DEFAULT NULL,
  `logoutdate` bigint(30) DEFAULT NULL,
  `ipaddressout` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idsession`),
  KEY `crugesession_iduser` (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

/*Data for the table `cruge_session` */

insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (1,1,1517261222,1517263022,0,'::1',1,1517261222,NULL,NULL),(2,1,1517311377,1517313177,1,'::1',2,1517311403,NULL,NULL),(3,1,1517602714,1517604514,0,'::1',1,1517602714,NULL,NULL),(4,1,1517605429,1517607229,0,'::1',2,1517605568,NULL,NULL),(5,1,1517841324,1517843124,0,'::1',1,1517841324,NULL,NULL),(6,1,1518033328,1518035128,1,'::1',1,1518033328,NULL,NULL),(7,1,1518751353,1518753153,1,'::1',1,1518751353,NULL,NULL),(8,1,1518753254,1518755054,1,'::1',3,1518753808,NULL,NULL),(9,1,1518819666,1518821466,0,'::1',1,1518819666,NULL,NULL),(10,1,1518821572,1518823372,1,'::1',1,1518821572,NULL,NULL),(11,1,1519776770,1519778570,0,'::1',2,1519777473,NULL,NULL),(12,1,1519779095,1519780895,0,'::1',2,1519779391,NULL,NULL),(13,1,1519781153,1519782953,1,'::1',1,1519781153,NULL,NULL),(14,1,1519790850,1519792650,0,'::1',2,1519792042,NULL,NULL),(15,1,1519794690,1519796490,1,'::1',1,1519794690,NULL,NULL),(16,1,1519942871,1519944671,0,'::1',1,1519942871,NULL,NULL),(17,1,1519949594,1519951394,1,'::1',1,1519949594,NULL,NULL),(18,1,1519957381,1519959181,1,'::1',1,1519957381,NULL,NULL),(19,1,1519991372,1519993172,0,'::1',1,1519991372,NULL,NULL),(20,1,1519993746,1519995546,1,'::1',1,1519993746,NULL,NULL),(21,1,1520024119,1520025919,0,'::1',1,1520024119,NULL,NULL),(22,1,1520026653,1520028453,0,'::1',1,1520026653,NULL,NULL),(23,1,1520028754,1520030554,0,'::1',3,1520029664,NULL,NULL),(24,1,1520035698,1520037498,0,'::1',1,1520035698,NULL,NULL),(25,1,1520093776,1520095576,0,'::1',2,1520093957,NULL,NULL),(26,1,1520104182,1520105982,1,'::1',1,1520104182,NULL,NULL),(27,1,1520198793,1520200593,0,'::1',1,1520198793,NULL,NULL),(28,1,1520208473,1520210273,1,'::1',2,1520208504,NULL,NULL),(29,1,1520257432,1520259232,0,'::1',1,1520257432,NULL,NULL),(30,1,1520260358,1520262158,0,'::1',1,1520260358,NULL,NULL),(31,1,1520279027,1520280827,0,'::1',2,1520280212,NULL,NULL),(32,1,1520288472,1520290272,1,'::1',1,1520288472,NULL,NULL);

/*Table structure for table `cruge_system` */

DROP TABLE IF EXISTS `cruge_system`;

CREATE TABLE `cruge_system` (
  `idsystem` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `largename` varchar(45) DEFAULT NULL,
  `sessionmaxdurationmins` int(11) DEFAULT '30',
  `sessionmaxsameipconnections` int(11) DEFAULT '10',
  `sessionreusesessions` int(11) DEFAULT '1' COMMENT '1yes 0no',
  `sessionmaxsessionsperday` int(11) DEFAULT '-1',
  `sessionmaxsessionsperuser` int(11) DEFAULT '-1',
  `systemnonewsessions` int(11) DEFAULT '0' COMMENT '1yes 0no',
  `systemdown` int(11) DEFAULT '0',
  `registerusingcaptcha` int(11) DEFAULT '0',
  `registerusingterms` int(11) DEFAULT '0',
  `terms` blob,
  `registerusingactivation` int(11) DEFAULT '1',
  `defaultroleforregistration` varchar(64) DEFAULT NULL,
  `registerusingtermslabel` varchar(100) DEFAULT NULL,
  `registrationonlogin` int(11) DEFAULT '1',
  PRIMARY KEY (`idsystem`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `cruge_system` */

insert  into `cruge_system`(`idsystem`,`name`,`largename`,`sessionmaxdurationmins`,`sessionmaxsameipconnections`,`sessionreusesessions`,`sessionmaxsessionsperday`,`sessionmaxsessionsperuser`,`systemnonewsessions`,`systemdown`,`registerusingcaptcha`,`registerusingterms`,`terms`,`registerusingactivation`,`defaultroleforregistration`,`registerusingtermslabel`,`registrationonlogin`) values (1,'default',NULL,30,10,1,-1,-1,0,0,0,0,'',0,'','',1);

/*Table structure for table `cruge_user` */

DROP TABLE IF EXISTS `cruge_user`;

CREATE TABLE `cruge_user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `regdate` bigint(30) DEFAULT NULL,
  `actdate` bigint(30) DEFAULT NULL,
  `logondate` bigint(30) DEFAULT NULL,
  `username` varchar(64) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL COMMENT 'Hashed password',
  `authkey` varchar(100) DEFAULT NULL COMMENT 'llave de autentificacion',
  `state` int(11) DEFAULT '0',
  `totalsessioncounter` int(11) DEFAULT '0',
  `currentsessioncounter` int(11) DEFAULT '0',
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `cruge_user` */

insert  into `cruge_user`(`iduser`,`regdate`,`actdate`,`logondate`,`username`,`email`,`password`,`authkey`,`state`,`totalsessioncounter`,`currentsessioncounter`) values (1,NULL,NULL,1520288472,'admin','admin@tucorreo.com','admin',NULL,1,0,0),(2,NULL,NULL,NULL,'invitado','invitado','nopassword',NULL,1,0,0);

/*Table structure for table `detalle` */

DROP TABLE IF EXISTS `detalle`;

CREATE TABLE `detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `id_materia_prima` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_producto` (`id_producto`),
  KEY `id_materia_prima` (`id_materia_prima`),
  CONSTRAINT `detalle_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detalle_ibfk_2` FOREIGN KEY (`id_materia_prima`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detalle` */

/*Table structure for table `imagen` */

DROP TABLE IF EXISTS `imagen`;

CREATE TABLE `imagen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pieza` int(11) NOT NULL,
  `path` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_materia_prima` (`id_pieza`),
  CONSTRAINT `imagen_ibfk_1` FOREIGN KEY (`id_pieza`) REFERENCES `componente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `imagen` */

insert  into `imagen`(`id`,`id_pieza`,`path`) values (10,10,'coonector2.jpg'),(12,11,'conector.jpg'),(13,12,'conector.jpg'),(14,13,'coonector2.jpg'),(15,13,'conector.jpg'),(16,14,'conector.jpg');

/*Table structure for table `materia_prima` */

DROP TABLE IF EXISTS `materia_prima`;

CREATE TABLE `materia_prima` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(50) NOT NULL,
  `Inactivo` tinyint(1) NOT NULL,
  `Caja_Sujecion` varchar(50) DEFAULT NULL,
  `Unidad_Medida` varchar(50) NOT NULL,
  `Ubicacion` varchar(50) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prove` (`id_proveedor`) USING BTREE,
  KEY `materiaprimatipo` (`id_tipo`),
  CONSTRAINT `materiaprimatipo` FOREIGN KEY (`id_tipo`) REFERENCES `materiaprimatipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `proveedor_fk` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

/*Data for the table `materia_prima` */

insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (13,'Primer Prueba',0,'no tiene','mm2','nose',4,NULL),(14,'asdasda Materia Prima',0,'asdasd','asda','DOSjj',3,NULL),(15,'testv',1,'2','asdas','DOS',3,NULL),(16,'test',0,'asdasd','1','asd',3,NULL),(17,'Cable nuevo  añdslkfj ñas',0,'no','mm2','DOS',3,NULL),(18,'nada',1,'dasd','asda','DOS',3,NULL),(19,'nadakk',0,'dasd','1','sdasdas',3,NULL),(20,'LÑ',1,'dasd','asdas','asd',3,NULL),(21,'LÑ',1,'dasd','asdas','asd',3,NULL),(22,'asdasda',0,'dasd','lkj','DOS',3,NULL),(23,'Nuevo tipo de conector',1,'Caja uno','unidad','Cajas ',3,NULL),(24,'test nuevo',0,'no tiene','ti','no',3,NULL),(25,' nuevo componente',1,'no tiene','unidad','Ubicacion',5,NULL),(26,'descripcion valida',0,'no','seccion','frente',3,NULL),(27,'test12',0,'no','seccion','DOS',5,NULL),(28,'nueva descripcion',0,'89','seccion','no tiene',5,NULL),(29,'Segunda prueba para probrar unidad de medidd',1,'no posee','Unidad','DOS',5,NULL),(30,'test para probar inactivo o no ',1,'no tiene','Seccion:mm2','Ubicado en caja de madera',5,NULL),(31,'Otro test',0,'no tiene','Seccion:mm2','en la caja azul',4,NULL),(32,'test1211',0,'dasd','mm2','DOS',5,NULL),(33,'test12',0,'no tiene','Unidad','DOS',4,NULL),(34,'Nuevo Producto',0,'Si','mm2','nose',5,NULL),(35,'nada',0,'no tiene','mm2','DOS',3,NULL),(36,'nada',0,'dasd','Unidad','DOS',3,NULL),(37,'test',0,'dasd','mm2','DOS',4,NULL),(38,'test12',0,'no tiene','mm2','DOS',3,NULL),(39,'test121',0,'aa','mm2','sdasdas',3,NULL),(40,'test12',0,'dasd','mm2','sdasdas',3,NULL),(41,'nada',0,'2','mm2','sdasdas',3,NULL);

/*Table structure for table `materiaprimatipo` */

DROP TABLE IF EXISTS `materiaprimatipo`;

CREATE TABLE `materiaprimatipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `materiaprimatipo` */

insert  into `materiaprimatipo`(`id`,`nombre`) values (1,'Cable'),(2,'Componente');

/*Table structure for table `producto` */

DROP TABLE IF EXISTS `producto`;

CREATE TABLE `producto` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `Diseno` varchar(100) NOT NULL,
  `FechaAlta` date NOT NULL,
  `FechaBaja` date NOT NULL,
  `Revision` varchar(30) NOT NULL,
  `Cliente` varchar(30) NOT NULL,
  `Observaciones` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `producto` */

insert  into `producto`(`id`,`Diseno`,`FechaAlta`,`FechaBaja`,`Revision`,`Cliente`,`Observaciones`) values (1,'Diseño Maverik','0000-00-00','0000-00-00','primera revision','Maverik','Setecel'),(2,'Diseño Maverik','0000-00-00','0000-00-00','CUALQUIER','Maverik','ÑALKFDSJ'),(3,'Diseño Maverik','2018-03-02','2018-03-15','CUALQUIER','Maverik','Ninguna'),(4,'Diseño Maverik','2018-03-02','2018-03-16','CUALQUIER','Maverikj','Ver.');

/*Table structure for table `proveedor` */

DROP TABLE IF EXISTS `proveedor`;

CREATE TABLE `proveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `dni_cuil_cuit` int(14) DEFAULT NULL,
  `codigo_postal` varchar(30) DEFAULT NULL,
  `direccion` varchar(100) NOT NULL,
  `localidad` varchar(50) DEFAULT NULL,
  `provincia` varchar(40) DEFAULT NULL,
  `barrio` varchar(30) DEFAULT NULL,
  `fecha_alta` date DEFAULT NULL,
  `fecha_baja` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `proveedor` */

insert  into `proveedor`(`id`,`nombre`,`telefono`,`email`,`dni_cuil_cuit`,`codigo_postal`,`direccion`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`) values (3,'Trielec','3456787','nestorestrada@gmail.com',33059373,'5425','av siempre viva 1298','sprinfield ','san juan','rawson','2017-09-23','0000-00-00'),(4,'Omar','687934','nestorestrada@gmail.com',23444567,'547','cordoba 89','rawson','san juan','obrero','2018-03-09','2018-03-22'),(5,'Cables Argentinos SA','687934','nestorestrada@gmail.com',23444567,'547','cordoba 89','rawson','san juan','obrero','2018-03-09','2018-03-22');

/*Table structure for table `stock` */

DROP TABLE IF EXISTS `stock`;

CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock` int(11) NOT NULL,
  `stock_min` int(11) NOT NULL,
  `stock_max` int(11) NOT NULL,
  `materia_prima_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `materia_prima_id_2` (`materia_prima_id`),
  KEY `materia_prima_id` (`materia_prima_id`),
  CONSTRAINT `stock_ibfk_1` FOREIGN KEY (`materia_prima_id`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

/*Data for the table `stock` */

insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (12,1000033,20,150,14),(14,1000033,20,150,15),(15,1200,123,5000,17),(16,1000033,20,150,22),(17,100003,123,150,25),(18,1000033,20,150,27),(19,15444,50,333,28),(20,1200,20,333,29),(21,1000033,11,5000,30),(22,1000033,123,5000,31),(23,1000033,123,333,32),(24,100003,20,150,33),(25,1000033,123,5000,34),(26,1000033,50,150,35),(27,1000033,123,150,36),(28,1000033,123,150,37),(29,1000033,20,150,38),(30,1000,20,5000,39),(31,1000033,11,5000,40);

/*Table structure for table `listadocables` */

DROP TABLE IF EXISTS `listadocables`;

/*!50001 DROP VIEW IF EXISTS `listadocables` */;
/*!50001 DROP TABLE IF EXISTS `listadocables` */;

/*!50001 CREATE TABLE  `listadocables`(
 `id` int(11) ,
 `tipo` varchar(5) ,
 `componente_id` int(11) ,
 `proveedor_id` int(11) ,
 `cod_setecel` varchar(30) ,
 `materia_prima_fk` int(11) ,
 `Descripcion` varchar(50) ,
 `Unidad_Medida` varchar(50) ,
 `stock` int(11) ,
 `nombre` varchar(100) 
)*/;

/*Table structure for table `listadocomponentes` */

DROP TABLE IF EXISTS `listadocomponentes`;

/*!50001 DROP VIEW IF EXISTS `listadocomponentes` */;
/*!50001 DROP TABLE IF EXISTS `listadocomponentes` */;

/*!50001 CREATE TABLE  `listadocomponentes`(
 `id` int(11) ,
 `tipo` varchar(10) ,
 `categoria` varchar(100) ,
 `componente_id` int(11) ,
 `proveedor_id` int(11) ,
 `cod_setecel` varchar(10) ,
 `materia_prima_fk` int(11) ,
 `Descripcion` varchar(50) ,
 `Unidad_Medida` varchar(50) ,
 `stock` int(11) ,
 `nombre` varchar(100) 
)*/;

/*View structure for view listadocables */

/*!50001 DROP TABLE IF EXISTS `listadocables` */;
/*!50001 DROP VIEW IF EXISTS `listadocables` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `listadocables` AS (select `mp`.`id` AS `id`,'cable' AS `tipo`,`c`.`id` AS `componente_id`,`p`.`id` AS `proveedor_id`,`c`.`cod_setecel` AS `cod_setecel`,`c`.`materia_prima_fk` AS `materia_prima_fk`,`mp`.`Descripcion` AS `Descripcion`,`mp`.`Unidad_Medida` AS `Unidad_Medida`,`s`.`stock` AS `stock`,`p`.`nombre` AS `nombre` from (((`cable` `c` join `materia_prima` `mp` on((`mp`.`id` = `c`.`materia_prima_fk`))) join `stock` `s` on((`s`.`materia_prima_id` = `mp`.`id`))) join `proveedor` `p` on((`p`.`id` = `mp`.`id_proveedor`)))) */;

/*View structure for view listadocomponentes */

/*!50001 DROP TABLE IF EXISTS `listadocomponentes` */;
/*!50001 DROP VIEW IF EXISTS `listadocomponentes` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `listadocomponentes` AS (select `mp`.`id` AS `id`,'componente' AS `tipo`,`ca`.`nombre` AS `categoria`,`c`.`id` AS `componente_id`,`p`.`id` AS `proveedor_id`,`c`.`cod_setecel` AS `cod_setecel`,`c`.`materia_prima_fk` AS `materia_prima_fk`,`mp`.`Descripcion` AS `Descripcion`,`mp`.`Unidad_Medida` AS `Unidad_Medida`,`s`.`stock` AS `stock`,`p`.`nombre` AS `nombre` from ((((`componente` `c` join `materia_prima` `mp` on((`mp`.`id` = `c`.`materia_prima_fk`))) join `stock` `s` on((`s`.`materia_prima_id` = `mp`.`id`))) join `proveedor` `p` on((`p`.`id` = `mp`.`id_proveedor`))) join `categoria` `ca` on((`ca`.`id` = `c`.`id_categoria`)))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
