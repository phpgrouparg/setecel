/*
SQLyog Ultimate v9.63 
MySQL - 5.5.5-10.1.29-MariaDB : Database - setecel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`setecel` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `setecel`;

/*Table structure for table `cable` */

DROP TABLE IF EXISTS `cable`;

CREATE TABLE `cable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_setecel` varchar(11) NOT NULL,
  `color_base` varchar(11) NOT NULL,
  `color_linea` varchar(11) NOT NULL,
  `seccion` float NOT NULL,
  `packaging` int(11) NOT NULL,
  `cod_proveedor` varchar(11) NOT NULL,
  `precio` float NOT NULL,
  `materia_prima_fk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `materia_prima` (`materia_prima_fk`),
  CONSTRAINT `materia_fk` FOREIGN KEY (`materia_prima_fk`) REFERENCES `materia_prima` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `categoria` */

DROP TABLE IF EXISTS `categoria`;

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Table structure for table `componente` */

DROP TABLE IF EXISTS `componente`;

CREATE TABLE `componente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_setecel` varchar(10) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `descripcion` varchar(10) NOT NULL,
  `terminal_asociado` int(11) NOT NULL,
  `precio` float NOT NULL,
  `cod_proveedor` varchar(10) NOT NULL,
  `packaging` int(11) NOT NULL,
  `cod_origen` varchar(10) NOT NULL,
  `materia_prima_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_categoria` (`id_categoria`) USING BTREE,
  KEY `componente_fk_mp` (`materia_prima_fk`),
  CONSTRAINT `componente_fk_mp` FOREIGN KEY (`materia_prima_fk`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `componente_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `detalle` */

DROP TABLE IF EXISTS `detalle`;

CREATE TABLE `detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `id_materia_prima` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_producto` (`id_producto`),
  KEY `id_materia_prima` (`id_materia_prima`),
  CONSTRAINT `detalle_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detalle_ibfk_2` FOREIGN KEY (`id_materia_prima`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `imagen` */

DROP TABLE IF EXISTS `imagen`;

CREATE TABLE `imagen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pieza` int(11) NOT NULL,
  `path` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_materia_prima` (`id_pieza`),
  CONSTRAINT `imagen_ibfk_1` FOREIGN KEY (`id_pieza`) REFERENCES `componente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `materia_prima` */

DROP TABLE IF EXISTS `materia_prima`;

CREATE TABLE `materia_prima` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(15) NOT NULL,
  `Inactivo` tinyint(1) NOT NULL,
  `Caja_Sujecion` varchar(15) NOT NULL,
  `Unidad_Medida` varchar(11) NOT NULL,
  `Ubicacion` varchar(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prove` (`id_proveedor`) USING BTREE,
  CONSTRAINT `proveedor_fk` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Table structure for table `producto` */

DROP TABLE IF EXISTS `producto`;

CREATE TABLE `producto` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `Diseno` varchar(100) NOT NULL,
  `FechaAlta` date NOT NULL,
  `FechaBaja` date NOT NULL,
  `Revision` varchar(30) NOT NULL,
  `Cliente` varchar(30) NOT NULL,
  `Observaciones` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Table structure for table `proveedor` */

DROP TABLE IF EXISTS `proveedor`;

CREATE TABLE `proveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `email` varchar(40) DEFAULT NULL,
  `dni_culil_cuit` int(14) DEFAULT NULL,
  `codigo_postal` varchar(10) DEFAULT NULL,
  `direccion` varchar(100) NOT NULL,
  `localidad` varchar(50) DEFAULT NULL,
  `provincia` varchar(40) DEFAULT NULL,
  `barrio` varchar(30) DEFAULT NULL,
  `fecha_alta` date DEFAULT NULL,
  `fecha_baja` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `stock` */

DROP TABLE IF EXISTS `stock`;

CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock` int(11) NOT NULL,
  `stock_min` int(11) NOT NULL,
  `stock_max` int(11) NOT NULL,
  `materia_prima_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `materia_prima_id_2` (`materia_prima_id`),
  KEY `materia_prima_id` (`materia_prima_id`),
  CONSTRAINT `stock_ibfk_1` FOREIGN KEY (`materia_prima_id`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `listadocables` */

DROP TABLE IF EXISTS `listadocables`;

/*!50001 DROP VIEW IF EXISTS `listadocables` */;
/*!50001 DROP TABLE IF EXISTS `listadocables` */;

/*!50001 CREATE TABLE  `listadocables`(
 `id` int(11) ,
 `tipo` varchar(5) ,
 `componente_id` int(11) ,
 `proveedor_id` int(11) ,
 `cod_setecel` varchar(11) ,
 `materia_prima_fk` int(11) ,
 `Descripcion` varchar(15) ,
 `Unidad_Medida` varchar(11) ,
 `stock` int(11) ,
 `nombre` varchar(100) 
)*/;

/*Table structure for table `listadocomponentes` */

DROP TABLE IF EXISTS `listadocomponentes`;

/*!50001 DROP VIEW IF EXISTS `listadocomponentes` */;
/*!50001 DROP TABLE IF EXISTS `listadocomponentes` */;

/*!50001 CREATE TABLE  `listadocomponentes`(
 `id` int(11) ,
 `tipo` varchar(10) ,
 `componente_id` int(11) ,
 `proveedor_id` int(11) ,
 `cod_setecel` varchar(10) ,
 `materia_prima_fk` int(11) ,
 `Descripcion` varchar(15) ,
 `Unidad_Medida` varchar(11) ,
 `stock` int(11) ,
 `nombre` varchar(100) 
)*/;

/*View structure for view listadocables */

/*!50001 DROP TABLE IF EXISTS `listadocables` */;
/*!50001 DROP VIEW IF EXISTS `listadocables` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `listadocables` AS (select `mp`.`id` AS `id`,'cable' AS `tipo`,`c`.`id` AS `componente_id`,`p`.`id` AS `proveedor_id`,`c`.`cod_setecel` AS `cod_setecel`,`c`.`materia_prima_fk` AS `materia_prima_fk`,`mp`.`Descripcion` AS `Descripcion`,`mp`.`Unidad_Medida` AS `Unidad_Medida`,`s`.`stock` AS `stock`,`p`.`nombre` AS `nombre` from (((`cable` `c` join `materia_prima` `mp` on((`mp`.`id` = `c`.`materia_prima_fk`))) join `stock` `s` on((`s`.`materia_prima_id` = `mp`.`id`))) join `proveedor` `p` on((`p`.`id` = `mp`.`id_proveedor`)))) */;

/*View structure for view listadocomponentes */

/*!50001 DROP TABLE IF EXISTS `listadocomponentes` */;
/*!50001 DROP VIEW IF EXISTS `listadocomponentes` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `listadocomponentes` AS (select `mp`.`id` AS `id`,'componente' AS `tipo`,`c`.`id` AS `componente_id`,`p`.`id` AS `proveedor_id`,`c`.`cod_setecel` AS `cod_setecel`,`c`.`materia_prima_fk` AS `materia_prima_fk`,`mp`.`Descripcion` AS `Descripcion`,`mp`.`Unidad_Medida` AS `Unidad_Medida`,`s`.`stock` AS `stock`,`p`.`nombre` AS `nombre` from (((`componente` `c` join `materia_prima` `mp` on((`mp`.`id` = `c`.`materia_prima_fk`))) join `stock` `s` on((`s`.`materia_prima_id` = `mp`.`id`))) join `proveedor` `p` on((`p`.`id` = `mp`.`id_proveedor`)))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
