/*
SQLyog Ultimate v9.63 
MySQL - 5.5.5-10.1.29-MariaDB : Database - setecel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `setecel`;

/*Table structure for table `cable` */

DROP TABLE IF EXISTS `cable`;

CREATE TABLE `cable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_setecel` varchar(30) DEFAULT NULL,
  `color_base` varchar(30) DEFAULT NULL,
  `color_linea` varchar(30) DEFAULT NULL,
  `seccion` float DEFAULT NULL,
  `packaging` int(11) DEFAULT NULL,
  `cod_proveedor` varchar(30) DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `materia_prima_fk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `materia_prima` (`materia_prima_fk`),
  CONSTRAINT `materia_fk` FOREIGN KEY (`materia_prima_fk`) REFERENCES `materia_prima` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=261 DEFAULT CHARSET=latin1;

/*Data for the table `cable` */

insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (1,'CA203000','Negro','',0.35,6000,'',1422.8,66);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (2,'CA203002','Negro','Rojo',0.35,6000,'',1422.8,67);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (3,'CA203004','Negro','Amarillo',0.35,6000,'',1422.8,68);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (4,'CA203009','Negro','Blanco',0.35,6000,'',1422.8,69);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (5,'CA203011','Marrón','',0.35,6000,'',1422.8,70);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (6,'CA203020','Rojo','Negro',0.35,6000,'',1422.8,71);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (7,'CA203022','Rojo','',0.35,6000,'',1422.8,72);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (8,'CA203024','Rojo','Amarillo',0.35,6000,'',1422.8,73);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (9,'CA203029','Rojo','Blanco',0.35,6000,'',1422.8,74);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (10,'CA203030','Naranja','Negro',0.35,6000,'',1422.8,75);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (11,'CA203033','Naranja','',0.35,6000,'',1422.8,76);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (12,'CA203040','Amarillo','Negro',0.35,6000,'',1422.8,77);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (13,'CA203042','Amarillo','Rojo',0.35,6000,'',1422.8,78);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (14,'CA203044','Amarillo','',0.35,6000,'',1422.8,79);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (15,'CA203045','Amarillo','Verde',0.35,6000,'',1422.8,80);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (16,'CA203049','Amarillo','Blanco',0.35,6000,'',1422.8,81);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (17,'CA203050','Verde','Negro',0.35,6000,'',1422.8,82);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (18,'CA203052','Verde','Rojo',0.35,6000,'',1422.8,83);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (19,'CA203054','Verde','Amarillo',0.35,6000,'',1422.8,84);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (20,'CA203055','Verde','',0.35,6000,'',1422.8,85);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (21,'CA203060','Azul','Negro',0.35,6000,'',1422.8,86);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (22,'CA203062','Azul','Rojo',0.35,6000,'',1422.8,87);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (23,'CA203066','Azul','',0.35,6000,'',1422.8,88);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (24,'CA203069','Azul','Blanco',0.35,6000,'',1422.8,89);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (25,'CA203077','Violeta','',0.35,6000,'',1422.8,90);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (26,'CA203079','Violeta','Blanco',0.35,6000,'',1422.8,91);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (27,'CA203080','Gris','Negro',0.35,6000,'',1422.8,92);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (28,'CA203087','Gris','Violeta',0.35,6000,'',1422.8,93);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (29,'CA203088','Gris','',0.35,6000,'',1422.8,94);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (30,'CA203090','Blanco','Negro',0.35,6000,'',1422.8,95);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (31,'CA203092','Blanco','Rojo',0.35,6000,'',1422.8,96);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (32,'CA203095','Blanco','Verde',0.35,6000,'',1422.8,97);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (33,'CA203099','Blanco','',0.35,6000,'',1422.8,98);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (34,'CA203122','Rosado','',0.35,6000,'',1422.8,99);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (35,'CA203155','Verde Claro','',0.35,6000,'',1422.8,100);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (36,'CA203166','Celeste','',0.35,6000,'',1422.8,101);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (37,'CA205000','Negro','',0.5,5000,'',1649.74,102);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (38,'CA205002','Negro','Rojo',0.5,5000,'WSK2005002',1649.74,103);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (39,'CA205004','Negro','Amarillo',0.5,5000,'WSK2005004',1649.74,104);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (40,'CA205006','Negro','Azul',0.5,5000,'',1649.74,105);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (41,'CA205009','Negro','Blanco',0.5,5000,'WSK2005009',1649.74,106);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (42,'CA205011','Marrón','',0.5,5000,'WSK2005011',1649.74,107);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (43,'CA205014','Marrón','Amarillo',0.5,5000,'',1649.74,108);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (44,'CA205019','Marrón','Blanco',0.5,5000,'',1649.74,109);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (45,'CA205020','Rojo','Negro',0.5,5000,'',1649.74,110);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (46,'CA205022','Rojo','',0.5,5000,'',1649.74,111);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (47,'CA205024','Rojo','Amarillo',0.5,5000,'WSK2005024',1649.74,112);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (48,'CA205029','Rojo','Blanco',0.5,5000,'',1649.74,113);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (49,'CA205030','Naranja','Negro',0.5,5000,'',1649.74,114);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (50,'CA205033','Naranja','',0.5,5000,'WSK2005033',1649.74,115);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (51,'CA205040','Amarillo','Negro',0.5,5000,'',1649.74,116);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (52,'CA205042','Amarillo','Rojo',0.5,5000,'WSK2005042',1649.74,117);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (53,'CA205044','Amarillo','',0.5,5000,'WSK2005044',1649.74,118);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (54,'CA205045','Amarillo','Verde',0.5,5000,'WSK2005045',1649.74,119);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (55,'CA205048','Amarillo','Gris',0.5,5000,'WSK2005045',1649.74,120);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (56,'CA205049','Amarillo','Blanco',0.5,5000,'WSK2005049',1649.74,121);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (57,'CA205050','Verde','Negro',0.5,5000,'WSK2005050',1649.74,122);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (58,'CA205052','Verde','Rojo',0.5,5000,'WSK2005052',1649.74,123);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (59,'CA205054','Verde','Amarillo',0.5,5000,'WSK2005054',1649.74,124);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (60,'CA205055','Verde','',0.5,5000,'WSK2005055',1649.74,125);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (61,'CA205056','Verde','Azul',0.5,5000,'',1649.74,126);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (62,'CA205059','Verde','Blanco',0.5,5000,'',1649.74,127);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (63,'CA205060','Azul','Negro',0.5,5000,'',1649.74,128);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (64,'CA205061','Azul','Marrón',0.5,5000,'',1649.74,129);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (65,'CA205062','Azul','Rojo',0.5,5000,'WSK2005062',1649.74,130);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (66,'CA205064','Azul','Amarillo',0.5,5000,'',1649.74,131);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (67,'CA205066','Azul','',0.5,5000,'',1649.74,132);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (68,'CA205069','Azul','Blanco',0.5,5000,'WSK2005069',1649.74,133);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (69,'CA205077','Violeta','',0.5,5000,'WSK2005049',1649.74,134);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (70,'CA205079','Violeta','Blanco',0.5,5000,'',1649.74,135);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (71,'CA205084','Gris','Amarillo',0.5,5000,'',1649.74,136);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (72,'CA205086','Gris','Azul',0.5,5000,'',1649.74,137);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (73,'CA205087','Gris','Violeta',0.5,5000,'',1649.74,138);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (74,'CA205088','Gris','',0.5,5000,'WSK2005088',1649.74,139);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (75,'CA205089','Gris','Blanco',0.5,5000,'WSK2005088',1649.74,140);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (76,'CA205090','Blanco','Negro',0.5,5000,'WSK2005090',1649.74,141);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (77,'CA205092','Blanco','Rojo',0.5,5000,'',1649.74,142);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (78,'CA205096','Blanco','Azul',0.5,5000,'',1649.74,143);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (79,'CA205099','Blanco','',0.5,5000,'WSK2005099',1649.74,144);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (80,'CA205122','Rosado','',0.5,5000,'WSK20050RR',1649.74,145);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (81,'CA205152','Verde Claro','Rojo',0.5,5000,'',1649.74,146);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (82,'CA205155','Verde Claro','',0.5,5000,'WSK20050VV',1649.74,147);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (83,'CA205160','Celeste','Negro',0.5,5000,'',1649.74,148);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (84,'CA205166','Celeste','',0.5,5000,'WSK20050CC',1649.74,149);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (85,'CA207000','Negro','',0.75,4000,'WSK2040055',2158.7,150);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (86,'CA207002','Negro','Rojo',0.75,4000,'',2158.7,151);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (87,'CA207009','Negro','Blanco',0.75,4000,'',2158.7,152);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (88,'CA207011','Marrón','',0.75,4000,'',2158.7,153);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (89,'CA207022','Rojo','',0.75,4000,'WSK2040055',2158.7,154);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (90,'CA207029','Rojo','Blanco',0.75,4000,'',2158.7,155);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (91,'CA207033','Naranja','',0.75,4000,'',2158.7,156);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (92,'CA207044','Amarillo','',0.75,4000,'WSK2040055',2158.7,157);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (93,'CA207052','Verde','Rojo',0.75,4000,'',2158.7,158);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (94,'CA207055','Verde','',0.75,4000,'WSK2040055',2158.7,159);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (95,'CA207059','Verde','Blanco',0.75,4000,'',2158.7,160);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (96,'CA207062','Azul','Rojo',0.75,4000,'',2158.7,161);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (97,'CA207064','Azul','Amarillo',0.75,4000,'',2158.7,162);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (98,'CA207066','Azul','',0.75,4000,'',2158.7,163);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (99,'CA207069','Azul','Blanco',0.75,4000,'',2158.7,164);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (100,'CA207082','Gris','Rojo',0.75,4000,'',2158.7,165);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (101,'CA207084','Gris','Amarillo',0.75,4000,'',2158.7,166);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (102,'CA207085','Gris','Verde',0.75,4000,'',2158.7,167);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (103,'CA207086','Gris','Azul',0.75,4000,'',2158.7,168);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (104,'CA207088','Gris','',0.75,4000,'',2158.7,169);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (105,'CA207099','Blanco','',0.75,4000,'',2158.7,170);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (106,'CA207122','Rosado','',0.75,4000,'',2158.7,171);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (107,'CA207166','Celeste','',0.75,4000,'',2158.7,172);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (108,'CA209000','Negro','',1,3000,'',2754.37,173);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (109,'CA209011','Marrón','',1,3000,'',2754.37,174);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (110,'CA209020','Rojo','Negro',1,3000,'',2754.37,175);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (111,'CA209022','Rojo','',1,3000,'',2754.37,176);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (112,'CA209026','Rojo','Azul',1,3000,'',2754.37,177);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (113,'CA209029','Rojo','Blanco',1,3000,'',2754.37,178);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (114,'CA209030','Naranja','Negro',1,3000,'',2754.37,179);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (115,'CA209033','Naranja','',1,3000,'',2754.37,180);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (116,'CA209042','Amarillo','Rojo',1,3000,'',2754.37,181);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (117,'CA209044','Amarillo','',1,3000,'',2754.37,182);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (118,'CA209050','Verde','Negro',1,3000,'',2754.37,183);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (119,'CA209055','Verde','',1,3000,'',2754.37,184);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (120,'CA209060','Azul','Negro',1,3000,'',2754.37,185);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (121,'CA209066','Azul','',1,3000,'',2754.37,186);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (122,'CA209069','Azul','Blanco',1,3000,'',2754.37,187);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (123,'CA209077','Violeta','',1,3000,'',2754.37,188);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (124,'CA209082','Gris','Rojo',1,3000,'',2754.37,189);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (125,'CA209090','Blanco','Negro',1,3000,'',2754.37,190);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (126,'CA209092','Blanco','Rojo',1,3000,'',2754.37,191);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (127,'CA209099','Blanco','',1,3000,'',2754.37,192);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (128,'CA209122','Rosado','',1,3000,'',2754.37,193);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (129,'CA209166','Celeste','',1,3000,'',2754.37,194);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (130,'CA215000','Negro','',1.5,2000,'',3867.5,195);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (131,'CA215009','Negro','Blanco',1.5,2000,'',3867.5,196);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (132,'CA215011','Marrón','',1.5,2000,'',3867.5,197);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (133,'CA215020','Rojo','Negro',1.5,2000,'',3867.5,198);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (134,'CA215022','Rojo','',1.5,2000,'',3867.5,199);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (135,'CA215029','Rojo','Blanco',1.5,2000,'',3867.5,200);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (136,'CA215034','Naranja','Amarillo',1.5,2000,'',3867.5,201);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (137,'CA215044','Amarillo','',1.5,2000,'',3867.5,202);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (138,'CA215055','Verde','',1.5,2000,'',3867.5,203);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (139,'CA215064','Azul','Amarillo',1.5,2000,'',3867.5,204);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (140,'CA215069','Azul','Blanco',1.5,2000,'',3867.5,205);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (141,'CA215099','Blanco','',1.5,2000,'',3867.5,206);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (142,'CA220000','Negro','',2,2000,'',0,207);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (143,'CA220022','Rojo','',2,2000,'',0,208);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (144,'CA225000','Negro','',2.5,1500,'',6399.72,209);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (145,'CA225022','Rojo','',2.5,1500,'',6399.72,210);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (146,'CA225029','Rojo','Blanco',2.5,1500,'',6399.72,211);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (147,'CA225055','Verde','',2.5,1500,'',6399.72,212);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (148,'CA240000','Negro','',4,1000,'WSK2040000',9993.41,213);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (149,'CA240022','Rojo','',4,1000,'WSK2040022',9993.41,214);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (150,'CA240029','Rojo','Blanco',4,1000,'WSK2040029',9993.41,215);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (151,'CA240055','Verde','',4,1000,'WSK2040055',9993.41,216);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (152,'CA260022','Rojo','',6,500,'',14659,217);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (153,'CA260029','Rojo','Blanco',6,500,'',14659,218);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (154,'CA260055','Verde','',6,500,'',14659,219);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (155,'CA520501','Negro y Azul','',2,2000,'',2600,220);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (156,'CE205000','Negro','',0.5,0,'',960.93,221);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (157,'CE205011','Marrón','',0.5,0,'',960.93,222);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (158,'CE205022','Rojo','',0.5,0,'',960.93,223);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (159,'CE205033','Naranja','',0.5,0,'',960.93,224);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (160,'CE205044','Amarillo','',0.5,0,'',960.93,225);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (161,'CE205054','Verde','Amarillo',0.5,0,'',960.93,226);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (162,'CE205055','Verde','',0.5,0,'',960.93,227);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (163,'CE205066','Azul','',0.5,0,'',960.93,228);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (164,'CE205077','Violeta','',0.5,0,'',960.93,229);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (165,'CE205088','Gris','',0.5,0,'',960.93,230);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (166,'CE205099','Blanco','',0.5,0,'',960.93,231);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (167,'CE205166','Celeste','',0.5,0,'',960.93,232);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (168,'CE207000','Negro','',0.75,0,'',0,233);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (169,'CE207011','Marrón','',0.75,0,'',0,234);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (170,'CE207022','Rojo','',0.75,0,'',0,235);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (171,'CE207033','Naranja','',0.75,0,'',0,236);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (172,'CE207044','Amarillo','',0.75,0,'',0,237);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (173,'CE207054','Verde','Amarillo',0.75,0,'',0,238);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (174,'CE207055','Verde','',0.75,0,'',0,239);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (175,'CE207066','Azul','',0.75,0,'',0,240);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (176,'CE207077','Violeta','',0.75,0,'',0,241);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (177,'CE207088','Gris','',0.75,0,'',0,242);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (178,'CE207099','Blanco','',0.75,0,'',0,243);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (179,'CE207166','Celeste','',0.75,0,'',0,244);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (180,'CG203000','Negro','',0.35,6000,'',960,245);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (181,'CG203044','Amarillo','',0.35,6000,'',960,246);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (182,'CG203099','Blanco','',0.35,6000,'',960,247);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (183,'CG205000','Negro','',0.5,5000,'',1300,248);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (184,'CG205002','Negro','Rojo',0.5,5000,'',1300,249);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (185,'CG205004','Negro','Amarillo',0.5,5000,'',1300,250);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (186,'CG205006','Negro','Azul',0.5,5000,'',1300,251);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (187,'CG205009','Negro','Blanco',0.5,5000,'',1300,252);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (188,'CG205011','Marrón','',0.5,5000,'',1300,253);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (189,'CG205014','Marrón','Amarillo',0.5,5000,'',1300,254);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (190,'CG205019','Marrón','Blanco',0.5,5000,'',1300,255);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (191,'CG205020','Rojo','Negro',0.5,5000,'',1300,256);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (192,'CG205022','Rojo','',0.5,5000,'',1300,257);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (193,'CG205024','Rojo','Amarillo',0.5,5000,'',1300,258);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (194,'CG205029','Rojo','Blanco',0.5,5000,'',1300,259);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (195,'CG205033','Naranja','',0.5,5000,'',1300,260);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (196,'CG205040','Amarillo','Negro',0.5,5000,'',1300,261);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (197,'CG205042','Amarillo','Rojo',0.5,5000,'',1300,262);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (198,'CG205044','Amarillo','',0.5,5000,'',1300,263);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (199,'CG205045','Amarillo','Verde',0.5,5000,'',1300,264);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (200,'CG205049','Amarillo','Blanco',0.5,5000,'',1300,265);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (201,'CG205050','Verde','Negro',0.5,5000,'',1300,266);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (202,'CG205052','Verde','Rojo',0.5,5000,'',1300,267);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (203,'CG205054','Verde','Amarillo',0.5,5000,'',1300,268);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (204,'CG205055','Verde','',0.5,5000,'',1300,269);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (205,'CG205056','Verde','Azul',0.5,5000,'',1300,270);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (206,'CG205059','Verde','Blanco',0.5,5000,'',1300,271);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (207,'CG205062','Azul','Rojo',0.5,5000,'',1300,272);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (208,'CG205064','Azul','Amarillo',0.5,5000,'',1300,273);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (209,'CG205066','Azul','',0.5,5000,'',1300,274);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (210,'CG205069','Azul','Blanco',0.5,5000,'',1300,275);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (211,'CG205077','Violeta','',0.5,5000,'',1300,276);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (212,'CG205088','Gris','',0.5,5000,'',1300,277);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (213,'CG205090','Blanco','Negro',0.5,5000,'',1300,278);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (214,'CG205092','Blanco','Rojo',0.5,5000,'',1300,279);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (215,'CG205096','Blanco','Azul',0.5,5000,'',1300,280);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (216,'CG205099','Blanco','',0.5,5000,'',1300,281);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (217,'CG205122','Rosado','',0.5,5000,'',1300,282);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (218,'CG205152','Verde Claro','Rojo',0.5,5000,'',1300,283);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (219,'CG205155','Verde Claro','',0.5,5000,'',1300,284);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (220,'CG205160','Celeste','Negro',0.5,5000,'',1300,285);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (221,'CG205166','Celeste','',0.5,5000,'',1300,286);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (222,'CG207000','Negro','',0.75,4000,'',1850,287);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (223,'CG207010','Marrón','Negro',0.75,4000,'',1850,288);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (224,'CG207012','Marrón','Rojo',0.75,4000,'',1850,289);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (225,'CG207016','Marrón','Azul',0.75,4000,'',1850,290);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (226,'CG207022','Rojo','',0.75,4000,'',1850,291);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (227,'CG207029','Rojo','Blanco',0.75,4000,'',1850,292);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (228,'CG207044','Amarillo','',0.75,4000,'',1850,293);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (229,'CG207050','Verde','Negro',0.75,4000,'',1850,294);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (230,'CG207052','Verde','Rojo',0.75,4000,'',1850,295);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (231,'CG207054','Verde','Amarillo',0.75,4000,'',1850,296);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (232,'CG207055','Verde','',0.75,4000,'',1850,297);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (233,'CG207056','Verde','Azul',0.75,4000,'',1850,298);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (234,'CG207066','Azul','',0.75,4000,'',1850,299);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (235,'CG207099','Blanco','',0.75,4000,'',1850,300);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (236,'CG207122','Rosado','',0.75,4000,'',1850,301);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (237,'CG209000','Negro','',1,3000,'',2400,302);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (238,'CG209002','Negro','Rojo',1,3000,'',2400,303);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (239,'CG209088','Gris','',1,3000,'',2400,304);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (240,'CG209099','Blanco','',1,3000,'',2400,305);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (241,'CG215000','Negro','',1.5,3000,'',2400,306);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (242,'CG215022','Rojo','',1.5,2000,'',3230,307);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (243,'CG215029','Rojo','Blanco',1.5,2000,'',3230,308);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (244,'CG215044','Amarillo','',1.5,2000,'',3230,309);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (245,'CG215055','Verde','',1.5,2000,'',3230,310);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (246,'CG215077','Violeta','',1.5,2000,'',3230,311);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (247,'CG215099','Blanco','',1.5,2000,'',3230,312);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (248,'CG225000','Negro','',2.5,1500,'',4750,313);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (249,'CG225011','Marrón','',2.5,1500,'',4750,314);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (250,'CG225022','Rojo','',2.5,1500,'',4750,315);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (251,'CG225029','Rojo','Blanco',2.5,1500,'',4750,316);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (252,'CG225055','Verde','',2.5,1500,'',4750,317);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (253,'CG240000','Negro','',4,1000,'',8000,318);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (254,'CG240022','Rojo','',4,1000,'',8000,319);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (255,'CG240029','Rojo','Blanco',4,1000,'',8000,320);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (256,'CG240055','Verde','',4,1000,'',8000,321);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (257,'CG260022','Rojo','',6,500,'',0,322);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (258,'CG260029','Rojo','Blanco',6,500,'',0,323);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (259,'CG260055','Verde','',6,500,'',0,324);
insert  into `cable`(`id`,`cod_setecel`,`color_base`,`color_linea`,`seccion`,`packaging`,`cod_proveedor`,`precio`,`materia_prima_fk`) values (260,'VT6X05','Verde','',0.5,0,'VT6X05',0,325);

/*Table structure for table `categoria` */

DROP TABLE IF EXISTS `categoria`;

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `categoria` */

insert  into `categoria`(`id`,`nombre`) values (12,'Capuchones');
insert  into `categoria`(`id`,`nombre`) values (13,'Cintas');
insert  into `categoria`(`id`,`nombre`) values (14,'Conectores');
insert  into `categoria`(`id`,`nombre`) values (15,'Easy Gun');
insert  into `categoria`(`id`,`nombre`) values (16,'Moldeados');
insert  into `categoria`(`id`,`nombre`) values (17,'Sellos ');
insert  into `categoria`(`id`,`nombre`) values (18,'Terminales');
insert  into `categoria`(`id`,`nombre`) values (19,'Termocontraibles');
insert  into `categoria`(`id`,`nombre`) values (20,'Tubos');
insert  into `categoria`(`id`,`nombre`) values (21,'Varios');

/*Table structure for table `cliente` */

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `telefono` varchar(60) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `dni_cuil_cuit` varchar(14) NOT NULL,
  `codigo_postal` varchar(50) DEFAULT NULL,
  `localidad` varchar(50) NOT NULL,
  `provincia` varchar(50) NOT NULL,
  `barrio` varchar(50) NOT NULL,
  `fecha_alta` date NOT NULL,
  `fecha_baja` date DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `rubro` varchar(50) DEFAULT NULL,
  `situacion_fiscal` varchar(50) DEFAULT NULL,
  `web` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `cliente` */

insert  into `cliente`(`id`,`nombre`,`telefono`,`direccion`,`dni_cuil_cuit`,`codigo_postal`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`,`email`,`rubro`,`situacion_fiscal`,`web`) values (1,'Pietcard Electrónica S.R.L.','(03404) 47 0192 ','Las Heras 1692','30649177577','3085','Pilar','Santa Fé','no informado','2018-01-02','2018-01-02','matias@pietcard.com.ar','Motopartista','Resp Inscripto','www.pietcard.com.ar');
insert  into `cliente`(`id`,`nombre`,`telefono`,`direccion`,`dni_cuil_cuit`,`codigo_postal`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`,`email`,`rubro`,`situacion_fiscal`,`web`) values (2,'Cega Electrónica S.A.','(0261) 405 0045          ','Cervantes 1317','30709491314','0','Godoy Cruz','Mendoza','no informado','2018-01-03','2018-01-03','echediack@cegaelectronica.com.ar','GNC','Resp Inscripto','www.cegaelectronica.com.ar');
insert  into `cliente`(`id`,`nombre`,`telefono`,`direccion`,`dni_cuil_cuit`,`codigo_postal`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`,`email`,`rubro`,`situacion_fiscal`,`web`) values (3,'Berta Electrónica S.R.L.','011) 4481 1888','26 de Abril 3475','30710367090','0','Ituzaingó','Buenos Aires','no informado','2018-01-04','2018-01-04','info@xenic.com.ar','GNC','Resp Inscripto','www.bertaelectronica.com');

/*Table structure for table `componente` */

DROP TABLE IF EXISTS `componente`;

CREATE TABLE `componente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_setecel` varchar(30) DEFAULT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `terminal_asociado` varchar(30) DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `cod_proveedor` varchar(50) DEFAULT NULL,
  `packaging` int(11) DEFAULT NULL,
  `cod_origen` varchar(30) DEFAULT NULL,
  `materia_prima_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_categoria` (`id_categoria`) USING BTREE,
  KEY `componente_fk_mp` (`materia_prima_fk`),
  CONSTRAINT `componente_cat` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`),
  CONSTRAINT `componente_fk_mp` FOREIGN KEY (`materia_prima_fk`) REFERENCES `materia_prima` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2341 DEFAULT CHARSET=latin1;

/*Data for the table `componente` */

insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2068,'281934-2',17,'282087-1',0.0144,'281934-2',0,'',1344);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2069,'282087-1',14,'282110-1',0,'282087-1',0,'',1345);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2070,'282110-1',18,'',0.0373,'282110-1',0,'',1346);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2071,'CAP10002',12,'',0.069,'153-CAP NE CHAS',1000,'',1347);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2072,'CAP10003',12,'',0.38,'153-CAP NE 2CON',1000,'',1348);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2073,'CAP10004',12,'',0.2,'0',1000,'',1349);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2074,'CAP10005',12,'',0.2,'0',1000,'',1350);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2075,'CAP10006',12,'',0.2,'0',1000,'',1351);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2076,'CAP10008',12,'',0,'-----',0,'',1352);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2077,'CAP10009',12,'',0,'-----',0,'',1353);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2078,'CAP10010',12,'',0.293104,'0',1000,'',1354);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2079,'CAP10011',12,'',0.55337,'0',500,'',1355);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2080,'CAP10012',12,'',0,'',0,'',1356);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2081,'CAP12001',12,'',0.069,'153-CAP ROJO +',1000,'',1357);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2082,'CAP1T007',12,'',0,'-----',0,'',1358);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2083,'CINA10',13,'',0.79,'CHTAC',18,'',1359);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2084,'CINA15',13,'',1.06,'CHTAC 16',20,'',1360);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2085,'CINA16',13,'',0,'-----',0,'',1361);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2086,'CINA25',13,'',0,'-----',0,'',1362);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2087,'CINA29',13,'',0,'0',0,'',1363);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2088,'CINA30',13,'',0.94,'16',20,'',1364);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2089,'CINA40',13,'',3.57,'113',18,'',1365);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2090,'CINET48',13,'',0,'0',0,'',1366);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2091,'CO100014',14,'TE108016',0.3497,'282189-1',0,'282189-1',1367);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2092,'CO100016',14,'TE108018',0.2371,'21-215',500,'',1368);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2093,'CO100017',14,'TE101029',0.13977,'21-216',500,'',1369);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2094,'CO100020',14,'TE108018',0.62,'Z0-0282193-1',500,'',1370);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2095,'CO100027',14,'TE108009',0.13,'Z0-0144545-0HR',500,'',1371);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2096,'CO100802',14,'TE108006',0.17,'ZO-0282080-1HR',500,'',1372);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2097,'CO100806',14,'TE108006',0.26,'0',500,'DJ7041Y-1.8-21',1373);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2098,'CO100808',14,'TE108006',0.35,'0',500,'',1374);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2099,'CO100809',14,'TE108007',0.32,'Z0-0282107-1HR',500,'',1375);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2100,'CO100810',14,'',0.3065,'282080-1',0,'282080-1',1376);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2101,'CO100812',14,'TE108002',3.5,'ZPP10001494',500,'538 - 64320 - 3311',1377);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2102,'CO100814',14,'TE108021',0.432,'211PL022S00492',500,'',1378);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2103,'CO100816',14,'TE108022',0.35,'1-1718806-1',500,'',1379);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2104,'CO100818',14,'TE108026',0.63,'Z1J0973704',500,'',1380);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2105,'CO100821',14,'TE108029 / TE108031',1.32,'Z9630210980',500,'',1381);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2106,'CO100822',14,'TE108028/TE108030',1.8,'ZPC24GR-7020',200,'',1382);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2107,'CO110020',14,'TE108018',0.62,'Z0-0282193-1',500,'',1383);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2108,'CO170019',14,'TE108025',0.4724,'1-967628-1',0,'',1384);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2109,'CO180012',14,'TE108015',0.4771,'1-967629-1',500,'1-967629-1',1385);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2110,'CO180022',14,'TE108018',0.28,'ZID404458HR',500,'DJ7022-3.5-21',1386);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2111,'CO180025',14,'TE108019',0.23,'ZID400770HR',500,'DJ7024-3.5-11',1387);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2112,'CO190000',14,'TE101000',0.03455,'ZML2BN',500,' DJ7021A-2.8-21',1388);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2113,'CO190001',14,'TE101001',0.0336,'ZML2AL',500,'DJ7021A-2.8-11',1389);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2114,'CO190002',14,'TE101000',0.03633,'ZML3BN',500,'DJ7031A-2.8-21',1390);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2115,'CO190003',14,'TE101001',0.0399,'ML3AL02N',500,'DJ7031A-2.8-11',1391);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2116,'CO190004',14,'TE101000',0.03749,'ZML4BN',500,'DJ7041A-2.8-21',1392);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2117,'CO190005',14,'TE101001',0.03749,'ZML4AL',500,'DJ7041A-2.8-11',1393);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2118,'CO190006',14,'TE101000',0.04232,'ZML6BN',500,'DJ7061A-2.8-20',1394);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2119,'CO190007',14,'TE101001',0.04232,'ZML6AL',500,'DJ7061A-2.8-10',1395);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2120,'CO190008',14,'TE101000',0.0496,'ZML9BN',400,'DJ7091A-2.8-20',1396);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2121,'CO190009',14,'TE101001',0.0496,'ZML9AL',400,'DJ7091A-2.8-10',1397);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2122,'CO190010',14,'TE101026',0.3005,'9-965382-2',10000,'',1398);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2123,'CO190202',14,'TE101002',0.0756,'ZRB4BLT',400,'DJ70413-6.3-21',1399);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2124,'CO190203',14,'TE101003',0.05796,'0',400,'',1400);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2125,'CO190204',14,'TE101002',0.0861,'ZCN6BSL',400,'DJ7068-6.3-21',1401);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2126,'CO190205',14,'TE101003',0.09345,'0',400,'DJ7062-6.3-10',1402);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2127,'CO190206',14,'TE101006',0,'0',1000,'',1403);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2128,'CO190207',14,'TE101003',0.04683,'ZRB3AL',400,'DJ7031A-6.3-11',1404);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2129,'CO190208',14,'TE101002',0.04463,'ZRB3BL',400,'DJ7031A-6.3-21',1405);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2130,'CO190210',14,'TE101006',0.0651,'ZDJ7041-6.3-21',400,'DJ7041-6.3-21',1406);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2131,'CO190211',14,'TE101003',0.074,'ZDJ7041-6.3-11',400,'DJ7041-6.3-11',1407);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2132,'CO190212',14,'TE101002',0.05796,'0',400,'',1408);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2133,'CO190213',14,'TE101003',0.05796,'0',400,'',1409);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2134,'CO190214',14,'TE101010',0.0441,'ZDJ70224-6.3-21',400,'DJ70224-6.3-21',1410);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2135,'CO190216',14,'TE101006',0.042,'ZDJ7048-6.3-21',400,'DJ7048-6.3-20',1411);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2136,'CO190217',14,'',0.05796,'0',400,'',1412);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2137,'CO190218',14,'TE101006',0.06825,'0',400,'DJ7021-6.3-21',1413);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2138,'CO190219',14,'TE101009',0.05796,'0',400,'',1414);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2139,'CO190220',14,'TE101002',0,'-----',0,'',1415);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2140,'CO190221',14,'TE101003',0,'-----',0,'',1416);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2141,'CO190222',14,'TE101002',0.09624,'0',1000,'',1417);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2142,'CO190223',14,'TE101003',0.09624,'0',1000,'',1418);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2143,'CO190224',14,'TE101002',0.042,'0',400,'DJ7048-6.3-21',1419);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2144,'CO190226',14,'',0,'-----',0,'DJ7011-6.3-21',1420);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2145,'CO190228',14,'TE101002',0,'-----',0,'DJ7022-6.3-21',1421);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2146,'CO190229',14,'TE101003',0,'-----',0,'DJ7022-6.3-11',1422);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2147,'CO190231',14,'TE101003',0.09345,'0',0,'DJ7061-6.3-11',1423);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2148,'CO190400',14,'TE101004',0.08526,'ZRL2BL',400,'DJ7022A-7.8-21',1424);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2149,'CO190401',14,'TE101005',0.08526,'ZRL2AL',400,'DJ7022A-7.8-11',1425);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2150,'CO193000',14,'TE103006',0.75,'Z12015798',0,'',1426);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2151,'CO194002',14,'TE108000',0.0633,'JS01104010',500,'',1427);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2152,'CO194004',14,'TE108000',0.0935,'0',50,'',1428);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2153,'CO194006',14,'TE108000',0.075,'ZDJ5557-8Y',500,' 0039012080',1429);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2154,'CO194008',14,'TE108000',0.1023,'JS01110010',500,'',1430);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2155,'CO194012',14,'TE108000',0.11,'ZDJ5557-14Y',500,'39012140',1431);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2156,'CO194020',14,'TE109000',0.03,'2510-3P',50,'770602-3',1432);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2157,'CO194022',14,'TE109000',0.054,'2318HJ-03',1000,'',1433);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2158,'CO194200',14,'TE108010',0.2941,'7129-6071',4000,'',1434);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2159,'CO194202',14,'TE108012',0.1415,'1670990-1',1000,'',1435);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2160,'CO194204',14,'TE108012',0.0934,'1-969489-4',5000,'',1436);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2161,'CO200208',14,'TE101002',0.00705,'11-03Ne',4000,'',1437);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2162,'CO200210',14,'TE201002',0.00822,'41-03Ne',4000,'',1438);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2163,'CO203000',14,'TE303002',0,'-----',0,'DJ3011B-4-21',1439);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2164,'CO210210',14,'TE201002',0.00822,'41-03Bl',4000,'',1440);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2165,'CO220210',14,'TE201002',0.00822,'41-03Bl',4000,'',1441);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2166,'CO240210',14,'TE201002',0.00822,'41-03Bl',4000,'',1442);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2167,'CO280210',14,'TE201002',0.00822,'41-03Bl',4000,'',1443);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2168,'CO290000',14,'TE101020',0.0307,'43-08NA',1000,'',1444);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2169,'CO290001',14,'TE101021',0.03843,'43-09NA',1000,'',1445);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2170,'CO290002',14,'TE101024',0.0932,'925015',3000,'',1446);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2171,'CO290200',14,'TE101002',0.05775,'ZML2B02N',400,'DJJ7021-6.3-21',1447);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2172,'CO290201',14,'TE101003',0,'0',1000,'',1448);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2173,'CO290202',14,'TE101006',0.05775,'ZML2B02N',400,'DJJ7021F-6.3-21',1449);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2174,'CO290204',14,'TE101002',0.0633,'2.42.614/80',500,'',1450);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2175,'CO290205',14,'TE101003',0.0595,'2.42.615/80',500,'',1451);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2176,'CO290206',14,'TE101002',0.13411,'2.42.623/80',250,'',1452);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2177,'CO290207',14,'TE101003',0.17953,'2.42.627/80',250,'',1453);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2178,'CO290210',14,'TE201002',0.00822,'41-03Bl',4000,'',1454);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2179,'CO290211',14,'TE201003',0.00972,'41-20Bl',4000,'',1455);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2180,'CO290212',14,'TE207000',0.01248,'41-32Bl',3000,'',1456);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2181,'CO290214',14,'TE207006',0.0133,'2.40.2381',4000,'',1457);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2182,'CO290216',14,'TE101002',0.0944,'2.42.621/80',250,'',1458);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2183,'CO291200',14,'TE101022',0.01011,'2.40.2340',500,'',1459);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2184,'CO291400',14,'TE201006',0,'',0,'',1460);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2185,'CO2N0214',14,'TE207006',0.0133,'2.40.2380',4000,'',1461);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2186,'CO300600',14,'TE101014',0,'0',0,'DJD7081-G2.3-21',1462);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2187,'FUS11001',21,'',0.13112,'FUS10501',2000,'',1463);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2188,'FUS20301',21,'',1.14,'F3',1000,'',1464);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2189,'FUS20501',21,'',1.14,'F5',1000,'',1465);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2190,'FUS20701',21,'',1.14,'F7',1000,'',1466);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2191,'FUS21001',21,'',1.14,'F10',1000,'',1467);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2192,'FUS21501',21,'',1.14,'F15',1000,'',1468);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2193,'FUS31001',21,'',0.05429,'0',1000,'',1469);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2194,'MOL10001',16,'',0.0124,'0',1000,'MDFNA 2-50',1470);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2195,'MOL10002',16,'',0.0054,'0',1000,'',1471);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2196,'MOL10003',16,'',0.0054,'0',1000,'',1472);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2197,'MOL10004',16,'',0.03,'0',1000,'',1473);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2198,'MOL10005',16,'',0,'-----',0,'',1474);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2199,'MOL1T002',16,'',0,'-----',0,'',1475);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2200,'MOL1T006',16,'TE303001',0.022,'Z85606302VL',1000,'',1476);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2201,'MOL1T007',16,'TE303000',0.022,'ZDJ3011B-4-21',1000,'',1477);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2202,'PFUS1901',21,'',0.1176,'ZFB003',1000,'3FB00302N',1478);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2203,'PFUS1902',21,'',0.08382,'10-07Na',1000,'',1479);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2204,'PFUS2001',21,'TE101012',0.08934,'42-21Ne',500,'',1480);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2205,'PFUS2002',21,'TE101012',0.04494,'42-23Ne',1000,'',1481);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2206,'PLU60001',21,'',2.1721,'1-773119-8',100,'',1482);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2207,'PRE90001',21,'',0.0071,'146-EL 1/80 ',10000,'',1483);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2208,'PRE00001',21,'',0,'',0,'',1484);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2209,'PRE90002',21,'',0.1529,'3-769/80',0,'',1485);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2210,'PRL50001',21,'TE101002',0.09548,'2.42.630/87',500,'',1486);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2211,'SE100014',17,'CO100014',0.0157,'828904-1',0,'828904-1',1487);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2212,'SE100800',17,'',0.03,'Z-0281934-2HR',10000,'',1488);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2213,'SE100818',17,'CO100818',0.028,'Z0-0963530-1',10000,'',1489);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2214,'SE100821',17,'',0.028,'0',0,'',1490);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2215,'SE101000',17,'',0.028,'Z15324976',10000,'',1491);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2216,'SE193000',17,'CO193000',0.096,'Z12015323',0,'',1492);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2217,'SE194200',17,'',0.05185,'7157-3621',120000,'',1493);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2218,'SE194201',17,'',0.136,'7157-3622',120000,'',1494);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2219,'SEN00000',21,'',1.97,'0',5000,'',1495);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2220,'TAP00812',21,'',0.3,'ZPP10001524',0,'',1496);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2221,'TE101000',18,'',0.0231,'ZTH028001',5000,'DJ621-E2.8×0.5',1497);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2222,'TE101001',18,'',0.026,'ZTM028001',10000,'DJ611-2.8×0.5',1498);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2223,'TE101002',18,'',0.02075,'11081',10000,'DJ621-E6.3',1499);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2224,'TE101003',18,'',0.02833,'22045',5000,'',1500);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2225,'TE101004',18,'',0.063,'604701BSS',2500,'DJ621-E7.8 x 0.8',1501);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2226,'TE101005',18,'',0.063,'6605701BS0',0,'DJ611-7.8E',1502);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2227,'TE101006',18,'',0.0273,'ZDJ621-B6.3',5000,'DJ621-B6.3',1503);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2228,'TE101007',18,'',0,'0',0,'',1504);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2229,'TE101009',18,'',0.01862,'0',10000,'',1505);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2230,'TE101010',18,'',0.03341,'11104',5000,'',1506);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2231,'TE101012',18,'',0.02533,'55387',10000,'',1507);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2232,'TE101014',18,'',0,'-----',0,'DJ621-E2.3A',1508);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2233,'TE101016',18,'',0.01982,'11082',8000,'',1509);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2234,'TE101017',18,'',0.02732,'22037',5000,'',1510);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2235,'TE101018',18,'',0.02695,'55375',8000,'',1511);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2236,'TE101020',18,'',0.01275,'27068',15000,'',1512);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2237,'TE101021',18,'',0.0324508,'45154/S',10000,'',1513);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2238,'TE101022',18,'',0.019,'1.01.0224C',10000,'',1514);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2239,'TE101024',18,'',0.0641,'150572',10000,'',1515);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2240,'TE101026',18,'',0.0206,'880634-1',30000,'',1516);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2241,'TE101029',18,'',0.0348208,'45154/E',10000,'',1517);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2242,'TE103006',18,'',0.096,'Z12124580',0,'',1518);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2243,'TE108000',18,'',0.01762,'82385/E',5000,'0039000047',1519);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2244,'TE108002',18,'',0.03,'Z6820002KSS',10000,'64322 - 1029',1520);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2245,'TE108004',18,'',0.037,'Z6820001KSS',10000,'',1521);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2246,'TE108006',18,'',0.018,'Z0-0282110-1',10000,'',1522);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2247,'TE108007',18,'',0.035,'Z0-0282109-1HR ',3000,'',1523);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2248,'TE108008',18,'',0.05,'Z0-0929939-3',10000,'',1524);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2249,'TE108009',18,'',0.027,'Z0-0142183-1HR',10000,'',1525);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2250,'TE108010',18,'',0.0884,'7115-1591',24000,'',1526);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2251,'TE108012',18,'',0.0251,'928999-1',13000,'',1527);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2252,'TE108015',18,'',0.0843,'1-962842-1',0,'1-962842-1',1528);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2253,'TE108016',18,'',0.0569,'927768-3',0,'927770-3',1529);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2254,'TE108018',18,'',0.0273585,'92435/E',5000,'',1530);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2255,'TE108019',18,'',0.05,'Z0-0962880-1',1000,'',1531);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2256,'TE108021',18,'',0.055,'211CL2S1160',0,'',1532);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2257,'TE108022',18,'',0.018,'1452674',0,'',1533);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2258,'TE108025',18,'',0.0843,'1-962842-1',0,'',1534);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2259,'TE108026',18,'',0.075,'Z0-0962876-1',4000,'',1535);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2260,'TE108028',18,'',0.06,'Z7703497722',5300,'',1536);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2261,'TE108029',18,'',0.1,'Z7703497343',5000,'',1537);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2262,'TE108030',18,'',0.08,'Z7703497725',2000,'',1538);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2263,'TE108031',18,'',0.12,'Z7703497703',5000,'',1539);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2264,'TE109000',18,'',0.017,'2318-TL',10000,'',1540);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2265,'TE201002',18,'CLIP 1',0.02078,'1.01.0424C',8000,'',1541);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2266,'TE201003',18,'',0.0206,'1.02.0786',7000,'',1542);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2267,'TE201004',18,'',0.0784,'5-160303-2',10000,'',1543);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2268,'TE201006',18,'',0,'',0,'',1544);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2269,'TE207000',18,'',0.0217,'15465',6000,'DJ627-D6.3A',1545);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2270,'TE207002',18,'',0.01862,'0',10000,'',1546);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2271,'TE207004',18,'',0,'-----',0,'DJ6211-D5.2',1547);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2272,'TE207006',18,'',0.0251,'1.01.0624',4000,'',1548);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2273,'TE210000',18,'',0.02618,'78368',10000,'',1549);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2274,'TE210001',18,'',0.02331,'38359',10000,'',1550);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2275,'TE219000',18,'',0,'-----',0,'',1551);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2276,'TE302000',18,'OJAL 1',0.02524,'25054/E',7000,'',1552);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2277,'TE302001',18,'OJAL 2',0.0404,'26065/E',3500,'',1553);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2278,'TE302002',18,'OJAL 3',0.08482,'26063/E',3500,'',1554);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2279,'TE302003',18,'',0,'0',0,'',1555);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2280,'TE302004',18,'',0.02075,'10300',5000,'',1556);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2281,'TE302005',18,'OJAL 4',0.01889,'1.06.1192',8000,'',1557);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2282,'TE302006',18,'OJAL 5',0,'',0,'',1558);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2283,'TE302007',18,'OJAL 6',0.02262,'25053/E',7000,'',1559);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2284,'TE303000',18,'CIL HEM 1',0.03623,'606013BS2',8000,'DJ221-3.5A',1560);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2285,'TE303001',18,'CIL MAC 1',0.03539,'606103BS2',8000,'DJ211-3.5A',1561);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2286,'TE303002',18,'',0,'-----',0,'DJ221-4A',1562);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2287,'TE303003',18,'',0,'-----',0,'DJ211-4A',1563);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2288,'TE303004',18,'',0,'-----',0,'',1564);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2289,'TE304000',18,'FUSE 1',0.03429,'37133',5000,'',1565);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2290,'TE305000',18,'',0.02575,'56211',10000,'',1566);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2291,'TE305001',18,'',0.02842,'56485',10000,'',1567);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2292,'TE306000',18,'',0.03675,'607202BS0',8000,'DJ222-3.5A',1568);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2293,'TE306002',18,'',0,'-----',0,'',1569);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2294,'TE306004',18,'',0,'-----',0,'DJ222-4A',1570);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2295,'TERM20120',19,'',0,'',0,'',1571);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2296,'TERMO1032',19,'',0.312,'0',200,'',1572);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2297,'TERMO1048',19,'',386,'0',100,'',1573);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2298,'TERMO1064',19,'',0.457,'0',100,'',1574);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2299,'TERMO1080',19,'',0.572,'0',100,'',1575);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2300,'TERMO1095',19,'',0.643,'0',100,'',1576);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2301,'TERMO1127',19,'',0.758,'0',50,'',1577);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2302,'TERMO1381',19,'',0,'-----',0,'',1578);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2303,'TRV60001',21,'CO194204',0.0866,'1-969490-4',5000,'',1579);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2304,'TRV70003',21,'CO170019',0.0831,'967633-1',0,'',1580);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2305,'TRV80000',21,'CO190010',0.1203,'965383-1',5000,'',1581);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2306,'TRV80002',21,'CO180012',0.0815,'967634-1',0,'967634-1',1582);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2307,'TUB10003',20,'',0.454,'0',100,'',1583);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2308,'TUB10004',20,'',0.55,'0',100,'',1584);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2309,'TUB10005',20,'',0.73,'0',100,'',1585);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2310,'TUB10006',20,'',1.045,'0',100,'',1586);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2311,'TUB10007',20,'',1.163,'0',50,'',1587);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2312,'TUB10008',20,'',1.321,'0',50,'',1588);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2313,'TUB10009',20,'',1.479,'0',50,'',1589);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2314,'TUB10010',20,'',1.881,'0',50,'',1590);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2315,'TUB10011',20,'',2.062,'0',50,'',1591);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2316,'TUB10012',20,'',2.534,'0',50,'',1592);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2317,'TUB10013',20,'',2.738,'0',50,'',1593);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2318,'TUB10014',20,'',2.941,'0',50,'',1594);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2319,'TUB10015',20,'',3.505,'0',50,'',1595);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2320,'TUB10016',20,'',3.732,'0',50,'',1596);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2321,'TUB10017',20,'',3.958,'0',50,'',1597);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2322,'TUB10018',20,'',4.174,'0',50,'',1598);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2323,'TUB10019',20,'',4.873,'0',50,'',1599);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2324,'TUB10020',20,'',5.59,'0',50,'',1600);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2325,'TUB10021',20,'',5.962,'0',50,'',1601);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2326,'TUB10022',20,'',6.698,'0',50,'',1602);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2327,'TUB10023',20,'',7.576,'0',50,'',1603);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2328,'TUB10024',20,'',7.906,'0',50,'',1604);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2329,'TUB10025',20,'',8.235,'0',50,'',1605);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2330,'TUB1T005',20,'',0.66,'M150 -06-08',100,'',1606);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2331,'TUB20007',20,'',0,'CO07',200,'',1607);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2332,'TUB20009',20,'',0,'CO09',100,'',1608);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2333,'TUB20013',20,'',0,'CO13',100,'',1609);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2334,'TUB39002',20,'',0.16,'0',1,'',1610);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2335,'TUB39003',20,'',0.23,'0',1,'',1611);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2336,'TUB39004',20,'',0.32,'0',0,'',1612);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2337,'TUB39006',20,'',0.51,'0',1,'',1613);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2338,'TUB39008',20,'',1.2,'0',1,'',1614);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2339,'TUB39010',20,'',1.95,'0',1,'',1615);
insert  into `componente`(`id`,`cod_setecel`,`id_categoria`,`terminal_asociado`,`precio`,`cod_proveedor`,`packaging`,`cod_origen`,`materia_prima_fk`) values (2340,'TUB40006',20,'',1.092,'0',100,'',1616);

/*Table structure for table `cruge_authassignment` */

DROP TABLE IF EXISTS `cruge_authassignment`;

CREATE TABLE `cruge_authassignment` (
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text,
  `itemname` varchar(64) NOT NULL,
  PRIMARY KEY (`userid`,`itemname`),
  KEY `fk_cruge_authassignment_cruge_authitem1` (`itemname`),
  KEY `fk_cruge_authassignment_user` (`userid`),
  CONSTRAINT `fk_cruge_authassignment_cruge_authitem1` FOREIGN KEY (`itemname`) REFERENCES `cruge_authitem` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cruge_authassignment_user` FOREIGN KEY (`userid`) REFERENCES `cruge_user` (`iduser`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_authassignment` */

/*Table structure for table `cruge_authitem` */

DROP TABLE IF EXISTS `cruge_authitem`;

CREATE TABLE `cruge_authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_authitem` */

insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_cable_admin',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_cable_create',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_cable_index',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_cable_update',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_cable_view',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_cliente_admin',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_cliente_create',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_cliente_delete',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_cliente_index',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_cliente_update',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_cliente_view',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_componente_admin',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_componente_create',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_componente_delete',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_componente_index',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_componente_update',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_componente_view',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_detalleParte_admin',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_detalleParte_create',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_detalleParte_index',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_imagen_admin',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_imagen_create',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_imagen_delete',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_imagen_index',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_imagen_update',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_imagen_view',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_listadocables_admin',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_listadocables_index',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_listadocomponentes_admin',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_listadocomponentes_index',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_listadocomponentes_view',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_materiaPrima_admin',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_materiaPrima_create',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_materiaPrima_index',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_materiaPrima_update',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_materiaPrima_view',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_orden_pedido',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_orden_presupuesto',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_producto_admin',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_producto_cantidades',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_producto_crear',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_producto_create',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_producto_delete',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_producto_index',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_producto_update',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_producto_view',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_proveedor_admin',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_proveedor_create',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_proveedor_delete',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_proveedor_index',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_proveedor_update',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_proveedor_view',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_stock_admin',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_stock_crearcable',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_stock_create',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_stock_index',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_stock_update',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_stock_view',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_ui_usermanagementadmin',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_viewDetalleparte_admin',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('action_viewDetalleparte_index',0,'',NULL,'N;');
insert  into `cruge_authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('admin',0,'',NULL,'N;');

/*Table structure for table `cruge_authitemchild` */

DROP TABLE IF EXISTS `cruge_authitemchild`;

CREATE TABLE `cruge_authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `crugeauthitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `cruge_authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `crugeauthitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `cruge_authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_authitemchild` */

/*Table structure for table `cruge_field` */

DROP TABLE IF EXISTS `cruge_field`;

CREATE TABLE `cruge_field` (
  `idfield` int(11) NOT NULL AUTO_INCREMENT,
  `fieldname` varchar(20) NOT NULL,
  `longname` varchar(50) DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  `required` int(11) DEFAULT '0',
  `fieldtype` int(11) DEFAULT '0',
  `fieldsize` int(11) DEFAULT '20',
  `maxlength` int(11) DEFAULT '45',
  `showinreports` int(11) DEFAULT '0',
  `useregexp` varchar(512) DEFAULT NULL,
  `useregexpmsg` varchar(512) DEFAULT NULL,
  `predetvalue` mediumblob,
  PRIMARY KEY (`idfield`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_field` */

/*Table structure for table `cruge_fieldvalue` */

DROP TABLE IF EXISTS `cruge_fieldvalue`;

CREATE TABLE `cruge_fieldvalue` (
  `idfieldvalue` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `idfield` int(11) NOT NULL,
  `value` blob,
  PRIMARY KEY (`idfieldvalue`),
  KEY `fk_cruge_fieldvalue_cruge_user1` (`iduser`),
  KEY `fk_cruge_fieldvalue_cruge_field1` (`idfield`),
  CONSTRAINT `fk_cruge_fieldvalue_cruge_field1` FOREIGN KEY (`idfield`) REFERENCES `cruge_field` (`idfield`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_cruge_fieldvalue_cruge_user1` FOREIGN KEY (`iduser`) REFERENCES `cruge_user` (`iduser`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cruge_fieldvalue` */

/*Table structure for table `cruge_session` */

DROP TABLE IF EXISTS `cruge_session`;

CREATE TABLE `cruge_session` (
  `idsession` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `created` bigint(30) DEFAULT NULL,
  `expire` bigint(30) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `ipaddress` varchar(45) DEFAULT NULL,
  `usagecount` int(11) DEFAULT '0',
  `lastusage` bigint(30) DEFAULT NULL,
  `logoutdate` bigint(30) DEFAULT NULL,
  `ipaddressout` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idsession`),
  KEY `crugesession_iduser` (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=latin1;

/*Data for the table `cruge_session` */

insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (1,1,1517261222,1517263022,0,'::1',1,1517261222,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (2,1,1517311377,1517313177,1,'::1',2,1517311403,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (3,1,1517602714,1517604514,0,'::1',1,1517602714,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (4,1,1517605429,1517607229,0,'::1',2,1517605568,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (5,1,1517841324,1517843124,0,'::1',1,1517841324,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (6,1,1518033328,1518035128,1,'::1',1,1518033328,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (7,1,1518751353,1518753153,1,'::1',1,1518751353,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (8,1,1518753254,1518755054,1,'::1',3,1518753808,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (9,1,1518819666,1518821466,0,'::1',1,1518819666,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (10,1,1518821572,1518823372,1,'::1',1,1518821572,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (11,1,1519776770,1519778570,0,'::1',2,1519777473,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (12,1,1519779095,1519780895,0,'::1',2,1519779391,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (13,1,1519781153,1519782953,1,'::1',1,1519781153,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (14,1,1519790850,1519792650,0,'::1',2,1519792042,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (15,1,1519794690,1519796490,1,'::1',1,1519794690,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (16,1,1519942871,1519944671,0,'::1',1,1519942871,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (17,1,1519949594,1519951394,1,'::1',1,1519949594,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (18,1,1519957381,1519959181,1,'::1',1,1519957381,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (19,1,1519991372,1519993172,0,'::1',1,1519991372,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (20,1,1519993746,1519995546,1,'::1',1,1519993746,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (21,1,1520024119,1520025919,0,'::1',1,1520024119,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (22,1,1520026653,1520028453,0,'::1',1,1520026653,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (23,1,1520028754,1520030554,0,'::1',3,1520029664,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (24,1,1520035698,1520037498,0,'::1',1,1520035698,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (25,1,1520093776,1520095576,0,'::1',2,1520093957,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (26,1,1520104182,1520105982,1,'::1',1,1520104182,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (27,1,1520198793,1520200593,0,'::1',1,1520198793,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (28,1,1520208473,1520210273,1,'::1',2,1520208504,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (29,1,1520257432,1520259232,0,'::1',1,1520257432,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (30,1,1520260358,1520262158,0,'::1',1,1520260358,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (31,1,1520279027,1520280827,0,'::1',2,1520280212,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (32,1,1520288472,1520290272,0,'::1',1,1520288472,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (33,1,1520291562,1520293362,0,'::1',1,1520291562,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (34,1,1520302553,1520304353,0,'::1',3,1520302640,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (35,1,1520387604,1520389404,0,'::1',1,1520387604,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (36,1,1520392761,1520394561,0,'::1',3,1520393154,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (37,1,1520396578,1520398378,0,'::1',1,1520396578,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (38,1,1520399109,1520400909,0,'::1',1,1520399109,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (39,1,1520425344,1520427144,0,'::1',2,1520426373,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (40,1,1520447472,1520449272,0,'::1',1,1520447472,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (41,1,1520451295,1520453095,0,'::1',3,1520452353,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (42,1,1520453979,1520455779,0,'::1',3,1520454427,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (43,1,1520475880,1520477680,0,'::1',1,1520475880,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (44,1,1520479777,1520481577,0,'::1',1,1520479777,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (45,1,1520530487,1520532287,0,'::1',1,1520530487,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (46,1,1520535093,1520536893,0,'::1',2,1520535637,1520535653,'::1');
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (47,1,1520535780,1520537580,0,'::1',1,1520535780,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (48,1,1520538512,1520540312,0,'::1',1,1520538512,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (49,1,1520562384,1520564184,0,'::1',3,1520563778,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (50,1,1520564294,1520566094,1,'::1',4,1520566034,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (51,1,1520571833,1520573633,0,'::1',1,1520571833,1520571951,'::1');
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (52,1,1520571955,1520573755,1,'::1',1,1520571955,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (53,1,1520605689,1520607489,0,'::1',1,1520605689,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (54,1,1520626918,1520628718,0,'::1',1,1520626918,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (55,1,1520628842,1520630642,0,'::1',1,1520628842,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (56,1,1520631185,1520632985,0,'::1',1,1520631185,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (57,1,1520633248,1520635048,1,'::1',1,1520633248,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (58,1,1521212341,1521214141,0,'::1',1,1521212341,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (59,1,1521219574,1521221374,0,'::1',1,1521219574,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (60,1,1521229947,1521231747,1,'::1',4,1521231298,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (61,1,1521398901,1521400701,0,'::1',1,1521398901,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (62,1,1521401198,1521402998,0,'::1',1,1521401198,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (63,1,1521404824,1521406624,1,'::1',1,1521404824,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (64,1,1521766070,1521767870,1,'::1',1,1521766070,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (65,1,1522572738,1522574538,1,'::1',1,1522572738,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (66,1,1523306237,1523308037,0,'::1',1,1523306237,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (67,1,1523311629,1523313429,0,'::1',1,1523311629,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (68,1,1523570362,1523572162,1,'::1',1,1523570362,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (69,1,1524171074,1524172874,1,'::1',2,1524171143,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (70,1,1524193295,1524195095,0,'::1',1,1524193295,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (71,1,1524196983,1524198783,0,'::1',1,1524196983,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (72,1,1524199201,1524201001,0,'::1',1,1524199201,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (73,1,1524201216,1524203016,1,'::1',1,1524201216,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (74,1,1525201829,1525203629,0,'::1',2,1525201830,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (75,1,1525215673,1525217473,1,'::1',1,1525215673,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (76,1,1525387669,1525389469,1,'::1',2,1525387670,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (77,1,1525701978,1525703778,0,'::1',1,1525701978,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (78,1,1525745406,1525747206,0,'::1',1,1525745406,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (79,1,1526065369,1526067169,0,'::1',1,1526065369,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (80,1,1526357211,1526359011,0,'::1',1,1526357211,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (81,1,1526362720,1526364520,0,'::1',1,1526362720,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (82,1,1526409104,1526410904,0,'::1',1,1526409104,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (83,1,1526426676,1526428476,0,'::1',2,1526426705,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (84,1,1526428569,1526430369,0,'127.0.0.1',2,1526430259,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (85,1,1526430472,1526432272,0,'127.0.0.1',1,1526430472,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (86,1,1527713817,1527715617,1,'127.0.0.1',1,1527713817,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (87,1,1527788704,1527790504,1,'127.0.0.1',1,1527788704,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (88,1,1527796021,1527797821,0,'127.0.0.1',1,1527796021,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (89,1,1528138313,1528140113,0,'127.0.0.1',1,1528138313,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (90,1,1528148263,1528150063,0,'127.0.0.1',1,1528148263,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (91,1,1528150607,1528152407,0,'127.0.0.1',1,1528150607,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (92,1,1528171696,1528173496,1,'127.0.0.1',1,1528171696,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (93,1,1528231821,1528233621,0,'::1',1,1528231821,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (94,1,1528234910,1528236710,0,'::1',2,1528235240,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (95,1,1528724843,1528726643,1,'::1',1,1528724843,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (96,1,1531024843,1531026643,1,'::1',1,1531024843,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (97,1,1531088855,1531090655,0,'::1',1,1531088855,NULL,NULL);
insert  into `cruge_session`(`idsession`,`iduser`,`created`,`expire`,`status`,`ipaddress`,`usagecount`,`lastusage`,`logoutdate`,`ipaddressout`) values (98,1,1539457916,1539459716,1,'::1',1,1539457916,NULL,NULL);

/*Table structure for table `cruge_system` */

DROP TABLE IF EXISTS `cruge_system`;

CREATE TABLE `cruge_system` (
  `idsystem` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `largename` varchar(45) DEFAULT NULL,
  `sessionmaxdurationmins` int(11) DEFAULT '30',
  `sessionmaxsameipconnections` int(11) DEFAULT '10',
  `sessionreusesessions` int(11) DEFAULT '1' COMMENT '1yes 0no',
  `sessionmaxsessionsperday` int(11) DEFAULT '-1',
  `sessionmaxsessionsperuser` int(11) DEFAULT '-1',
  `systemnonewsessions` int(11) DEFAULT '0' COMMENT '1yes 0no',
  `systemdown` int(11) DEFAULT '0',
  `registerusingcaptcha` int(11) DEFAULT '0',
  `registerusingterms` int(11) DEFAULT '0',
  `terms` blob,
  `registerusingactivation` int(11) DEFAULT '1',
  `defaultroleforregistration` varchar(64) DEFAULT NULL,
  `registerusingtermslabel` varchar(100) DEFAULT NULL,
  `registrationonlogin` int(11) DEFAULT '1',
  PRIMARY KEY (`idsystem`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `cruge_system` */

insert  into `cruge_system`(`idsystem`,`name`,`largename`,`sessionmaxdurationmins`,`sessionmaxsameipconnections`,`sessionreusesessions`,`sessionmaxsessionsperday`,`sessionmaxsessionsperuser`,`systemnonewsessions`,`systemdown`,`registerusingcaptcha`,`registerusingterms`,`terms`,`registerusingactivation`,`defaultroleforregistration`,`registerusingtermslabel`,`registrationonlogin`) values (1,'default',NULL,30,10,1,-1,-1,0,0,0,0,'',0,'','',1);

/*Table structure for table `cruge_user` */

DROP TABLE IF EXISTS `cruge_user`;

CREATE TABLE `cruge_user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `regdate` bigint(30) DEFAULT NULL,
  `actdate` bigint(30) DEFAULT NULL,
  `logondate` bigint(30) DEFAULT NULL,
  `username` varchar(64) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL COMMENT 'Hashed password',
  `authkey` varchar(100) DEFAULT NULL COMMENT 'llave de autentificacion',
  `state` int(11) DEFAULT '0',
  `totalsessioncounter` int(11) DEFAULT '0',
  `currentsessioncounter` int(11) DEFAULT '0',
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `cruge_user` */

insert  into `cruge_user`(`iduser`,`regdate`,`actdate`,`logondate`,`username`,`email`,`password`,`authkey`,`state`,`totalsessioncounter`,`currentsessioncounter`) values (1,NULL,NULL,1539457917,'admin','admin@tucorreo.com','admin',NULL,1,0,0);
insert  into `cruge_user`(`iduser`,`regdate`,`actdate`,`logondate`,`username`,`email`,`password`,`authkey`,`state`,`totalsessioncounter`,`currentsessioncounter`) values (2,NULL,NULL,NULL,'invitado','invitado','nopassword',NULL,1,0,0);

/*Table structure for table `detalle` */

DROP TABLE IF EXISTS `detalle`;

CREATE TABLE `detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `id_materia_prima` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_producto` (`id_producto`),
  KEY `id_materia_prima` (`id_materia_prima`),
  CONSTRAINT `detalle_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detalle_ibfk_2` FOREIGN KEY (`id_materia_prima`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detalle` */

/*Table structure for table `detalle_parte` */

DROP TABLE IF EXISTS `detalle_parte`;

CREATE TABLE `detalle_parte` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `materia_prima_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  `numero_revision` int(11) DEFAULT NULL,
  `cantidad_cable` int(11) DEFAULT NULL,
  `cantidad_comp` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `materia_prima_fk` (`materia_prima_id`),
  KEY `producto_fk` (`producto_id`),
  CONSTRAINT `materia_prima_fk` FOREIGN KEY (`materia_prima_id`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `producto_fk` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `detalle_parte` */

insert  into `detalle_parte`(`id`,`materia_prima_id`,`producto_id`,`numero_revision`,`cantidad_cable`,`cantidad_comp`) values (1,66,1,1,12,NULL);
insert  into `detalle_parte`(`id`,`materia_prima_id`,`producto_id`,`numero_revision`,`cantidad_cable`,`cantidad_comp`) values (2,68,1,1,2,NULL);
insert  into `detalle_parte`(`id`,`materia_prima_id`,`producto_id`,`numero_revision`,`cantidad_cable`,`cantidad_comp`) values (3,73,1,1,12,NULL);
insert  into `detalle_parte`(`id`,`materia_prima_id`,`producto_id`,`numero_revision`,`cantidad_cable`,`cantidad_comp`) values (4,1616,1,1,NULL,12);
insert  into `detalle_parte`(`id`,`materia_prima_id`,`producto_id`,`numero_revision`,`cantidad_cable`,`cantidad_comp`) values (5,69,2,1,23,NULL);
insert  into `detalle_parte`(`id`,`materia_prima_id`,`producto_id`,`numero_revision`,`cantidad_cable`,`cantidad_comp`) values (6,66,3,1,89,NULL);
insert  into `detalle_parte`(`id`,`materia_prima_id`,`producto_id`,`numero_revision`,`cantidad_cable`,`cantidad_comp`) values (7,236,3,1,100,NULL);
insert  into `detalle_parte`(`id`,`materia_prima_id`,`producto_id`,`numero_revision`,`cantidad_cable`,`cantidad_comp`) values (8,1614,3,1,NULL,12);

/*Table structure for table `imagen` */

DROP TABLE IF EXISTS `imagen`;

CREATE TABLE `imagen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pieza` int(11) NOT NULL,
  `path` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_materia_prima` (`id_pieza`),
  CONSTRAINT `imagen_ibfk_1` FOREIGN KEY (`id_pieza`) REFERENCES `componente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `imagen` */

/*Table structure for table `materia_prima` */

DROP TABLE IF EXISTS `materia_prima`;

CREATE TABLE `materia_prima` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(255) NOT NULL,
  `Inactivo` tinyint(1) NOT NULL,
  `Caja_Sujecion` varchar(50) DEFAULT NULL,
  `Unidad_Medida` varchar(50) DEFAULT NULL,
  `Ubicacion` varchar(50) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prove` (`id_proveedor`) USING BTREE,
  KEY `materiaprimatipo` (`id_tipo`),
  CONSTRAINT `materiaprimatipo` FOREIGN KEY (`id_tipo`) REFERENCES `materiaprimatipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `proveedor_fk` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1617 DEFAULT CHARSET=latin1;

/*Data for the table `materia_prima` */

insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (66,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (67,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (68,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (69,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (70,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (71,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (72,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (73,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (74,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (75,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (76,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (77,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (78,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (79,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (80,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (81,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (82,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (83,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (84,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (85,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (86,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (87,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (88,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (89,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (90,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (91,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (92,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (93,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (94,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (95,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (96,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (97,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (98,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (99,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (100,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (101,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (102,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (103,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (104,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (105,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (106,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (107,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (108,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (109,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (110,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (111,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (112,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (113,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (114,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (115,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (116,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (117,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (118,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (119,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (120,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (121,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (122,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (123,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (124,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (125,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (126,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (127,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (128,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (129,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (130,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (131,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (132,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (133,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (134,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (135,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (136,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (137,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (138,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (139,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (140,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (141,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (142,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (143,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (144,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (145,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (146,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (147,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (148,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (149,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (150,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (151,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (152,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (153,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (154,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (155,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (156,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (157,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (158,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (159,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (160,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (161,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (162,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (163,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (164,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (165,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (166,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (167,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (168,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (169,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (170,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (171,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (172,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (173,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (174,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (175,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (176,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (177,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (178,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (179,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (180,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (181,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (182,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (183,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (184,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (185,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (186,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (187,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (188,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (189,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (190,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (191,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (192,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (193,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (194,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (195,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (196,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (197,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (198,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (199,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (200,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (201,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (202,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (203,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (204,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (205,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (206,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (207,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (208,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (209,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (210,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (211,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (212,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (213,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (214,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (215,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (216,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (217,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (218,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (219,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (220,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (221,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (222,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (223,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (224,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (225,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (226,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (227,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (228,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (229,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (230,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (231,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (232,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',4,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (233,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',5,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (234,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',5,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (235,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',5,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (236,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',5,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (237,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',5,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (238,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',5,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (239,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',5,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (240,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',5,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (241,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',5,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (242,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',5,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (243,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',5,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (244,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',5,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (245,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (246,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (247,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (248,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (249,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (250,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (251,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (252,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (253,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (254,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (255,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (256,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (257,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (258,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (259,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (260,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (261,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (262,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (263,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (264,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (265,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (266,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (267,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (268,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (269,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (270,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (271,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (272,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (273,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (274,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (275,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (276,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (277,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (278,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (279,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (280,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (281,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (282,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (283,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (284,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (285,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (286,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (287,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (288,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (289,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (290,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (291,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (292,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (293,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (294,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (295,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (296,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (297,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (298,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (299,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (300,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (301,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (302,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (303,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (304,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (305,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (306,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (307,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (308,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (309,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (310,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (311,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (312,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (313,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (314,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (315,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (316,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (317,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (318,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (319,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (320,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (321,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (322,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (323,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (324,'Sin Descripcion',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',6,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (325,'Vaina a tubo 6 x 0,50',0,'Sin Caja de sujecion','mm2','Sin Ubicacion',5,1);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1344,'Sello Tyco para conector Superseal',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1345,'Conector Tyco Hembra 3 vias color Negro, Línea Superseal,  Terminal 1,5 mm 060',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1346,'Terminal Mini Mic 1,5 mm, Tyco para conector Superseal',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1347,'capuchon PVC color Negro Recto inscripcion \"-\" Chasis',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1348,'capuchon PVC color Negro F Mayor 48 mm, F Menor 20 mm, para alojar 2 Conectores',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1349,'capuchon PVC color Negro Boca cuadrada 21 x 19 mm opuesto tubo F 8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1350,'capuchon PVC color Negro Boca cuadrada 34 x 21 mm opuesto tubo F 9 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1351,'capuchon PVC color Negro F Mayor 30 mm, F Menor 20 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1352,'capuchon PVC color Negro Boca cuadrada 23 x 18 mm salida de F 9 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1353,'capuchon PVC color Negro F Mayor 65 mm, F Menor 28 mm, para alojar Varios Conectores',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1354,'Capuchón PVC color Negro Boca rectangular 51 x 18 mm salida de F 11 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1355,'Capuchón PVC color Negro Boca rectangular 44 x 26 mm salida de F 20 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1356,'Capuchón PVC color Negro Boca rectangular 49 x 18 mm salida de F 10 mm, altura 40 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1357,'capuchon PVC color Rojo Recto inscripcion \"+\" Positivo',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1358,'capuchon PVC Transparente, Boca cuadrada 28 x 23 mm opuesto tubo F 9 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1359,'Cinta de PVC con adhesivo color Negro de 19 mm x 20 mts, TACSA Extra 10',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1360,'Cinta de PVC con adhesivo color Verde de 19 mm x 20 mts',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1361,'Cinta de PVC con adhesivo color Azul de 19 mm x 20 mts',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',13,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1362,'Cinta de Papel color Blanco 12mm x 50m',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1363,'Cinta de Papel color Blanco 48mm x 50m',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1364,'Cinta de PVC con adhesivo color Negro de 19 mm x 20 mts, TACSA 15 Plus',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1365,'Cinta de Tela con adhesivo color Negro de 19 mm x 18 mts',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1366,'Cinta de Empaque Transparente 48 mm x 100 mta',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1367,'Conector Tyco Automotriz Hembra 2 vías color Negro, Línea Bulldog, Terminal 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1368,'Conector Automotriz Hembra 2 vias color Negro, Línea Bulldog, Terminal 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1369,'Conector Automotriz Macho 2 vias color Negro, Línea Bulldog, Terminal 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1370,'Conector Automotriz Hembra 5 vias color Negro, Línea Bulldog, Terminal 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1371,'Conector Automotriz Macho 2 vias color Negro, Línea Bulldog, Terminal 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1372,'Conector Automotriz Sellado Hembra 2 vias color Negro, Línea Superseal,  Terminal 1,5 mm 060',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1373,'Conector Automotriz Sellado Hembra 4 vias color Negro, Línea Superseal,  Terminal 1,5 mm 060',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1374,'Conector Automotriz Sellado Hembra 5 vias color Negro, Línea Superseal,  Terminal 1,5 mm 060',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1375,'Conector Automotriz Sellado Macho 5 vias color Negro, Línea Superseal,  Terminal 1,5 mm 060',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1376,'Conector Tyco Hembra 2 vias color Negro, Línea Superseal,  Terminal 1,5 mm 060',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1377,'Conector Automotriz 48 vias color Negro, Term 1,5 mm, uso GNC, Línea ECU, Molex CMC',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1378,'Conector Automotriz 2 vias color Negro, Term 1,5 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1379,'Conector Hembra 5 vias color Negro, Serie MCON 1.2, Term 1,5 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1380,'Conector Automotriz Hembra 4 vias color Negro, Term 1,5 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1381,'Conector Automotriz Macho 6 vías mixto Sicma color Gris, Terminal de 1,5 y 2,8',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1382,'Conector Automotriz 24 vias color Gris, Term 1,5 / 2,8 mm, uso GNC, Sicma mixto c/Traba corredera Violeta',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1383,'Conector Automotriz Hembra 5 vias color Marrón, Línea Bulldog, Terminal 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1384,'Conector Automotriz Macho 15 vias color Violeta, FLA-ST-GEH, Terminal 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1385,'Conector Hembra 18 vías color Gris, Terminal 2,8 mm .110, con Traba Secundaria',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1386,'Conector Automotriz Hembra 2 vias color Gris, Línea Bulldog con Alambre destraba al costado, Terminal 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1387,'Conector Automotriz Macho 2 vias color Gris, Línea Bulldog con guia doble, Terminal 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1388,'Conector Hembra 2 vias color Blanco con Traba Seguridad Terminal 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1389,'Conector Macho 2 vias color Blanco con Traba Seguridad Terminal 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1390,'Conector Hembra 3 vias color Blanco con Traba Seguridad Terminal 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1391,'Conector Macho 3 vias  color Blanco con Traba Seguridad Terminal 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1392,'Conector Hembra 4 vias color Blanco con Traba Seguridad Terminal 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1393,'Conector Macho 4 vias color Blanco con Traba Seguridad Terminal 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1394,'Conector Hembra 6 vias color Blanco con Traba Seguridad Terminal 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1395,'Conector Macho 6 vias color Blanco con Traba Seguridad Terminal 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1396,'Conector Hembra 9 vias color Blanco con Traba Seguridad Terminal 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1397,'Conector Macho 9 vias color Blanco con Traba Seguridad Terminal 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1398,'Conector Hembra 8 vias color Blanco con T/S, Terminal 2,8 mm .110, Tyco 9-965382-1',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1399,'Conector Hembra 4 vias color Blanco con Traba Seguridad Terminal 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1400,'Conector Macho 4 vias color Blanco con Traba Seguridad Terminal 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1401,'Conector Hembra 6 vias color Blanco con Traba Seguridad Terminal 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1402,'Conector Macho 6 vias color Blanco con Traba Seguridad Terminal 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1403,'Conector Hembra 3 vias color Blanco c/TSeg Terminal 6,3 mm lengueta Compensadora TE101005 - configuracion \"T\"',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1404,'Conector Macho 3 vias color Blanco con Traba Seguridad Terminal 6,3 mm - configuracion \"T\"',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1405,'Conector Hembra 3 vias color Blanco c/Tseg,  Terminal 6,3 mm Fastin-faston TE101002 - configuracion \"T\"',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1406,'Conector Hembra 4 vias color Blanco con Traba Seguridad Terminal 6,3 mm lengueta Compensadora',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1407,'Conector Macho 4 vias color Blanco con Traba Deslizante Terminal 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1408,'Conector Hembra 4 vias color Blanco con Traba Seguridad Terminal 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1409,'Conector Macho 4 vias color Blanco con Traba Seguridad Terminal 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1410,'Conector Hembra 2 vias color Blanco con Traba Seguridad Terminal 6,3 mm Fastin faston',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1411,'Conector Hembra 4 vias color Blanco con Traba Deslizante Terminal Clip 6,3 mm lengueta Compensadora',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1412,'Conector Macho 4 vias color Blanco con Traba Seguridad Terminal Plano 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1413,'Conector Hembra 2 vias color Blanco con Traba Seguridad Terminal Clip 6,3 mm lengueta Compensadora, disposición T',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1414,'Conector Macho 2 vias color Blanco con Traba Seguridad Terminal 6,3 mm, disposición T',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1415,'Conector Hembra 6 vias color Blanco con Traba Seguridad Terminal 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1416,'Conector Macho 6 vias color Blanco con Traba Seguridad Terminal 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1417,'Conector Hembra 6 vias color Blanco con Traba Seguridad Terminal 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1418,'Conector Macho 6 vias color Blanco con Traba Seguridad Terminal 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1419,'Conector Hembra 4 vias color Blanco con Traba Deslizante Terminal Clip 6,3 mm Fastin-faston',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1420,'Conector Hembra 1 vias color Blanco con Traba Seguridad Terminal Clip 6,3 mm Fastin-faston',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1421,'Conector Hembra 2 vias color Blanco con Traba Seguridad Terminal Clip 6,3 mm Fastin-faston, T',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1422,'Conector Macho 2 vias color Blanco con Traba Seguridad Terminal Clip 6,3 mm, T',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1423,'Conector Macho 6 vias color Blanco con Traba Seguridad Terminal 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1424,'Conector Hembra 2 vias color Blanco con Traba Seguridad Terminal 8,2 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1425,'Conector Macho 2 vias color Blanco con Traba Seguridad Terminal 8,2 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1426,'Conector Automotriz 4 vías Delphi',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1427,'Conector Hembra 4 vias Línea Mini Fit Jr., color Blanco con Traba Seguridad Terminal 1,7 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1428,'Conector Hembra 6 vias Línea Mini Fit Jr., color Blanco con Traba Seguridad Terminal 1,7 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1429,'Conector Hembra 8 vias Línea Mini Fit Jr., color Blanco con Traba Seguridad Terminal 1,7 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1430,'Conector Hembra 10 vias Línea Mini Fit Jr., color Blanco con Traba Seguridad Terminal 1,7 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1431,'Conector Hembra 14 vias Línea Mini Fit Jr., color Blanco con Traba Seguridad Terminal 1,7 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1432,'Conector Hembra 3 vias color Beige Polarizado CST-100 Terminal x,x mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1433,'Conector Hembra 3 vias Serie CI01 Terminal 1,5 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1434,'Conector Hembra 7 vias Yazaki TLC 7129-6071 Terminal cuadrado 2,1 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1435,'Conector Hembra 10 vias Tyco 1670990-1, Terminal cuadrado 1,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1436,'Conector Hembra 6 vias color Azul, AMP 1-969489-4, Terminal cuadrado 1,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1437,'Aislador Porta Clip 1 via Hembra color Negro sin Traba de Seguridad Terminal 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1438,'Aislador Porta Clip Hembra color Negro, Terminal Pala 6,3 mm s/Traba',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1439,'Aislador Terminal cilindrico Hembra Ø 3,95 mm, Color Blanco, sin Traba Seguridad',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1440,'Aislador Porta Clip Hembra color Marrón,Terminal Pala 6,3 mm s/Traba',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1441,'Aislador Porta Clip Hembra color Rojo,Terminal Pala 6,3 mm s/Traba',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1442,'Aislador Porta Clip Hembra color Amarillo,Terminal Pala 6,3 mm s/Traba',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1443,'Aislador Porta Clip Hembra color Gris,Terminal Pala 6,3 mm s/Traba',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1444,'Conector Hembra 3 vias color Blanco sin Traba Seguridad Terminal 2,8 mm Fastin-faston',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1445,'Conector Macho 3 vias color Blanco sin Traba Seguridad Terminal 2,8 mm Fastin-faston',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1446,'Conector Hembra 3 vias color Blanco sin Traba Seguridad Terminal 3,7 mm FF, AMP 925015.0',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1447,'Conector Hembra 2 vias color Blanco sin Traba Seguridad Terminal 6,3 mm configuracion \"L\"',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1448,'Conector Macho 2 vias color Blanco sin Traba Seguridad Terminal 6,3 mm configuracion \"L\"',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1449,'Conector Hembra 2 vias color Blanco sin Traba Seguridad Terminal 6,3 mm Lengüeta Compensadora, configuracion \"L\"',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1450,'Conector Hembra 2 vias color Blanco sin Traba Seguridad Terminal 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1451,'Conector Macho 2 vias color Blanco sin Traba Seguridad Terminal 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1452,'Conector Hembra 6 vias color Blanco sin Traba Seguridad Terminal 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1453,'Conector Macho 6 vias color Blanco sin Traba Seguridad Terminal 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1454,'Aislador Porta Clip Hembra color Blanco,Terminal Pala 6,3 mm s/Traba',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1455,'Aislador Porta Clip Macho color Blanco,Terminal Pala 6,3 mm s/Traba',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1456,'Aislador Terminal Hembra Bandera color Blanco, Pala 6,3 mm s/Traba',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1457,'Aislador Terminal Hembra Bandera FF color Blanco, Pala 6,3 mm s/Traba',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1458,'Conector Hembra 4 vías color Blanco, Terminal Pala Hembra 6,3 mm, Línea Blanca',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1459,'Aislador Porta Clip 1 via Hembra color Blanco sin Traba de Seguridad Terminal 5 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1460,'Aislador Porta Clip Hembra color Blanco,Terminal Pala 4,8 mm s/Traba',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1461,'Aislador Terminal Hembra Bandera FF color Natural, Pala 6,3 mm s/Traba',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1462,'Conector Hembra Impermeable 8 vias color Negro con Traba Seguridad Terminal 2,2 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1463,'Fusible Tipo Americano de vidrio 31 mm x 6,3 mm, 10 Amp',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1464,'Fusible Ficha Plano 3 Amp',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1465,'Fusible Ficha Plano 5 Amp',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1466,'Fusible Ficha Plano 7,5 Amp',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1467,'Fusible Ficha Plano 10 Amp',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1468,'Fusible Ficha Plano 15 Amp',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1469,'Fusible Electrónico PTC Reseteable de 9 Amp - WH16 900',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1470,'Moldeado PVC para Terminal Plano Hembra de 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1471,'Moldeado PVC Negro para Terminal Plano Bandera Hembra de 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1472,'Moldeado PVC Negro para Terminal Plano Bandera Hembra de 4,75 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1473,'Moldeado PVC Negro Posicionador para corrugado Ø 13 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1474,'Moldeado guia Cableado',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1475,'Moldeado PVC Transparente para Terminal Plano Bandera Hembra de 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1476,'Moldeado PVC Transparente para Terminal Cilíndrico Macho 3,5 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1477,'Moldeado PVC Transparente para Terminal Cilíndrico Hembra 3,5 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1478,'Porta Fusibles cilindrico 30 mm, 2 Fusibles',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1479,'Porta Fusibles cilindrico 30 mm, Color Blanco, 1 Fusible',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1480,'Porta Fusibles Ficha con Tapa y Ensamblado, Color Negro',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1481,'Porta Fusibles Ficha sin Tapa Color Negro',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1482,'Plug Motor color Azul, Tyco 1-773119-8',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1483,'Precinto 2,5 mm largo 100 mm color Blanco',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1484,'Precinto 2,5 mm largo 100 mm color Negro',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1485,'Precinto de amarre con Gozne, 5 mm largo 180 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1486,'Porta Relé color Negro 5 vías, Terminal 6,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1487,'Sello para Conector Automotriz Tyco 282189-1, Tipo Bulldog',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1488,'Sello para Conector Automotriz, Línea Superseal CO100802, CO100806, CO100808, CO100809',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1489,'Sello para Conector Automotriz 4 vías CO100818',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1490,'Sello para Conector Automotriz 6 vías mixto Sicma CO100821',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1491,'Sello para Conector Automotriz, Línea Bulldog CO100000',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1492,'Sello para Conector Automotriz 4 vías Delphi CO193000',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1493,'Sello para Conector Automotriz Yazaki 7129-6071, entrada Cable, 7157-3621',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1494,'Sello para Conector Automotriz Yazaki 7129-6071, Tapón, 7157-3622',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1495,'Sensor de Temperatura NTC Dis 19499, Fabricante Elkab, Código Fiat 07720915008',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1496,'TAPA CONECTOR DE 48 V - CO100812',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1497,'Terminal Plano Hembra 2,8 mm con Traba Seguridad para seccion 0,85 - 1,25 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1498,'Terminal Plano Macho 2,8 mm con Traba Seguridad para seccion 0,85 - 1,25 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1499,'Terminal Plano Hembra 6,3 mm con Traba Seguridad para seccion 0,85 - 1,25 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1500,'Terminal Plano Macho 6,3 mm con Traba Seguridad para seccion 0,85 - 1,25 mm2 - Largo Pala 15,5 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1501,'Terminal Plano Hembra 8,2 mm con Traba Seguridad',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1502,'Terminal Plano Macho 8,2 mm con Traba Seguridad, seccion 4,0 a 6,0 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1503,'Terminal Plano Hembra 6,3 mm con Traba Seguridad, lengueta Compensadora',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1504,'Terminal Plano Macho 6,3 mm con Traba Seguridad para cable 0,85 - 1,25 mm2 - Largo Pala 13,1 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1505,'Terminal Plano Macho 6,3 mm con Traba Seguridad para cable 0,85 - 1,25 mm2 - Largo Pala 14 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1506,'Terminal Plano Hembra 6,3 mm con Traba Seguridad para seccion 2,5 - 6,00 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1507,'Terminal Plano Hembra 5,0 mm con Traba Seguridad para seccion 0,35 - 1,00 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1508,'Terminal Plano Hembra 2,3 mm con Traba Seguridad para seccion 0,5 - 0,8 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1509,'Terminal Plano Hembra 6,3 mm con Traba Seguridad para seccion 0,8 - 3,00 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1510,'Terminal Plano Macho 6,3 mm con Traba Seguridad para seccion 1,00 - 3,00 mm2 - Largo Pala 15,5 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1511,'Terminal Plano Hembra 5,0 mm con Traba Seguridad para seccion 1 - 3 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1512,'Terminal Plano Hembra 2,8 mm c/TS, macho 0,8 mm y  seccion 0,25 - 1 mm2, Línea Fastin-faston',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1513,'Terminal Plano Macho 2,8 mm c/TS de 0,8 mm, seccion 0,25 - 1 mm2, Línea Fastin-faston',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1514,'Terminal Plano Hembra 5 mm s/TS, macho 0,8 mm y  seccion 0,25 - 1 mm2, Línea Fastin-faston',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1515,'Terminal Plano Hembra 2,8 mm c/TS, seccion 0,25 - 1 mm2, Línea Fastin-faston Serie 110, AMP 150572',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1516,'Terminal Plano Hembra 2,8 mm c/TS, seccion 0,5 - 1 mm2, Línea Fastin-faston Serie 110, AMP 880634-1',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1517,'Terminal Plano Macho 2,8 mm c/TS de 0,8 mm, seccion 0,25 - 1 mm2, Estañado, Línea Fastin-faston',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1518,'Terminal Cilíndrico Hembra Ø 3,0 mm para conector Delphi',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1519,'Terminal cilindrico Hembra para Conector Línea Mini Fit Jr., F 1,70 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1520,'Terminal cilindrico Hembra para Conector Línea ECU F 0,60 mm para cable 0,75 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1521,'Terminal cilindrico Hembra para Conector Línea ECU F 1,50 mm para cable 1,5 a 2,0 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1522,'Terminal Hembra 1,5 mm para Conector Automotriz Línea AMP Superseal para cable 0,5 a 0,85 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1523,'Terminal Macho 1,5 mm para Conector Automotriz Línea AMP Superseal para cable 0,5 a 0,85 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1524,'Terminal Hembra 3,5 mm para Conector Línea Bulldog para cable 0,5 a 1 mm2, con Traba doble',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1525,'Terminal Macho 3,5 mm para Conector Línea Bulldog para cable 0,5 a 1 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1526,'Terminal rectangular Hembra Yazaki 7115-1591,  2,10 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1527,'Terminal rectangular Hembra 1,8 mm, Tyco 928999-1 o 929454',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1528,'Terminal Plano Macho 2,8 mm, ancho 0,8 mm, para cable sección ',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1529,'Terminal Tyco Hembra 2,8 mm para Conector Línea Bulldog para cable 0,5 a 1 mm2, con Sello',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1530,'Terminal Hembra 3,5 mm para Conector Línea Bulldog para cable 0,5 a 1 mm2, con Traba simple',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1531,'Terminal Macho 2,8 mm para Conector Línea Bulldog para cable 0,5 a 1 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1532,'Terminal Plano Macho 1,5 mm, ancho 0,8 mm, para cable sección ',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1533,'Terminal Tyco Hembra 1,5 mm para Conector Serie MCON 1.2, para cable 0,5 a 1 mm2, con Sello',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1534,'Terminal Tyco Macho 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1535,'Terminal Automotriz Hembra 1,5 mm, Boca de Pescado',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1536,'Terminal Automotriz Hembra Sicma 1,5 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1537,'Terminal Automotriz Macho Sicma 1,5 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1538,'Terminal Automotriz Hembra Sicma 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1539,'Terminal Automotriz Macho Sicma 2,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1540,'Terminal de contacto CST-100 para Electrónica 1,5 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1541,'Terminal Plano Hembra para Aislador Unitario, Pala 6,3 mm Macho 0,8, cable 0,35 - 1 mm2, FF',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1542,'Terminal Plano Macho para Aislador Unitario, Pala 6,3 mm x 11,5 mm Espesor 0,8, cable 0,5 - 1,5 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1543,'Terminal Plano Hembra 3,7 mm sin T/S para cable 0,5 - 0,8 mm2, AMP 160303-2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1544,'Terminal Plano Hembra para Aislador Unitario, Pala 4,75 mm Macho 0,8, cable 0,5 - 2 mm2, FF',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1545,'Terminal Plano Hembra Bandera con Punto de Retención, 6,35 mm Macho 0,80 mm, cable 0,35 a 1 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1546,'Terminal Plano Hembra Bandera 4,75 mm para cable 0,5 - 1,374 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1547,'Terminal Bandera Plano Hembra 5,2 mm para cable xxxxxx',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1548,'Terminal Plano Hembra Bandera Fastin Faston, 6,35 mm Macho 0,80 mm, cable 0,5 a 1 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1549,'Terminal Contacto Portalamparas',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1550,'Terminal Remache para Portalampara',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1551,'Terminal para Diodos',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1552,'Terminal Ojal F Int 6,5 mm, F Ext 12 mm, para cable seccion 1 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1553,'Terminal Ojal F Int 6,5 mm, F Ext 12 mm, para cable seccion 4 mm2 Espesor 0,5 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1554,'Terminal Ojal F Int 6,5 mm, F Ext 12 mm, para cable seccion 4 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1555,'Terminal Ojal F Int 6,5 mm, F Ext 12 mm  A 90º, para cable seccion 4 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',7,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1556,'Terminal Ojal Estriado Ø 4,25 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1557,'Terminal Ojal F Int 5,25 mm, F Ext 8,4 mm, espesor 0,5mm, para cable seccion 0,5 a 2 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1558,'Terminal Ojal F Int 4,2 mm, F Ext 9,7 mm, espesor 0,5 mm, para cable seccion 0,8 a 2 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1559,'Terminal Ojal F Int 5,25 mm, F Ext 9,75 mm, espesor 0,5mm, para cable seccion 0,5 a 2 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1560,'Terminal cilindrico Hembra F 3,5 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1561,'Terminal cilindrico Macho F 3,5 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1562,'Terminal cilindrico Hembra F 3,95 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1563,'Terminal cilindrico Macho F 3,95 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1564,'Terminal cilindrico Hembra F 1,95 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1565,'Terminal Porta Fusible para cable seccion 1,5 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1566,'Terminal Empalme seccion 1,5 mm2 / 4,0 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1567,'Terminal Empalme seccion 2,0 mm2 / 5,0 mm2',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',24,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1568,'Terminal cilindrico Doble Hembra F 3,5 mm - Largo 8,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',5,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1569,'Terminal cilindrico Doble Hembra F 3,5 mm - Largo 16,2 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1570,'Terminal cilindrico Doble Hembra F 3,95 mm - Largo 8,3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1571,'termocontraible con Adhesivo Negro pared fina ø 12 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',28,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1572,'termocontraible Negro pared fina ø 3,2 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1573,'termocontraible Negro pared fina ø 4,8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1574,'termocontraible Negro pared fina ø 6,4 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1575,'termocontraible Negro pared fina ø 8,0 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1576,'termocontraible Negro pared fina ø 9,5 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1577,'termocontraible Negro pared fina ø 12,7 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',9,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1578,'termocontraible Negro ø 38,1 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',13,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1579,'Trava color Azul Tyco 1-969490-4, para Conector de 6 vías AMP 1-969489-4',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1580,'Trava color Violeta para Conector Automotriz 15 vías CO170019',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1581,'Trava color Gris Tyco 965383-1, para Conector de 8 vías Tyco 965382-1',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1582,'Trava color Gris Tyco 967634-1, para Conector de 18 vías Tyco 1-967629-1',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',8,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1583,'Tubo PVC color Negro F 3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1584,'Tubo PVC color Negro F 4 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1585,'Tubo PVC color Negro F 5 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1586,'Tubo PVC color Negro F 6 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1587,'Tubo PVC color Negro F 7 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1588,'Tubo PVC color Negro F 8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1589,'Tubo PVC color Negro F 9 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1590,'Tubo PVC color Negro F 10 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1591,'Tubo PVC color Negro F 11 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1592,'Tubo PVC color Negro F 12 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1593,'Tubo PVC color Negro F 13 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1594,'Tubo PVC color Negro F 14 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1595,'Tubo PVC color Negro F 15 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1596,'Tubo PVC color Negro F 16 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1597,'Tubo PVC color Negro F 17 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1598,'Tubo PVC color Negro F 18 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1599,'Tubo PVC color Negro F 19 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1600,'Tubo PVC color Negro F 20 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1601,'Tubo PVC color Negro F 21 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1602,'Tubo PVC color Negro F 22 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1603,'Tubo PVC color Negro F 23 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1604,'Tubo PVC color Negro F 24 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1605,'Tubo PVC color Negro F 25 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1606,'Tubo PVC Transparente F 5 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',11,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1607,'Tubo Corrugado de polipropileno color Negro F Ext 7,1 mm, Ø Int 4,9 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',12,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1608,'Tubo Corrugado de polipropileno color Negro F Ext 9,5 mm, Ø Int 6,7 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',12,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1609,'Tubo Corrugado de polipropileno color Negro F Ext 13,2 mm, Ø Int 9,1',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',12,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1610,'Spaghetti Barnizado de Polyester color Blanco F 2 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1611,'Spaghetti Barnizado de Polyester color Blanco F 3 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1612,'Spaghetti Barnizado de Polyester color Blanco F 4 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1613,'Spaghetti Barnizado de Polyester color Blanco F 6 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1614,'Spaghetti Barnizado de Polyester color Blanco F 8 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1615,'Spaghetti Barnizado de Polyester color Blanco F 10 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);
insert  into `materia_prima`(`id`,`Descripcion`,`Inactivo`,`Caja_Sujecion`,`Unidad_Medida`,`Ubicacion`,`id_proveedor`,`id_tipo`) values (1616,'Tubo PVC Reforzado color Negro F 6 mm',0,'Sin Caja de sujecion','Unidad','Sin Ubicacion',10,2);

/*Table structure for table `materiaprimatipo` */

DROP TABLE IF EXISTS `materiaprimatipo`;

CREATE TABLE `materiaprimatipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `materiaprimatipo` */

insert  into `materiaprimatipo`(`id`,`nombre`) values (1,'Cable');
insert  into `materiaprimatipo`(`id`,`nombre`) values (2,'Componente');

/*Table structure for table `orden` */

DROP TABLE IF EXISTS `orden`;

CREATE TABLE `orden` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_id` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `tipo` enum('presupuesto','pedido') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cliente` (`cliente_id`),
  CONSTRAINT `cliente` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `orden` */

/*Table structure for table `orden_detalle` */

DROP TABLE IF EXISTS `orden_detalle`;

CREATE TABLE `orden_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orden_id` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `total` float(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orden` (`orden_id`),
  KEY `productofk` (`producto_id`),
  CONSTRAINT `orden` FOREIGN KEY (`orden_id`) REFERENCES `orden` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `productofk` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `orden_detalle` */

/*Table structure for table `producto` */

DROP TABLE IF EXISTS `producto`;

CREATE TABLE `producto` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `Diseno` varchar(100) DEFAULT NULL,
  `FechaAlta` date NOT NULL,
  `FechaBaja` date DEFAULT NULL,
  `Observaciones` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `producto` */

insert  into `producto`(`id`,`Diseno`,`FechaAlta`,`FechaBaja`,`Observaciones`) values (1,'Diseño Maverik','2018-06-05',NULL,'Nuevo conector para guiñes 2');
insert  into `producto`(`id`,`Diseno`,`FechaAlta`,`FechaBaja`,`Observaciones`) values (2,'asdfadsf','2018-06-05',NULL,'');
insert  into `producto`(`id`,`Diseno`,`FechaAlta`,`FechaBaja`,`Observaciones`) values (3,'Diseño de mazo para autos','2018-06-11',NULL,'Esto es otra prueba de Nestor');

/*Table structure for table `proveedor` */

DROP TABLE IF EXISTS `proveedor`;

CREATE TABLE `proveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `dni_cuil_cuit` varchar(14) DEFAULT NULL,
  `codigo_postal` varchar(30) DEFAULT NULL,
  `direccion` varchar(100) NOT NULL,
  `localidad` varchar(50) DEFAULT NULL,
  `provincia` varchar(40) DEFAULT NULL,
  `barrio` varchar(30) DEFAULT NULL,
  `fecha_alta` date DEFAULT NULL,
  `fecha_baja` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

/*Data for the table `proveedor` */

insert  into `proveedor`(`id`,`nombre`,`telefono`,`email`,`dni_cuil_cuit`,`codigo_postal`,`direccion`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`) values (4,'Cibasa S.A.','(011) 42418909','eruffo@cibasa.com','30504671301','1824','Av. Gral. San Martín 3020 ','Lanús','Buenos Aires','no informado','2018-00-00','2018-00-01');
insert  into `proveedor`(`id`,`nombre`,`telefono`,`email`,`dni_cuil_cuit`,`codigo_postal`,`direccion`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`) values (5,'Conectech S.A.','(011) 4756 5777','gutman@conectech.com.ar','30708670592','3152','Panamá 3152','Munro','Buenos Aires','no informado','2018-00-02','2018-00-03');
insert  into `proveedor`(`id`,`nombre`,`telefono`,`email`,`dni_cuil_cuit`,`codigo_postal`,`direccion`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`) values (6,'Coelar S.R.L.','(011)  4713 8511       ','dpinosa@coelar.com.ar','30710687915','1650','Calle 22 (ex Rodríguez Peña) Nº 3655','San Martín','Buenos Aires','no informado','2018-00-01','2018-00-02');
insert  into `proveedor`(`id`,`nombre`,`telefono`,`email`,`dni_cuil_cuit`,`codigo_postal`,`direccion`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`) values (7,'Pfeifer','011 0000 0000','no informado','00000000000','0000','no informado','no informaod','no informado','no informado',NULL,NULL);
insert  into `proveedor`(`id`,`nombre`,`telefono`,`email`,`dni_cuil_cuit`,`codigo_postal`,`direccion`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`) values (8,'Tyco','011',NULL,NULL,NULL,'no informado',NULL,NULL,NULL,NULL,NULL);
insert  into `proveedor`(`id`,`nombre`,`telefono`,`email`,`dni_cuil_cuit`,`codigo_postal`,`direccion`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`) values (9,'Tacsa','000',NULL,NULL,NULL,'no informado',NULL,NULL,NULL,NULL,NULL);
insert  into `proveedor`(`id`,`nombre`,`telefono`,`email`,`dni_cuil_cuit`,`codigo_postal`,`direccion`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`) values (10,'Mayle','00',NULL,NULL,NULL,'no definodo',NULL,NULL,NULL,NULL,NULL);
insert  into `proveedor`(`id`,`nombre`,`telefono`,`email`,`dni_cuil_cuit`,`codigo_postal`,`direccion`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`) values (11,'Poberaj','000',NULL,NULL,NULL,'no definido',NULL,NULL,NULL,NULL,NULL);
insert  into `proveedor`(`id`,`nombre`,`telefono`,`email`,`dni_cuil_cuit`,`codigo_postal`,`direccion`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`) values (12,'Elexa','0000',NULL,NULL,NULL,'no definido',NULL,NULL,NULL,NULL,NULL);
insert  into `proveedor`(`id`,`nombre`,`telefono`,`email`,`dni_cuil_cuit`,`codigo_postal`,`direccion`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`) values (13,'Resi','00000',NULL,NULL,NULL,'no definido',NULL,NULL,NULL,NULL,NULL);
insert  into `proveedor`(`id`,`nombre`,`telefono`,`email`,`dni_cuil_cuit`,`codigo_postal`,`direccion`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`) values (24,'Industrias Zeta S.R.L.','(011) 4748 7555','z3@industriaszeta.com','30576813151','1611','M T de Alvear (Ruta 202) Nº 3525','Tigre','Buenos Aires','no informado','2018-00-03','2018-00-04');
insert  into `proveedor`(`id`,`nombre`,`telefono`,`email`,`dni_cuil_cuit`,`codigo_postal`,`direccion`,`localidad`,`provincia`,`barrio`,`fecha_alta`,`fecha_baja`) values (28,'Sin Definir','00',NULL,NULL,NULL,'No definido',NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `stock` */

DROP TABLE IF EXISTS `stock`;

CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock` int(11) NOT NULL,
  `stock_min` int(11) NOT NULL,
  `stock_max` int(11) NOT NULL,
  `materia_prima_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `materia_prima_id_2` (`materia_prima_id`),
  KEY `materia_prima_id` (`materia_prima_id`),
  CONSTRAINT `stock_ibfk_1` FOREIGN KEY (`materia_prima_id`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=773 DEFAULT CHARSET=latin1;

/*Data for the table `stock` */

insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (2,0,0,0,66);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (3,0,0,0,67);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (4,0,0,0,68);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (5,0,0,0,69);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (6,0,0,0,70);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (7,0,0,0,71);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (8,0,0,0,72);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (9,0,0,0,73);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (10,0,0,0,74);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (11,0,0,0,75);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (12,0,0,0,76);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (13,0,0,0,77);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (14,0,0,0,78);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (15,0,0,0,79);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (16,0,0,0,80);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (17,0,0,0,81);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (18,0,0,0,82);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (19,0,0,0,83);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (20,0,0,0,84);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (21,0,0,0,85);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (22,0,0,0,86);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (23,0,0,0,87);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (24,0,0,0,88);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (25,0,0,0,89);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (26,0,0,0,90);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (27,0,0,0,91);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (28,0,0,0,92);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (29,0,0,0,93);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (30,0,0,0,94);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (31,0,0,0,95);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (32,0,0,0,96);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (33,0,0,0,97);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (34,0,0,0,98);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (35,0,0,0,99);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (36,0,0,0,100);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (37,0,0,0,101);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (38,0,0,0,102);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (39,0,0,0,103);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (40,0,0,0,104);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (41,0,0,0,105);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (42,0,0,0,106);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (43,0,0,0,107);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (44,0,0,0,108);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (45,0,0,0,109);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (46,0,0,0,110);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (47,0,0,0,111);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (48,0,0,0,112);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (49,0,0,0,113);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (50,0,0,0,114);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (51,0,0,0,115);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (52,0,0,0,116);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (53,0,0,0,117);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (54,0,0,0,118);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (55,0,0,0,119);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (56,0,0,0,120);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (57,0,0,0,121);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (58,0,0,0,122);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (59,0,0,0,123);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (60,0,0,0,124);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (61,0,0,0,125);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (62,0,0,0,126);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (63,0,0,0,127);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (64,0,0,0,128);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (65,0,0,0,129);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (66,0,0,0,130);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (67,0,0,0,131);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (68,0,0,0,132);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (69,0,0,0,133);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (70,0,0,0,134);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (71,0,0,0,135);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (72,0,0,0,136);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (73,0,0,0,137);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (74,0,0,0,138);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (75,0,0,0,139);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (76,0,0,0,140);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (77,0,0,0,141);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (78,0,0,0,142);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (79,0,0,0,143);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (80,0,0,0,144);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (81,0,0,0,145);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (82,0,0,0,146);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (83,0,0,0,147);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (84,0,0,0,148);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (85,0,0,0,149);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (86,0,0,0,150);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (87,0,0,0,151);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (88,0,0,0,152);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (89,0,0,0,153);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (90,0,0,0,154);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (91,0,0,0,155);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (92,0,0,0,156);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (93,0,0,0,157);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (94,0,0,0,158);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (95,0,0,0,159);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (96,0,0,0,160);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (97,0,0,0,161);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (98,0,0,0,162);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (99,0,0,0,163);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (100,0,0,0,164);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (101,0,0,0,165);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (102,0,0,0,166);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (103,0,0,0,167);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (104,0,0,0,168);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (105,0,0,0,169);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (106,0,0,0,170);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (107,0,0,0,171);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (108,0,0,0,172);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (109,0,0,0,173);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (110,0,0,0,174);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (111,0,0,0,175);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (112,0,0,0,176);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (113,0,0,0,177);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (114,0,0,0,178);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (115,0,0,0,179);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (116,0,0,0,180);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (117,0,0,0,181);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (118,0,0,0,182);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (119,0,0,0,183);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (120,0,0,0,184);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (121,0,0,0,185);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (122,0,0,0,186);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (123,0,0,0,187);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (124,0,0,0,188);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (125,0,0,0,189);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (126,0,0,0,190);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (127,0,0,0,191);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (128,0,0,0,192);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (129,0,0,0,193);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (130,0,0,0,194);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (131,0,0,0,195);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (132,0,0,0,196);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (133,0,0,0,197);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (134,0,0,0,198);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (135,0,0,0,199);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (136,0,0,0,200);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (137,0,0,0,201);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (138,0,0,0,202);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (139,0,0,0,203);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (140,0,0,0,204);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (141,0,0,0,205);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (142,0,0,0,206);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (143,0,0,0,207);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (144,0,0,0,208);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (145,0,0,0,209);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (146,0,0,0,210);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (147,0,0,0,211);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (148,0,0,0,212);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (149,0,0,0,213);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (150,0,0,0,214);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (151,0,0,0,215);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (152,0,0,0,216);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (153,0,0,0,217);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (154,0,0,0,218);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (155,0,0,0,219);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (156,0,0,0,220);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (157,0,0,0,221);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (158,0,0,0,222);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (159,0,0,0,223);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (160,0,0,0,224);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (161,0,0,0,225);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (162,0,0,0,226);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (163,0,0,0,227);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (164,0,0,0,228);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (165,0,0,0,229);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (166,0,0,0,230);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (167,0,0,0,231);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (168,0,0,0,232);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (169,0,0,0,233);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (170,0,0,0,234);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (171,0,0,0,235);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (172,0,0,0,236);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (173,0,0,0,237);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (174,0,0,0,238);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (175,0,0,0,239);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (176,0,0,0,240);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (177,0,0,0,241);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (178,0,0,0,242);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (179,0,0,0,243);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (180,0,0,0,244);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (181,0,0,0,245);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (182,0,0,0,246);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (183,0,0,0,247);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (184,0,0,0,248);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (185,0,0,0,249);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (186,0,0,0,250);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (187,0,0,0,251);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (188,0,0,0,252);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (189,0,0,0,253);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (190,0,0,0,254);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (191,0,0,0,255);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (192,0,0,0,256);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (193,0,0,0,257);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (194,0,0,0,258);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (195,0,0,0,259);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (196,0,0,0,260);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (197,0,0,0,261);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (198,0,0,0,262);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (199,0,0,0,263);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (200,0,0,0,264);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (201,0,0,0,265);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (202,0,0,0,266);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (203,0,0,0,267);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (204,0,0,0,268);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (205,0,0,0,269);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (206,0,0,0,270);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (207,0,0,0,271);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (208,0,0,0,272);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (209,0,0,0,273);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (210,0,0,0,274);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (211,0,0,0,275);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (212,0,0,0,276);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (213,0,0,0,277);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (214,0,0,0,278);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (215,0,0,0,279);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (216,0,0,0,280);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (217,0,0,0,281);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (218,0,0,0,282);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (219,0,0,0,283);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (220,0,0,0,284);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (221,0,0,0,285);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (222,0,0,0,286);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (223,0,0,0,287);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (224,0,0,0,288);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (225,0,0,0,289);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (226,0,0,0,290);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (227,0,0,0,291);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (228,0,0,0,292);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (229,0,0,0,293);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (230,0,0,0,294);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (231,0,0,0,295);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (232,0,0,0,296);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (233,0,0,0,297);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (234,0,0,0,298);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (235,0,0,0,299);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (236,0,0,0,300);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (237,0,0,0,301);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (238,0,0,0,302);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (239,0,0,0,303);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (240,0,0,0,304);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (241,0,0,0,305);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (242,0,0,0,306);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (243,0,0,0,307);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (244,0,0,0,308);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (245,0,0,0,309);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (246,0,0,0,310);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (247,0,0,0,311);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (248,0,0,0,312);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (249,0,0,0,313);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (250,0,0,0,314);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (251,0,0,0,315);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (252,0,0,0,316);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (253,0,0,0,317);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (254,0,0,0,318);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (255,0,0,0,319);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (256,0,0,0,320);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (257,0,0,0,321);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (258,0,0,0,322);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (259,0,0,0,323);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (260,0,0,0,324);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (261,0,0,0,325);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (513,0,0,0,1344);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (514,0,0,0,1345);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (515,0,0,0,1346);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (516,0,0,0,1347);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (517,0,0,0,1348);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (518,0,0,0,1349);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (519,0,0,0,1350);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (520,0,0,0,1351);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (521,0,0,0,1352);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (522,0,0,0,1353);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (523,0,0,0,1354);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (524,0,0,0,1355);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (525,0,0,0,1356);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (526,0,0,0,1357);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (527,0,0,0,1358);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (528,0,0,0,1359);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (529,0,0,0,1360);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (530,0,0,0,1361);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (531,0,0,0,1362);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (532,0,0,0,1363);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (533,0,0,0,1364);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (534,0,0,0,1365);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (535,0,0,0,1366);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (536,0,0,0,1367);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (537,0,0,0,1368);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (538,0,0,0,1369);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (539,0,0,0,1370);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (540,0,0,0,1371);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (541,0,0,0,1372);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (542,0,0,0,1373);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (543,0,0,0,1374);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (544,0,0,0,1375);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (545,0,0,0,1376);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (546,0,0,0,1377);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (547,0,0,0,1378);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (548,0,0,0,1379);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (549,0,0,0,1380);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (550,0,0,0,1381);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (551,0,0,0,1382);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (552,0,0,0,1383);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (553,0,0,0,1384);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (554,0,0,0,1385);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (555,0,0,0,1386);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (556,0,0,0,1387);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (557,0,0,0,1388);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (558,0,0,0,1389);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (559,0,0,0,1390);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (560,0,0,0,1391);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (561,0,0,0,1392);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (562,0,0,0,1393);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (563,0,0,0,1394);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (564,0,0,0,1395);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (565,0,0,0,1396);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (566,0,0,0,1397);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (567,0,0,0,1398);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (568,0,0,0,1399);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (569,0,0,0,1400);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (570,0,0,0,1401);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (571,0,0,0,1402);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (572,0,0,0,1403);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (573,0,0,0,1404);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (574,0,0,0,1405);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (575,0,0,0,1406);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (576,0,0,0,1407);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (577,0,0,0,1408);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (578,0,0,0,1409);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (579,0,0,0,1410);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (580,0,0,0,1411);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (581,0,0,0,1412);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (582,0,0,0,1413);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (583,0,0,0,1414);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (584,0,0,0,1415);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (585,0,0,0,1416);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (586,0,0,0,1417);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (587,0,0,0,1418);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (588,0,0,0,1419);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (589,0,0,0,1420);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (590,0,0,0,1421);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (591,0,0,0,1422);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (592,0,0,0,1423);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (593,0,0,0,1424);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (594,0,0,0,1425);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (595,0,0,0,1426);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (596,0,0,0,1427);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (597,0,0,0,1428);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (598,0,0,0,1429);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (599,0,0,0,1430);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (600,0,0,0,1431);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (601,0,0,0,1432);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (602,0,0,0,1433);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (603,0,0,0,1434);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (604,0,0,0,1435);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (605,0,0,0,1436);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (606,0,0,0,1437);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (607,0,0,0,1438);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (608,0,0,0,1439);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (609,0,0,0,1440);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (610,0,0,0,1441);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (611,0,0,0,1442);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (612,0,0,0,1443);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (613,0,0,0,1444);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (614,0,0,0,1445);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (615,0,0,0,1446);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (616,0,0,0,1447);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (617,0,0,0,1448);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (618,0,0,0,1449);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (619,0,0,0,1450);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (620,0,0,0,1451);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (621,0,0,0,1452);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (622,0,0,0,1453);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (623,0,0,0,1454);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (624,0,0,0,1455);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (625,0,0,0,1456);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (626,0,0,0,1457);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (627,0,0,0,1458);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (628,0,0,0,1459);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (629,0,0,0,1460);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (630,0,0,0,1461);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (631,0,0,0,1462);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (632,0,0,0,1463);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (633,0,0,0,1464);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (634,0,0,0,1465);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (635,0,0,0,1466);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (636,0,0,0,1467);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (637,0,0,0,1468);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (638,0,0,0,1469);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (639,0,0,0,1470);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (640,0,0,0,1471);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (641,0,0,0,1472);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (642,0,0,0,1473);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (643,0,0,0,1474);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (644,0,0,0,1475);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (645,0,0,0,1476);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (646,0,0,0,1477);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (647,0,0,0,1478);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (648,0,0,0,1479);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (649,0,0,0,1480);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (650,0,0,0,1481);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (651,0,0,0,1482);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (652,0,0,0,1483);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (653,0,0,0,1484);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (654,0,0,0,1485);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (655,0,0,0,1486);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (656,0,0,0,1487);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (657,0,0,0,1488);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (658,0,0,0,1489);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (659,0,0,0,1490);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (660,0,0,0,1491);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (661,0,0,0,1492);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (662,0,0,0,1493);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (663,0,0,0,1494);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (664,0,0,0,1495);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (665,0,0,0,1496);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (666,0,0,0,1497);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (667,0,0,0,1498);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (668,0,0,0,1499);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (669,0,0,0,1500);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (670,0,0,0,1501);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (671,0,0,0,1502);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (672,0,0,0,1503);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (673,0,0,0,1504);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (674,0,0,0,1505);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (675,0,0,0,1506);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (676,0,0,0,1507);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (677,0,0,0,1508);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (678,0,0,0,1509);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (679,0,0,0,1510);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (680,0,0,0,1511);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (681,0,0,0,1512);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (682,0,0,0,1513);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (683,0,0,0,1514);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (684,0,0,0,1515);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (685,0,0,0,1516);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (686,0,0,0,1517);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (687,0,0,0,1518);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (688,0,0,0,1519);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (689,0,0,0,1520);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (690,0,0,0,1521);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (691,0,0,0,1522);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (692,0,0,0,1523);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (693,0,0,0,1524);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (694,0,0,0,1525);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (695,0,0,0,1526);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (696,0,0,0,1527);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (697,0,0,0,1528);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (698,0,0,0,1529);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (699,0,0,0,1530);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (700,0,0,0,1531);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (701,0,0,0,1532);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (702,0,0,0,1533);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (703,0,0,0,1534);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (704,0,0,0,1535);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (705,0,0,0,1536);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (706,0,0,0,1537);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (707,0,0,0,1538);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (708,0,0,0,1539);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (709,0,0,0,1540);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (710,0,0,0,1541);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (711,0,0,0,1542);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (712,0,0,0,1543);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (713,0,0,0,1544);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (714,0,0,0,1545);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (715,0,0,0,1546);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (716,0,0,0,1547);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (717,0,0,0,1548);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (718,0,0,0,1549);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (719,0,0,0,1550);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (720,0,0,0,1551);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (721,0,0,0,1552);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (722,0,0,0,1553);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (723,0,0,0,1554);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (724,0,0,0,1555);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (725,0,0,0,1556);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (726,0,0,0,1557);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (727,0,0,0,1558);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (728,0,0,0,1559);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (729,0,0,0,1560);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (730,0,0,0,1561);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (731,0,0,0,1562);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (732,0,0,0,1563);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (733,0,0,0,1564);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (734,0,0,0,1565);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (735,0,0,0,1566);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (736,0,0,0,1567);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (737,0,0,0,1568);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (738,0,0,0,1569);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (739,0,0,0,1570);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (740,0,0,0,1571);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (741,0,0,0,1572);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (742,0,0,0,1573);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (743,0,0,0,1574);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (744,0,0,0,1575);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (745,0,0,0,1576);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (746,0,0,0,1577);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (747,0,0,0,1578);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (748,0,0,0,1579);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (749,0,0,0,1580);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (750,0,0,0,1581);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (751,0,0,0,1582);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (752,0,0,0,1583);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (753,0,0,0,1584);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (754,0,0,0,1585);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (755,0,0,0,1586);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (756,0,0,0,1587);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (757,0,0,0,1588);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (758,0,0,0,1589);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (759,0,0,0,1590);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (760,0,0,0,1591);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (761,0,0,0,1592);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (762,0,0,0,1593);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (763,0,0,0,1594);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (764,0,0,0,1595);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (765,0,0,0,1596);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (766,0,0,0,1597);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (767,0,0,0,1598);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (768,0,0,0,1599);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (769,0,0,0,1600);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (770,0,0,0,1601);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (771,0,0,0,1602);
insert  into `stock`(`id`,`stock`,`stock_min`,`stock_max`,`materia_prima_id`) values (772,0,0,0,1603);

/*Table structure for table `listadocables` */

DROP TABLE IF EXISTS `listadocables`;

/*!50001 DROP VIEW IF EXISTS `listadocables` */;
/*!50001 DROP TABLE IF EXISTS `listadocables` */;

/*!50001 CREATE TABLE  `listadocables`(
 `id` int(11) ,
 `tipo` varchar(5) ,
 `componente_id` int(11) ,
 `proveedor_id` int(11) ,
 `cod_setecel` varchar(30) ,
 `materia_prima_fk` int(11) ,
 `Descripcion` varchar(255) ,
 `Unidad_Medida` varchar(50) ,
 `stock` int(11) ,
 `nombre` varchar(100) 
)*/;

/*Table structure for table `listadocomponentes` */

DROP TABLE IF EXISTS `listadocomponentes`;

/*!50001 DROP VIEW IF EXISTS `listadocomponentes` */;
/*!50001 DROP TABLE IF EXISTS `listadocomponentes` */;

/*!50001 CREATE TABLE  `listadocomponentes`(
 `id` int(11) ,
 `tipo` varchar(10) ,
 `categoria` varchar(100) ,
 `componente_id` int(11) ,
 `proveedor_id` int(11) ,
 `cod_setecel` varchar(30) ,
 `materia_prima_fk` int(11) ,
 `Descripcion` varchar(255) ,
 `Unidad_Medida` varchar(50) ,
 `stock` int(11) ,
 `nombre` varchar(100) 
)*/;

/*Table structure for table `view_detalleparte` */

DROP TABLE IF EXISTS `view_detalleparte`;

/*!50001 DROP VIEW IF EXISTS `view_detalleparte` */;
/*!50001 DROP TABLE IF EXISTS `view_detalleparte` */;

/*!50001 CREATE TABLE  `view_detalleparte`(
 `id` int(11) ,
 `materia_prima_id` int(11) ,
 `id_tipo` int(11) ,
 `elemento_id` int(11) ,
 `producto_id` int(11) ,
 `numero_revision` int(11) ,
 `descripcion` varchar(255) ,
 `cod_setecel` varchar(30) ,
 `cantidad_cable` int(11) 
)*/;

/*Table structure for table `viewdetalleparte` */

DROP TABLE IF EXISTS `viewdetalleparte`;

/*!50001 DROP VIEW IF EXISTS `viewdetalleparte` */;
/*!50001 DROP TABLE IF EXISTS `viewdetalleparte` */;

/*!50001 CREATE TABLE  `viewdetalleparte`(
 `materia_prima_id` int(11) ,
 `id_tipo` int(11) ,
 `elemento_id` int(11) ,
 `producto_id` int(11) ,
 `numero_revision` int(11) ,
 `Descripcion` varchar(255) ,
 `cod_setecel` varchar(30) ,
 `cantidad` int(11) 
)*/;

/*View structure for view listadocables */

/*!50001 DROP TABLE IF EXISTS `listadocables` */;
/*!50001 DROP VIEW IF EXISTS `listadocables` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `listadocables` AS (select `mp`.`id` AS `id`,'cable' AS `tipo`,`c`.`id` AS `componente_id`,`p`.`id` AS `proveedor_id`,`c`.`cod_setecel` AS `cod_setecel`,`c`.`materia_prima_fk` AS `materia_prima_fk`,`mp`.`Descripcion` AS `Descripcion`,`mp`.`Unidad_Medida` AS `Unidad_Medida`,`s`.`stock` AS `stock`,`p`.`nombre` AS `nombre` from (((`cable` `c` join `materia_prima` `mp` on((`mp`.`id` = `c`.`materia_prima_fk`))) join `stock` `s` on((`s`.`materia_prima_id` = `mp`.`id`))) join `proveedor` `p` on((`p`.`id` = `mp`.`id_proveedor`)))) */;

/*View structure for view listadocomponentes */

/*!50001 DROP TABLE IF EXISTS `listadocomponentes` */;
/*!50001 DROP VIEW IF EXISTS `listadocomponentes` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `listadocomponentes` AS (select `mp`.`id` AS `id`,'componente' AS `tipo`,`ca`.`nombre` AS `categoria`,`c`.`id` AS `componente_id`,`p`.`id` AS `proveedor_id`,`c`.`cod_setecel` AS `cod_setecel`,`c`.`materia_prima_fk` AS `materia_prima_fk`,`mp`.`Descripcion` AS `Descripcion`,`mp`.`Unidad_Medida` AS `Unidad_Medida`,`s`.`stock` AS `stock`,`p`.`nombre` AS `nombre` from ((((`componente` `c` join `materia_prima` `mp` on((`mp`.`id` = `c`.`materia_prima_fk`))) join `stock` `s` on((`s`.`materia_prima_id` = `mp`.`id`))) join `proveedor` `p` on((`p`.`id` = `mp`.`id_proveedor`))) join `categoria` `ca` on((`ca`.`id` = `c`.`id_categoria`)))) */;

/*View structure for view view_detalleparte */

/*!50001 DROP TABLE IF EXISTS `view_detalleparte` */;
/*!50001 DROP VIEW IF EXISTS `view_detalleparte` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_detalleparte` AS select `dp`.`id` AS `id`,`mp`.`id` AS `materia_prima_id`,`mp`.`id_tipo` AS `id_tipo`,`c`.`id` AS `elemento_id`,`dp`.`producto_id` AS `producto_id`,`dp`.`numero_revision` AS `numero_revision`,`mp`.`Descripcion` AS `descripcion`,`c`.`cod_setecel` AS `cod_setecel`,`dp`.`cantidad_cable` AS `cantidad_cable` from ((`detalle_parte` `dp` join `materia_prima` `mp` on((`mp`.`id` = `dp`.`materia_prima_id`))) join `cable` `c` on((`c`.`materia_prima_fk` = `mp`.`id`))) union all select `dp`.`id` AS `id`,`mp`.`id` AS `materia_prima_id`,`mp`.`id_tipo` AS `id_tipo`,`c`.`id` AS `elemento_id`,`dp`.`producto_id` AS `producto_id`,`dp`.`numero_revision` AS `numero_revision`,`mp`.`Descripcion` AS `descripcion`,`c`.`cod_setecel` AS `cod_setecel`,`dp`.`cantidad_comp` AS `cantidad_comp` from ((`detalle_parte` `dp` join `materia_prima` `mp` on((`mp`.`id` = `dp`.`materia_prima_id`))) join `componente` `c` on((`c`.`materia_prima_fk` = `mp`.`id`))) */;

/*View structure for view viewdetalleparte */

/*!50001 DROP TABLE IF EXISTS `viewdetalleparte` */;
/*!50001 DROP VIEW IF EXISTS `viewdetalleparte` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `viewdetalleparte` AS select `mp`.`id` AS `materia_prima_id`,`mp`.`id_tipo` AS `id_tipo`,`c`.`id` AS `elemento_id`,`dp`.`producto_id` AS `producto_id`,`dp`.`numero_revision` AS `numero_revision`,`mp`.`Descripcion` AS `Descripcion`,`c`.`cod_setecel` AS `cod_setecel`,`dp`.`cantidad_cable` AS `cantidad` from ((`detalle_parte` `dp` join `materia_prima` `mp` on((`mp`.`id` = `dp`.`materia_prima_id`))) join `cable` `c` on((`c`.`materia_prima_fk` = `mp`.`id`))) union all select `mp`.`id` AS `materia_prima_id`,`mp`.`id_tipo` AS `id_tipo`,`c`.`id` AS `elemento_id`,`dp`.`producto_id` AS `producto_id`,`dp`.`numero_revision` AS `numero_revision`,`mp`.`Descripcion` AS `Descripcion`,`c`.`cod_setecel` AS `cod_setecel`,`dp`.`cantidad_comp` AS `cantidad` from ((`detalle_parte` `dp` join `materia_prima` `mp` on((`mp`.`id` = `dp`.`materia_prima_id`))) join `componente` `c` on((`c`.`materia_prima_fk` = `mp`.`id`))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
